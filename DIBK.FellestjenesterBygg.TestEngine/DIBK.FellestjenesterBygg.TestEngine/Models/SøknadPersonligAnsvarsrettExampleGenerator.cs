﻿using no.kxml.skjema.dibk.soeknadpersonligansvarsrett;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class PersonligAnsvarsrettExampleGenerator
    {
        public SoeknadPersonligAnsvarsrettType NySoeknadPersonligAnsvarsrett()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var personligAnsvarsrett = new SoeknadPersonligAnsvarsrettType();
            
            personligAnsvarsrett.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer =  rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 1",
                        postnr = "3800",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    bygningsnummer = "80466985",
                    bolignummer = "H0102"
                }
            };

            personligAnsvarsrett.tiltakshaver = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },

                foedselsnummer = "08117000290",
                navn = "Kari Hansen",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Kirkegt 4",
                    poststed = "Bø i Telemark",
                    postnr = "3800"
                },

                telefonnummer = "56749845",
                mobilnummer = "44552211",
                epost = "mj@domene.no"

            };

            personligAnsvarsrett.ansvarsrett = new AnsvarsrettType()
            {
                heleAnsvarsomraadet = false,

                ansvarsomraader = new[]
                {
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "Kontroll",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    arbeidsomraade = "Overordnet ansvar for kontroll",
                },

                    new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    arbeidsomraade = "Overordnet ansvar for utførelse (bygning, anlegg eller konstruksjon, tekniske installasjoner)",
                },

                    new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    arbeidsomraade = "Overordnet ansvar for prosjektering (bygning, anlegg eller konstruksjon, tekniske installasjoner)",
                }
            }
            };

                personligAnsvarsrett.signatur = new SignaturType()
                {
                    signaturdato = new System.DateTime(2016, 09, 08),
                    signertAv = "",
                    signertPaaVegneAv = "",
                    signeringssteg = ""
                };

            personligAnsvarsrett.kompetanseEgenUtdanning = false;
            personligAnsvarsrett.kompetanseInnleidForetak = true;
            personligAnsvarsrett.kompetanseMedhjelpersUtdannelse = true; 
    
            return personligAnsvarsrett;
        }
    }
}