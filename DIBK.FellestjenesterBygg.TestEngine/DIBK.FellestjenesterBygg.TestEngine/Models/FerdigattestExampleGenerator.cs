﻿using no.kxml.skjema.dibk.ferdigattest;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class FerdigattestExampleGenerator
    {

        public FerdigattestType NyFerdigattest()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var ferdigattest = new FerdigattestType();
            ferdigattest.fraSluttbrukersystem = "Fellestjenester bygg testmotor";

            ferdigattest.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2015",
                sakssekvensnummer = "123"
            };

            ferdigattest.tilfredstillerTiltaketKraveneFerdigattest = true;

            ferdigattest.tiltakshaver = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                foedselsnummer = "08117000290",
                navn = "MAJA Testperson KLEPPA",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Kirkegt 4",
                    poststed = "Bø i Telemark",
                    postnr = "3800"
                },
                telefonnummer = "56749845",
                mobilnummer = "44552211",
                epost = "mj@domene.no"
            };

            ferdigattest.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Gamlegt 4",                     
                        poststed = "Bø i Telemark",
                        postnr = "3800"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = "H0102"
                },
            };

            ferdigattest.foretattIkkeSoeknadspliktigeJusteringer = true;
            ferdigattest.tilstrekkeligDokumentasjonOverlevertEier = false;
            ferdigattest.utfoertInnen = new System.DateTime(2016, 12, 15);
            ferdigattest.utfoertInnenSpecified = true;
            ferdigattest.typeArbeider = "Dette er en beskrivelse av det gjenstående arbeidet før kravene til ferdigattest er tilfredstilt.";
            ferdigattest.bekreftelseInnen = new System.DateTime(2016, 12, 01);
            ferdigattest.bekreftelseInnenSpecified = true;



            ferdigattest.energiforsyning = new EnergiforsyningType()
            {
                varmefordeling = new []
                {
                    new KodeType()
                    {
                        kodeverdi = "elektriske panelovner",
                        kodebeskrivelse = "elektriske panelovner"
                    },
                    new KodeType()
                    {
                        kodeverdi = "elektriske varmekabler",
                        kodebeskrivelse = "elektriske varmekabler"
                    },
               },
                energiforsyning = new []
                {
                    new KodeType()
                    {
                        kodeverdi = "biobrensel",
                        kodebeskrivelse = "biobrensel"
                    },
                    new KodeType()
                    {
                        kodeverdi = "elektrisitet",
                        kodebeskrivelse = "elektrisitet"
                    },
                },
                relevant = false,
                relevantSpecified = true
            };


            ferdigattest.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "974702665", // 974702665 DIBK Oslo
                navn = "Byggmester Bob AS",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Christian V gate 34",
                    poststed = "Bø i Telemark",
                    postnr = "3800"
                },
                telefonnummer = "45678900",
                mobilnummer = "44667788",
                epost = "jan@domene.no"
            };

            return ferdigattest;
        }
    }
}
