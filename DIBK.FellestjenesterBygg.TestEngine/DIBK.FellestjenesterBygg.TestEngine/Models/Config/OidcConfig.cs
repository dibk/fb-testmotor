﻿using System;
using System.Configuration;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.Config
{
    public class OidcConfig
    {
        private OidcConfig(
            Uri authority, Guid clientId, Uri redirectUri, Uri postLogoutRedirectUri, string scope, string acrValues, Guid clientSecret, Uri tokenEndpointUri)
        {
            Authority = authority;
            ClientId = clientId;
            RedirectUri = redirectUri;
            PostLogoutRedirectUri = postLogoutRedirectUri;
            Scope = scope;
            AcrValues = acrValues;
            ClientSecret = clientSecret;
            TokenEndpointUri = tokenEndpointUri;
        }

        public Uri Authority { get; private set; }
        public Guid ClientId { get; private set; }
        public Uri RedirectUri { get; private set; }
        public Uri PostLogoutRedirectUri { get; private set; }
        public string Scope { get; private set; }
        public string AcrValues { get; private set; }
        public Guid ClientSecret { get; private set; }
        public Uri TokenEndpointUri { get; private set; }

        public static OidcConfig Create()
        {
            return new OidcConfig(
                new Uri(ConfigurationManager.AppSettings["api:oidc:Authority"]),
                new Guid(ConfigurationManager.AppSettings["api:oidc:ClientId"]),
                new Uri(ConfigurationManager.AppSettings["api:oidc:RedirectUri"]),
                new Uri(ConfigurationManager.AppSettings["api:oidc:PostLogoutRedirectUri"]),
                ConfigurationManager.AppSettings["api:oidc:Scope"],
                ConfigurationManager.AppSettings["api:oidc:AcrValues"],
                new Guid(ConfigurationManager.AppSettings["api:oidc:ClientSecret"]),
                new Uri(ConfigurationManager.AppSettings["api:oidc:TokenEndpointUri"])
            );
        }
    }
}