﻿using System;
using System.Configuration;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.Config
{
    public class ApiConfig
    {
        private ApiConfig(
            Uri fellesbyggApiUrl, Uri arbeidsflytApiUrl, Uri altinnApiUrl, Guid altinnApiKey, OidcConfig oidcConfig)
        {
            FellesbyggApiUrl = fellesbyggApiUrl;
            ArbeidsflytApiUrl = arbeidsflytApiUrl;
            AltinnApiUrl = altinnApiUrl;
            AltinnApiKey = altinnApiKey;
            Oidc = oidcConfig;
        }

        public Uri FellesbyggApiUrl { get; private set; }
        public Uri ArbeidsflytApiUrl { get; private set; }
        public Uri AltinnApiUrl { get; private set; }
        public Guid AltinnApiKey { get; private set; }
        public OidcConfig Oidc { get; private set; }

        public static ApiConfig Create()
        {
            return new ApiConfig(
                new Uri(ConfigurationManager.AppSettings["api:FellesbyggApiUrl"]),
                new Uri(ConfigurationManager.AppSettings["api:ArbeidsflytApiUrl"]),
                new Uri(ConfigurationManager.AppSettings["api:AltinnApiUrl"]),
                new Guid(ConfigurationManager.AppSettings["api:AltinnApiKey"]),
                OidcConfig.Create()
            );
        }
    }
}
