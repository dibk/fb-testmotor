using System;
using no.kxml.skjema.dibk.suppleringAvSoknad;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class SuppleringAvSoeknadExampleGenerator
    {
        public SuppleringAvSoeknadType NySuppleringAvSoeknadType()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var suppleringAvSoeknad = new SuppleringAvSoeknadType();

            suppleringAvSoeknad.foelgebrev = "Denne forsendelsen inneholder litt informasjon som vi hadde liggende men glemte å sende inn og så svarer vi litt bedre på noe som ikke helt falt i smak i den opprinnelige søknaden.";

            suppleringAvSoeknad.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Storgata 19",
                        landkode = "NO",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                    },

                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = "H0201",
                    kommunenavn = "Nome"
                }
            };

            suppleringAvSoeknad.tiltakshaver = new PartType()
            {
                partstype = new KodeType { kodeverdi = "Privatperson", kodebeskrivelse = "Privatperson" },
                foedselsnummer = "28044102120",
                navn = "Ludvig Moholt",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 1",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "12345678",
                mobilnummer = "87654321",
                epost = "navn@domene.no",
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Truls Trulsen",
                    telefonnummer = "12312345",
                    mobilnummer = "12312312",
                    epost = "truls@trulsen.no"
                }
            };

            // ansvarlig soeker
            suppleringAvSoeknad.ansvarligSoeker = new PartType
            {
                partstype = new KodeType
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910065203", // 914994780 Arkitektum
                navn = "ARKITEKT FLINK",
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Benjamin Fjell",
                    epost = "benjamin@fjell.no",
                    mobilnummer = "99977788",
                    telefonnummer = "55500055"
                },
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Lillegata 5",
                    postnr = "7003",
                    poststed = "Trondheim"
                },
                telefonnummer = "11223344",
                mobilnummer = "90909090",
                epost = "ftbtest@yahoo.com"
            };

            suppleringAvSoeknad.ettersending = new[]
            {
                new EttersendingType()
                {
                    kommentar = "Ettersending nr.1",
                    tema = new KodeType() {kodebeskrivelse = "Andre forhold", kodeverdi = "andreForhold"},
                    tittel = "Nr.1",
                    vedlegg = new[]
                    {
                        new VedleggType()
                        {
                            filnavn = "nr1.pdf",
                            vedleggstype = new KodeType() {kodebeskrivelse = "Andre redegjørelser", kodeverdi = "AndreRedegjoerelser"},
                        }
                    }
                },
                new EttersendingType()
                {
                    kommentar = "Ettersending nr.2",
                    tema = new KodeType() {kodebeskrivelse = "Andre forhold", kodeverdi = "andreForhold"},
                    tittel = "Nr.2",
                    vedlegg = new[]
                    {
                        new VedleggType()
                        {
                            filnavn = "nr2.pdf",
                            vedleggstype = new KodeType() {kodebeskrivelse = "Andre redegjørelser", kodeverdi = "AndreRedegjoerelser"},
                        }
                    },
                    underskjema = new []
                    {
                        new KodeType() {kodebeskrivelse = "Vedleggsopplysninger", kodeverdi = "Vedleggsopplysninger"}
                    }

                }
            };

            suppleringAvSoeknad.mangelbesvarelse = new[]
            {
                new MangelbesvarelseType()
                {
                    tema = new KodeType() {kodebeskrivelse = "Atkomst", kodeverdi = "atkomst"},
                    mangel = new MangeltekstType()
                    {
                        mangelId = "80b21f8e-a5e3-401b-8e45-39c9c5249b8e",
                        mangeltekst = new KodeType()
                            {kodebeskrivelse = "Avkjørsel er ikke inntegnet", kodeverdi = "1.35"}
                    },
                    tittel = "Manglende tegning for avkjørsel",
                    erMangelBesvart = true,
                    erMangelBesvartSpecified = true,
                    erMangelBesvaresSenere = false,
                    erMangelBesvaresSenereSpecified = true,
                    kommentar =
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac lectus vel arcu ornare accumsan vel vel massa. Vestibulum dictum " +
                        "neque nec dui faucibus efficitur. Quisque urna odio, ullamcorper a diam a, egestas maximus odio. Nullam lacinia consequat neque sed" +
                        " pharetra. Nunc congue massa arcu, vel tincidunt arcu sollicitudin vitae. Mauris nunc lectus, bibendum vel ante sed, efficitur " +
                        "varius justo. Quisque rhoncus lacinia lacinia. Nulla tincidunt eros sed sapien semper, a tincidunt ante volutpat. Ut vestibulum in " +
                        "massa nec condimentum. Aenean finibus porta scelerisque. Fusce a nisl sodales, euismod ante ornare, varius turpis. Sed finibus " +
                        "magna malesuada magna elementum placerat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. " +
                        "Fusce ultrices rutrum gravida."
                },
                new MangelbesvarelseType()
                {
                    tema = new KodeType() {kodebeskrivelse = "Plan", kodeverdi = "plan"},
                    mangel = new MangeltekstType()
                    {
                        mangelId = "80b21f8e-a5e3-401b-8e45-249b8e39c9c5",
                        mangeltekst = new KodeType()
                            {kodebeskrivelse = "Dispensasjon fra krav om regulering", kodeverdi = "3.50"}
                    },
                    tittel = "Manglende tegning for avkjørsel",
                    erMangelBesvart = false,
                    erMangelBesvartSpecified = true,
                    erMangelBesvaresSenere = true,
                    erMangelBesvaresSenereSpecified = true,
                    kommentar =
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    underskjema = new []
                    {
                        new KodeType() {kodebeskrivelse = "Gjennomføringsplan", kodeverdi = "Gjennomfoeringsplan"},
                        new KodeType() {kodebeskrivelse = "Vedleggsopplysninger", kodeverdi = "Vedleggsopplysninger"}
                    },
                    vedlegg = new[]
                    {
                        new VedleggType()
                        {
                            filnavn = "nr2.pdf",
                            vedleggstype = new KodeType() {kodebeskrivelse = "Andre redegjørelser", kodeverdi = "AndreRedegjoerelser"},
                        }
                    }
                },
                new MangelbesvarelseType()
                {
                    mangel = new MangeltekstType()
                    {
                        mangelId = "e8e4bb52-1c8d-4122-8695-48749012a564",
                        mangeltekst = new KodeType()
                            {kodebeskrivelse = "Areal for parkering mangler", kodeverdi = "1.41"}
                    },
                    tittel = "Situasjonsplan er uadekvat i beskrivelsen av parekeringsareal",
                    erMangelBesvart = true,
                    erMangelBesvartSpecified = true,
                    erMangelBesvaresSenere = false,
                    erMangelBesvaresSenereSpecified = true,
                    kommentar =
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    vedlegg = new[]
                    {
                        new VedleggType()
                        {
                            filnavn = "nr2.pdf",
                            vedleggstype = new KodeType() {kodebeskrivelse = "Situasjonsplan", kodeverdi = "Situasjonsplan"},
                            versjonsdato = DateTime.Now,
                            versjonsdatoSpecified = true,
                            versjonsnummer = "I405.SD"
                        }
                    }
                }
            };

            suppleringAvSoeknad.kommunensSaksnummer = new SaksnummerType()
            {
                saksaar = "2021",
                sakssekvensnummer = "42"
            };

            suppleringAvSoeknad.metadata = new MetadataType()
            {
                fraSluttbrukersystem = "Fellestjenester bygg testmotor",
                ftbId = "c3ca1cc6-5945-4e26-a410-6624e93ab7a4",
                prosjektnavn = "Johansens Taj Mahal",
                sluttbrukersystemUrl = "",
                klartForSigneringFraSluttbrukersystem = true,
                klartForSigneringFraSluttbrukersystemSpecified = true
            };

            return suppleringAvSoeknad;
        }
    }
}