﻿using no.kxml.skjema.dibk.igangsettingstillatelse;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class IgangsettingstillatelseExampleGenerator
    {
        public IgangsettingstillatelseType NyIgangsettingstillatelseType()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            var igangsettingstillatelse = new IgangsettingstillatelseType();
            igangsettingstillatelse.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            igangsettingstillatelse.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2015",
                sakssekvensnummer = "124340"
            };

            var eiendomByggestedEiendomsidentifikasjon = new MatrikkelnummerType
            {
                kommunenummer = rand.GetRandomTestKommuneNummer(),
                gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                festenummer = "0",
                seksjonsnummer = "0"
            };

            var eiendomByggestedAdresse = new EiendommensAdresseType
            {
                adresselinje1 = "Storgata 3",
                postnr = "7003",
                poststed = "Trondheim"
            };

            var eiendomIdx0 = new EiendomType
            {
                eiendomsidentifikasjon = eiendomByggestedEiendomsidentifikasjon,
                adresse = eiendomByggestedAdresse,
                bygningsnummer = "123456789",
                bolignummer = "H0101"
            };

            igangsettingstillatelse.eiendomByggested = new[]
            {
                eiendomIdx0
            };

            igangsettingstillatelse.beroererArbeidsplasser = true;

            igangsettingstillatelse.gjelderHeleTiltaket = false;

            var igangsettingstillatelseDelsoeknaderIdx0 = new DelsoeknadIgangsettingType
            {
                kommunensSaksnummer = new SaksnummerType
                {
                    saksaar = "2015",
                    sakssekvensnummer = "123496"
                },
                delAvTiltaket = "Del av tiltaket -- trapp",
                tillatelsedato = new System.DateTime(2016, 11, 11),
                tillatelsedatoSpecified = true,
                kommentar = "Trappen er på plass"
            };
            var igangsettingstillatelseDelsoeknaderIdx1 = new DelsoeknadIgangsettingType
            {
                kommunensSaksnummer = new SaksnummerType
                {
                    saksaar = "2015",
                    sakssekvensnummer = "123497"
                },
                delAvTiltaket = "Del av tiltaket -- fyrtårn",
                tillatelsedato = new System.DateTime(2016, 10, 11),
                tillatelsedatoSpecified = true,
                kommentar = "Dette går litt tregt"
            };

            igangsettingstillatelse.delsoeknader = new []
            {
                igangsettingstillatelseDelsoeknaderIdx0,
                igangsettingstillatelseDelsoeknaderIdx1,
            };

            igangsettingstillatelse.delAvTiltaket = "Denne søknaden gjelder overbygg, påbygg og bod";

            igangsettingstillatelse.ansvarligSoeker =
                new PartType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    organisasjonsnummer = "974702665", // 974702665 DIBK Oslo
                    navn = "Byggmester Bob",
                    kontaktperson = "Hans Hansen",
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Lillegata 5",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "hansen@domene.no"
                };
            
            return igangsettingstillatelse;
        } 
    }
}