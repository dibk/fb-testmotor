﻿using no.kxml.skjema.dibk.gjenpartnabovarsel;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class GjenpartNabovarselExampleGenerator
    {
        public NabovarselType NyttGjenpartNabovarsel()
        {

            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var gjenpartNabovarsel = new NabovarselType();

            var adresseTilHansen = new EnkelAdresseType()
            {
                adresselinje1 = "Storgata 5",
                postnr = "7003",
                poststed = "Trondheim"
            };

            gjenpartNabovarsel.eiendomByggested = new[]
                {
                    new EiendomType()
                    {
                        adresse = new EiendommensAdresseType()
                        {
                        adresselinje1 = "Storgata 19",
                        landkode = "NO",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                        },
                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                            bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                            festenummer = "1",
                            seksjonsnummer = "1",
                            kommunenummer = rand.GetRandomTestKommuneNummer()
                        }
                    }
                };

            gjenpartNabovarsel.beskrivelseAvTiltak = new[]
            {
                new TiltakType()
                {
                    bruk = new FormaalType
                    {
                        tiltaksformaal = new []
                        {
                            new KodeType()
                            {
                                kodeverdi = "Annet",
                                kodebeskrivelse = "Annet"
                            },
                        },
                        beskrivPlanlagtFormaal = "",
                    },
                    type = new [] {
                        new KodeType()
                        {
                            kodeverdi = "fasade",
                            kodebeskrivelse = "Endring av bygg - utvendig - Fasade"
                        }
                    },
                    foelgebrev = "foelgebrev"
                }
            };


            gjenpartNabovarsel.rammebetingelser = new RammerType()
            {
                arealdisponering = new ArealdisponeringType()
                {
                    gjeldendePlan = new PlanType()
                    {
                        plantype = new KodeType()
                        {
                            kodeverdi = "RP",
                            kodebeskrivelse = "Reguleringsplan"
                        },
                        navn = "rammebetingelser navn"
                    }
                }
            };

            gjenpartNabovarsel.tiltakshaver = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                navn = "Delilah Kvittingfoss",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Roald Dalhs gate 4",
                    poststed = "Skien",
                    postnr = "3701"
                },
                telefonnummer = "46576879",
                mobilnummer = "99009900",
                epost = "dh@domene.no"
            };

            gjenpartNabovarsel.naboeier = new[]
            {
                new NaboGjenboerType()
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = "Privatperson",
                        kodebeskrivelse = "Privatperson"
                    },

                    navn = "Ole Olsen",
                    foedselsnummer = "08117000290",

                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Storgata 3",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },

                    gjelderNaboeiendom = new EiendomType()
                    {
                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            gaardsnummer = "109",
                            bruksnummer = "1",
                            kommunenummer = "1601",
                            seksjonsnummer = "1"
                        },
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = "Storgata 3",
                            postnr = "7003",
                            poststed = "Trondheim"
                        }
                    }
                },

            };

            gjenpartNabovarsel.kontaktPerson = new PartType()
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = "",
                        kodebeskrivelse = ""
                    },
                    navn = "Ole Olsen",
                    foedselsnummer = "08117000290",

                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Storgata 3",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
            };
            
            gjenpartNabovarsel.dispensasjon = new DispensasjonType()
            {
                dispensasjonstype = new[]
                    {
                        new KodeType()
                        {
                            kodeverdi = "PLAN",
                            kodebeskrivelse = "Arealplaner"
                        }
                    }
            };


            return gjenpartNabovarsel;
        }
    }
}
