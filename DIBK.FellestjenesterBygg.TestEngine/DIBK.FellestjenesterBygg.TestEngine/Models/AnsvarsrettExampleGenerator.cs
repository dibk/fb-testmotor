﻿using no.kxml.skjema.dibk.ansvarsrett;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class AnsvarsrettExampleGenerator
    {
        public ErklaeringAnsvarsrettType NyErklaeringAnsvarsrett()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var erklaeringAnsvarsrett = new ErklaeringAnsvarsrettType();

            erklaeringAnsvarsrett.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            erklaeringAnsvarsrett.hovedinnsendingsnummer = "44556677";

            erklaeringAnsvarsrett.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2016",
                sakssekvensnummer = "3456"
            };

            erklaeringAnsvarsrett.kommunensSaksnummer.saksaar = "2016";
            erklaeringAnsvarsrett.kommunensSaksnummer.sakssekvensnummer = "231";

            erklaeringAnsvarsrett.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer =  rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 1",
                        postnr = "3800",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    bygningsnummer = "80466985",
                    bolignummer = "H0102"
                }
            };

            erklaeringAnsvarsrett.ansvarsrett = new AnsvarsrettType()
            {

                foretak = new ForetakType()
                {
                    kontaktperson = "Jens Jensen",
                    harSentralGodkjenning = true,
                    partstype = new KodeType()
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    organisasjonsnummer = "974760223",   // 974760223 DIBK
                    navn = "Nordmann Bygg og Anlegg AS",
                    adresse = new EnkelAdresseType
                    {
                        adresselinje1 = "Bøgata 16",
                        postnr = "3802",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    telefonnummer = "12345678",
                    mobilnummer = "98765432",
                    epost = "tor@arkitektum.no",
                    signaturdato = new System.DateTime(2016, 12, 05),
                    signaturdatoSpecified = true,
                },

                ansvarsomraader = new[]
                {
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                    vaarReferanse = System.Guid.NewGuid().ToString()
                },

                    new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for utførelse (bygning, anlegg eller konstruksjon, tekniske installasjoner)",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = false,
                    samsvarKontrollVedFerdigattest = true
                },

                    new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "SØK",
                        kodebeskrivelse = "Ansvarlig søker"
                    },
                    beskrivelseAvAnsvarsomraade = "Andre",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = false,
                    samsvarKontrollVedIgangsettingstillatelse = false,
                    samsvarKontrollVedMidlertidigBrukstillatelse = false,
                    samsvarKontrollVedFerdigattest = false
                },

                    new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for prosjektering (bygning, anlegg eller konstruksjon, tekniske installasjoner)",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = true,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true
                },
                    new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for prosjektering (bygning, anlegg eller konstruksjon, tekniske installasjoner)",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },
                    samsvarKontrollVedRammetillatelse = true,
                    samsvarKontrollVedIgangsettingstillatelse = true,
                    samsvarKontrollVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollVedFerdigattest = true,
                    dekkesOmraadeAvSentralGodkjenning = true                    
                }


            },
                erklaeringAnsvarligProsjekterende = true,
                erklaeringAnsvarligUtfoerende = true,
                erklaeringAnsvarligKontrollerende = true
            };


            erklaeringAnsvarsrett.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910297937",
                navn = "FANA OG HAFSLO REVISJON",
                kontaktperson = "Siv. Ing. Borge",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "tor@arkitektum.no"
            };

            erklaeringAnsvarsrett.signatur = new SignaturType
            {
                signaturdato = new System.DateTime(2016, 09, 08),
                signertAv = "",
                signertPaaVegneAv = "",
                signeringssteg = ""
            };


            return erklaeringAnsvarsrett;
        }
    }
}