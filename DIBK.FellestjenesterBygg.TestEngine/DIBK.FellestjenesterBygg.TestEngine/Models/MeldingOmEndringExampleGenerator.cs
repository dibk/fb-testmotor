﻿using no.kxml.skjema.dibk.meldingOmEndring;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class MeldingOmEndringExampleGenerator
    {
        ExampleDataRandomizer rand = new ExampleDataRandomizer();

        public MeldingOmEndringType NyMeldingOmEndring()
        {
            var meldingOmEndring = new MeldingOmEndringType();

            meldingOmEndring.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            meldingOmEndring.prosjektnavn = "FTB";

            meldingOmEndring.kommunensSaksnummer = new SaksnummerType()
            {
                saksaar = "2019",
                sakssekvensnummer = "12345"
            };

            meldingOmEndring.eiendomByggested = new[]
            {
                new EiendomType()
                {
                     kommunenavn = "Hammerfest",
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = "2004",
                        gaardsnummer = "1",
                        bruksnummer = "3",
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Båtsfjorden 6",
                        postnr = "9664",
                        poststed = "SANDØYBOTN",
                        gatenavn = "Båtsfjorden",
                        husnr = "6"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = "H0101"
                }
            };

            meldingOmEndring.endringAnsvarsretter = new EndringAnsvarsrettType
            {
                skifteAvAnsvarsrett = true,
                skifteAvAnsvarsrettSpecified = true,
                nyAnsvarsrett = false,
                nyAnsvarsrettSpecified = true,
                avslutteAnsvarsrett = false,
                avslutteAnsvarsrettSpecified = true
            };

            meldingOmEndring.endringAnsvarligSoekerTiltakshaver = new EndringSoekerType
            {
                endringAnsvarligSoeker = true,
                endringAnsvarligSoekerSpecified = true,
                endringTiltakshaver = false,
                endringTiltakshaverSpecified = true
            };
            meldingOmEndring.beskrivelseAvEndring = "Valgfritt felt for nærmere beskrivelse av endring";

            meldingOmEndring.tiltakshaver = new PartType
            {
                partstype = new KodeType
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                foedselsnummer = "25125401530",
                navn = "Filip Mohamed",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Storgata 5",
                    postnr = "7003",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "11223344",
                mobilnummer = "87654321",
                epost = "navn@domene.no",
                signaturdato = new System.DateTime(2016, 07, 28),
                signaturdatoSpecified = true
            };

            meldingOmEndring.ansvarligSoeker =
                new PartType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    organisasjonsnummer = "910297937",
                    navn = "FANA OG HAFSLO REVISJON",
                    kontaktperson = "Benjamin Fjell",
                    adresse = new EnkelAdresseType
                    {
                        adresselinje1 = "Lillegata 5",
                        postnr = "7003",
                        poststed = "Bø i Telemark"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "tine@arkitektum.no",
                    signaturdato = new System.DateTime(2016, 07, 26),
                    signaturdatoSpecified = true
                };

            return meldingOmEndring;
        }
    }
}