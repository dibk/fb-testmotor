﻿using System;
using no.kxml.skjema.dibk.nabovarselV2;
using EnkelAdresseType = no.kxml.skjema.dibk.nabovarsel.EnkelAdresseType;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class NabovarselExampleGeneratorV2list
    {
        public no.kxml.skjema.dibk.nabovarselV2.NabovarselType NyttNabovarsel()
        {
            var rand = new ExampleDataRandomizer();
            var nabovarsel = new no.kxml.skjema.dibk.nabovarselV2.NabovarselType();


            nabovarsel.eiendomByggested = new[]
            {
                new no.kxml.skjema.dibk.nabovarselV2.EiendomType() 
                {
                    adresse = new no.kxml.skjema.dibk.nabovarselV2.EiendommensAdresseType
                    {
                        adresselinje1 = "Storgata 19",
                        landkode = "NO",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                    },
                    eiendomsidentifikasjon = new no.kxml.skjema.dibk.nabovarselV2.MatrikkelnummerType
                    {
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "1",
                        seksjonsnummer = "1",
                        kommunenummer = rand.GetRandomTestKommuneNummer()
                    }
                }
            };


            nabovarsel.beskrivelseAvTiltak = new[]
            {
                new no.kxml.skjema.dibk.nabovarselV2.TiltakType
                {
                    bruk = new[]
                    {
                        new no.kxml.skjema.dibk.nabovarselV2.KodeType
                        {
                            kodeverdi = "Annet",
                            kodebeskrivelse = "Annet"
                        }
                    },
                    type = new[]
                    {
                        new no.kxml.skjema.dibk.nabovarselV2.KodeType
                        {
                            kodeverdi = "fasade",
                            kodebeskrivelse = "Endring av bygg - utvendig - Fasade"
                        }
                    },
                    foelgebrev = "foelgebrev"
                }
            };


            nabovarsel.rammebetingelser = new no.kxml.skjema.dibk.nabovarselV2.RammerType
            {
                arealdisponering = new no.kxml.skjema.dibk.nabovarselV2.ArealdisponeringType
                {
                    gjeldendePlan = new no.kxml.skjema.dibk.nabovarselV2.PlanType
                    {
                        plantype = new no.kxml.skjema.dibk.nabovarselV2.KodeType
                        {
                            kodeverdi = "RP",
                            kodebeskrivelse = "Reguleringsplan"
                        },
                        navn = "rammebetingelser navn"
                    }
                }
            };

            nabovarsel.tiltakshaver = new no.kxml.skjema.dibk.nabovarselV2.PartType
            {
                partstype = new no.kxml.skjema.dibk.nabovarselV2.KodeType
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                foedselsnummer = "10023000208",
                navn = "Tor Hammer",
                adresse = new no.kxml.skjema.dibk.nabovarselV2.EnkelAdresseType
                {
                    adresselinje1 = "Roald Dalhs gate 4",
                    poststed = "Skien",
                    postnr = "3701"
                },
                telefonnummer = "46576879",
                mobilnummer = "99009900",
                epost = "tor@arkitektum.no",
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Kari Nordmann",
                    telefonnummer = "12344567",
                    mobilnummer = "98945777",
                    epost = "tor@arkitektum.no"
                }
            };



            nabovarsel.naboeier = new[]
            {
                new no.kxml.skjema.dibk.nabovarselV2.NaboGjenboerType()
                {
                  partstype  = new no.kxml.skjema.dibk.nabovarselV2.KodeType()
                  {
                      kodebeskrivelse = "Privatperson",
                      kodeverdi = "Privatperson"
                  },
                  navn = "Vigdis Vater",
                  foedselsnummer = "10019000953",

                  adresse = new no.kxml.skjema.dibk.nabovarselV2.EnkelAdresseType()
                  {
                      adresselinje1 = "Storgata 3",
                      postnr = "7003",
                      poststed = "Trondheim"
                  },
                  gjelderNaboeiendom = new no.kxml.skjema.dibk.nabovarselV2.EiendomType()
                  {
                        eiendomsidentifikasjon = new no.kxml.skjema.dibk.nabovarselV2.MatrikkelnummerType
                        {
                            gaardsnummer = "109",
                            bruksnummer = "1",
                            kommunenummer = "5001",
                            seksjonsnummer = "1"
                        },
                        adresse = new no.kxml.skjema.dibk.nabovarselV2.EiendommensAdresseType
                        {
                            adresselinje1 = "Storgata 3",
                            postnr = "7003",
                            poststed = "Trondheim"
                        }
                   }
                },

                new no.kxml.skjema.dibk.nabovarselV2.NaboGjenboerType()
                {
                    partstype  = new no.kxml.skjema.dibk.nabovarselV2.KodeType()
                    {
                        kodebeskrivelse = "Privatperson",
                        kodeverdi = "Privatperson"
                    },
                    navn = "Tom Tømrer",
                    foedselsnummer = "10028600254",

                    adresse = new no.kxml.skjema.dibk.nabovarselV2.EnkelAdresseType()
                    {
                        adresselinje1 = "Storgata 3",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    gjelderNaboeiendom = new no.kxml.skjema.dibk.nabovarselV2.EiendomType()
                    {
                        eiendomsidentifikasjon = new no.kxml.skjema.dibk.nabovarselV2.MatrikkelnummerType
                        {
                            gaardsnummer = "109",
                            bruksnummer = "1",
                            kommunenummer = "5001",
                            seksjonsnummer = "1"
                        },
                        adresse = new no.kxml.skjema.dibk.nabovarselV2.EiendommensAdresseType
                        {
                            adresselinje1 = "Storgata 3",
                            postnr = "7003",
                            poststed = "Trondheim"
                        }
                    }
                },

                new no.kxml.skjema.dibk.nabovarselV2.NaboGjenboerType()
                {
                    partstype  = new no.kxml.skjema.dibk.nabovarselV2.KodeType()
                    {
                        kodebeskrivelse = "Foretak",
                        kodeverdi = "Foretak"
                    },
                    navn = "GRAV OG SPRENG",
                    organisasjonsnummer = "910065157",

                    adresse = new no.kxml.skjema.dibk.nabovarselV2.EnkelAdresseType()
                    {
                        adresselinje1 = "Foretningsgata 234",
                        postnr = "9520",
                        poststed = "KAUTOKEINO"
                    },
                    gjelderNaboeiendom = new no.kxml.skjema.dibk.nabovarselV2.EiendomType()
                    {
                        eiendomsidentifikasjon = new no.kxml.skjema.dibk.nabovarselV2.MatrikkelnummerType
                        {
                            gaardsnummer = "109",
                            bruksnummer = "2",
                            kommunenummer = "5001",
                            seksjonsnummer = "1"
                        },
                        adresse = new no.kxml.skjema.dibk.nabovarselV2.EiendommensAdresseType
                        {
                            adresselinje1 = "Storgata 2",
                            postnr = "7003",
                            poststed = "Trondheim"
                        }
                    }
                }
            };



            nabovarsel.ansvarligSoeker = new no.kxml.skjema.dibk.nabovarselV2.PartType
            {
                partstype = new no.kxml.skjema.dibk.nabovarselV2.KodeType
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910065203",
                navn = "ARKITEKT FLINK",
                telefonnummer = "12345678",
                mobilnummer = "99009900",
                epost = "tor@arkitektum.no"
            };

            nabovarsel.dispensasjon = new no.kxml.skjema.dibk.nabovarselV2.DispensasjonType
            {
                dispensasjonstype = new[]
                {
                    new no.kxml.skjema.dibk.nabovarselV2.KodeType
                    {
                        kodeverdi = "PLAN",
                        kodebeskrivelse = "Arealplaner"
                    }
                }
            };


            nabovarsel.beskrivPlanlagtFormaal = "Det søkes om fasadeendring av gitt bygg som beskrevet i vedlagt materiale.";




            nabovarsel.signatur = new no.kxml.skjema.dibk.nabovarselV2.SignaturType
            {
                signaturdato = new DateTime(2016, 09, 08),
                signertAv = "",
                signertPaaVegneAv = "",
                signeringssteg = ""
            };
            nabovarsel.fraSluttbrukersystem = "Fellestjenester bygg testmotor";

            return nabovarsel;
        }
    }
}