using no.kxml.skjema.dibk.distribusjon_ansvarsrett;
using no.kxml.skjema.dibk.distribusjonKontrollOgSamsvarserklaering;
using no.kxml.skjema.dibk.soeknadpersonligansvarsrett;
using System.Collections.Generic;
using System.Configuration;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class FormService
    {
        public Skjema GetForm(int servicecode, int serviceedition)
        {
            List<Skjema> forms = GetForms();
            Skjema form = forms.Find(f => f.ServiceCode == servicecode && f.ServiceEditionCode == serviceedition);

            return form;
        }

        public Skjema GetFormSampleData(string DataFormatID, string DataFormatVersion)
        {
            List<Skjema> forms = GetForms(true, false, false);
            Skjema form = forms.Find(f => f.DataFormatID == DataFormatID && f.DataFormatVersion == DataFormatVersion);

            return form;
        }

        public List<Vedlegg> GetVedlegg(int servicecode, int serviceedition)
        {
            Vedlegg attachmentSituasjonsplan = new AttachmentExampleGenerator().AttachmentSituasjonsplan();
            Vedlegg attachmentNyTegningPlan = new AttachmentExampleGenerator().AttachmentNyTegningPlan();
            //Vedlegg attachmentDispensasjonssoeknad = new AttachmentExampleGenerator().AttachmentDispensasjonssoeknad();
            Vedlegg attchmentKartA = new AttachmentExampleGenerator().AttachmentKartA();
            Vedlegg attchmentKartB = new AttachmentExampleGenerator().AttachmentKartB();

            List<Vedlegg> Vedlegg = new List<Vedlegg>();
            if (servicecode == 4373) //Søknad om tiltak uten ansvarsrett
            {
                Vedlegg.Add(attachmentSituasjonsplan);
                Vedlegg.Add(attachmentNyTegningPlan);
            }

            //Søknad om rammetillatelse, Søknad om tillatelse i ett trinn, Søknad om igangsettingstillatelse, Søknad om midlertidig brukstillatelse, Søknad om ferdigattest
            if (servicecode == 4397 || servicecode == 4528 || servicecode == 4401 || servicecode == 4399 ||
                servicecode == 4400)
            {
                Vedlegg.Add(attachmentSituasjonsplan);
                Vedlegg.Add(attachmentNyTegningPlan);
                Vedlegg.Add(attchmentKartA);
                Vedlegg.Add(attchmentKartB);
            }

            return Vedlegg;
        }

        public List<Skjema> GetForms()
        {
            return GetForms(false, false, false);
        }

        public List<Skjema> GetForms(bool data, bool attachments, bool formCollection)
        {
            var origValidationApi = new ValidationApiUrl
            {
                Url = ConfigurationManager.AppSettings["api:ArbeidsflytApiUrl"] + ConfigurationManager.AppSettings["arbeidsflytValidationAPI"],
                Type = ValidationApiType.Original
            };

            var validationApi = new ValidationApiUrl
            {
                Url = ConfigurationManager.AppSettings["validationAPI"],
                Type = ValidationApiType.New
            };

            List<Skjema> forms = new List<Skjema>();

            Vedlegg attachmentSituasjonsplan = new AttachmentExampleGenerator().AttachmentSituasjonsplan();
            Vedlegg attachmentNyPlan = new AttachmentExampleGenerator().AttachmentNyTegningPlan();
            Vedlegg attachmentDispensasjonssoeknad = new AttachmentExampleGenerator().AttachmentDispensasjonssoeknad();
            Vedlegg attchmentKartA = new AttachmentExampleGenerator().AttachmentKartA();
            Vedlegg attchmentKartB = new AttachmentExampleGenerator().AttachmentKartB();

            // Predefinere noen skjema som kann legges inn som skjemasett ... <Gjennomføringsplan>
            no.kxml.skjema.dibk.gjennomforingsplan.GjennomfoeringsplanType gjennomforingsplanSkjema =
                new GjennomforingsplanExampleGenerator().NyGjenomforingsplan();

            SkjemaSet gjennomfoeringsPlanSkjemaSet = new SkjemaSet("Gjennomføringsplan", "5502", "41793",
                Serialize(gjennomforingsplanSkjema));

            // På ett trinn, legg ved gjennomføringsplan som skjemasett

            // Tiltak uten ansvarsrett V2
            no.kxml.skjema.dibk.tiltakutenansvarsrettV2.TiltakUtenAnsvarsrettType tiltakUtenAnsvarsrettV2 =
                new TiltakutenansvarsrettV2ExampleGenerator().NyttTiltakUtenAnsvarsrett();

            var tiltakutenansvarsrettForm2 = new Skjema
            {
                FormId = "5153",
                ServiceCode = 4373,
                ServiceEditionCode = 2,
                DataFormatID = "5853",
                DataFormatVersion = "43137",
                Name = "Søknad om tillatelse til tiltak uten ansvarsrett v2",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=31129618",
                SendTo = "Kommunen",
                FormType = "Hovedsøknad",
                Status = "Testmiljø og i produksjon"
            };

            if (data)
                tiltakutenansvarsrettForm2.Data = Serialize(tiltakUtenAnsvarsrettV2);

            forms.Add(tiltakutenansvarsrettForm2);

            // Tiltak uten ansvarsrett V3
            no.kxml.skjema.dibk.tiltakutenansvarsrettV3.TiltakUtenAnsvarsrettType tiltakUtenAnsvarsrettV3 =
                new TiltakutenansvarsrettV3ExampleGenerator().NyttTiltakUtenAnsvarsrett();

            var tiltakutenansvarsrettForm3 = new Skjema
            {
                FormId = "5153",
                ServiceCode = 4373,
                ServiceEditionCode = 3,
                DataFormatID = "6742",
                DataFormatVersion = "45753",
                Name = "Søknad om tillatelse til tiltak uten ansvarsrett v3",
                AvailableService = true,
                OpenWikiUrl = "",
                SendTo = "Kommunen",
                FormType = "Hovedsøknad",
                Status = "Under utvikling"
            };

            if (data)
                tiltakutenansvarsrettForm3.Data = Serialize(tiltakUtenAnsvarsrettV3);
            
            forms.Add(tiltakutenansvarsrettForm3);

            // Rammetillatelse V2
            no.kxml.skjema.dibk.rammesoknadV2.RammetillatelseType rammetillatelseV2 = new RammesoknadV2ExampleGenerator().NyRammetillatelse();

            var rammetillatelseV2Form = new Skjema
            {
                FormId = "5174",
                ServiceCode = 4397,
                ServiceEditionCode = 2,
                DataFormatID = "5693",
                DataFormatVersion = "42425",
                Name = "Søknad om rammetillatelse V2",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=31129620",
                SendTo = "Kommunen",
                FormType = "Hovedsøknad",
                Status = "Testmiljø og i produksjon"
            };

            if (data)
                rammetillatelseV2Form.Data = Serialize(rammetillatelseV2);
            
            if (attachments)
                rammetillatelseV2Form.Vedlegg = new List<Vedlegg> { attachmentSituasjonsplan, attachmentNyPlan };

            if (formCollection)
                rammetillatelseV2Form.SkjemaSet = new List<SkjemaSet> { gjennomfoeringsPlanSkjemaSet };
            
            forms.Add(rammetillatelseV2Form);

            //// Rammetillatelse V3
            no.kxml.skjema.dibk.rammesoknadV3.RammetillatelseType rammetillatelseV3 = new RammesoknadV3ExampleGenerator().NyRammetillatelse();

            var rammetillatelseV3Form = new Skjema
            {
                FormId = "5174",
                ServiceCode = 4397,
                ServiceEditionCode = 3,
                DataFormatID = "6741",
                DataFormatVersion = "45752",
                Name = "Søknad om rammetillatelse V3",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=31129620",
                SendTo = "Kommunen",
                FormType = "Hovedsøknad",
                Status = "Under utvikling"
            };

            if (data)
                rammetillatelseV3Form.Data = Serialize(rammetillatelseV3);
            
            if (attachments)
                rammetillatelseV3Form.Vedlegg = new List<Vedlegg>{ attachmentSituasjonsplan, attachmentNyPlan };

            if (formCollection)
                rammetillatelseV3Form.SkjemaSet = new List<SkjemaSet> { gjennomfoeringsPlanSkjemaSet };

            forms.Add(rammetillatelseV3Form);

            // Ett-trinns-søknad V2
            no.kxml.skjema.dibk.etttrinnsoknadV2.EttTrinnType etttrinnsoknadV2 = new EttTrinnsoknadV2ExampleGenerator().NyEttTrinnSoknad();

            var ettTrinnssoknadV2Form = new Skjema
            {
                FormId = "5174",
                ServiceCode = 4528,
                ServiceEditionCode = 2,
                DataFormatID = "5694",
                DataFormatVersion = "42434",
                Name = "Søknad om tillatelse i ett trinn V2",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=31129622",
                SendTo = "Kommunen",
                FormType = "Hovedsøknad",
                Status = "Testmiljø og i produksjon"
            };

            if (data)
                ettTrinnssoknadV2Form.Data = Serialize(etttrinnsoknadV2);

            if (attachments)
            {
                ettTrinnssoknadV2Form.Vedlegg = new List<Vedlegg>
                {
                    attachmentSituasjonsplan,
                    attachmentNyPlan,
                    attachmentDispensasjonssoeknad,
                    attchmentKartA,
                    attchmentKartB
                };
            }

            if (formCollection)
                ettTrinnssoknadV2Form.SkjemaSet = new List<SkjemaSet> { gjennomfoeringsPlanSkjemaSet };

            forms.Add(ettTrinnssoknadV2Form);

            //// Ett-trinns-søknad V3
            no.kxml.skjema.dibk.etttrinnsoknadV3.EttTrinnType etttrinnsoknadV3 = new EttTrinnsoknadV3ExampleGenerator().NyEttTrinnSoknad();
            var ettTrinnssoknadV3Form = new Skjema
            {
                FormId = "5174",
                ServiceCode = 4528,
                ServiceEditionCode = 3,
                DataFormatID = "6740",
                DataFormatVersion = "45751",
                Name = "Søknad om tillatelse i ett trinn V3",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=31129622",
                SendTo = "Kommunen",
                FormType = "Hovedsøknad",
                Status = "Under utvikling"
            };

            if (data)
                ettTrinnssoknadV3Form.Data = Serialize(etttrinnsoknadV3);
            
            if (attachments)
            {
                ettTrinnssoknadV3Form.Vedlegg = new List<Vedlegg>
                {
                    attachmentSituasjonsplan,
                    attachmentNyPlan,
                    attachmentDispensasjonssoeknad,
                    attchmentKartA,
                    attchmentKartB
                };
            }

            if (formCollection)
                ettTrinnssoknadV3Form.SkjemaSet = new List<SkjemaSet> { gjennomfoeringsPlanSkjemaSet };

            forms.Add(ettTrinnssoknadV3Form);

            // Supplering av søknad
            no.kxml.skjema.dibk.suppleringAvSoknad.SuppleringAvSoeknadType suppleringAvSoeknad =
                new SuppleringAvSoeknadExampleGenerator().NySuppleringAvSoeknadType();

            var suppleringAvSoeknadForm = new Skjema
            {
                FormId = "",
                ServiceCode = 5721,
                ServiceEditionCode = 1,
                DataFormatID = "6962",
                DataFormatVersion = "46471",
                Name = "Supplering av søknad",
                AvailableService = true,
                OpenWikiUrl = "https://dibk.atlassian.net/wiki/spaces/FB/pages/2134671380/Supplering+av+s+knad",
                SendTo = "Kommunen",
                FormType = "Supplering til hovedsøknad",
                Status = "Under utvikling"
            };

            if (data)
                suppleringAvSoeknadForm.Data = Serialize(suppleringAvSoeknad);

            forms.Add(suppleringAvSoeknadForm);

            // Igangsettingstillatelse V2
            no.kxml.skjema.dibk.igangsettingstillatelseV2.IgangsettingstillatelseType igangsettingstillatelseV2 =
                new IgangsettingstillatelseV2ExampleGenerator().NyIgangsettingstillatelseType();

            var igangsettingstillatelseV2Form = new Skjema
            {
                FormId = "5151",
                ServiceCode = 4401,
                ServiceEditionCode = 2,
                DataFormatID = "5845",
                DataFormatVersion = "43007",
                Name = "Søknad om igangsettingstillatelse V2",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=20021280",
                SendTo = "Kommunen",
                FormType = "Hovedsøknad",
                Status = "Testmiljø og i produksjon"
            };
            
            if (data)
                igangsettingstillatelseV2Form.Data = Serialize(igangsettingstillatelseV2);

            if (formCollection)
                igangsettingstillatelseV2Form.SkjemaSet = new List<SkjemaSet> { gjennomfoeringsPlanSkjemaSet };

            forms.Add(igangsettingstillatelseV2Form);

            // Midlertidig brukstillatelse V2
            no.kxml.skjema.dibk.midlertidigbrukstillatelseV2.MidlertidigBrukstillatelseType midlertidigBrukstillatelseV2 = new MidlertidigBrukstillatelseV2ExampleGenerator().NyMidlertidigBrukstillatelse();
            var midlertidigBrukstillatelseV2Form = new Skjema
            {
                FormId = "5169",
                ServiceCode = 4399,
                ServiceEditionCode = 5,
                DataFormatID = "5844",
                DataFormatVersion = "43006",
                Name = "Søknad om midlertidig brukstillatelse V2",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=20021282",
                SendTo = "Kommunen",
                FormType = "Hovedsøknad",
                Status = "Testmiljø og i produksjon"
            };

            if (data)
                midlertidigBrukstillatelseV2Form.Data = Serialize(midlertidigBrukstillatelseV2);

            if (formCollection)
                midlertidigBrukstillatelseV2Form.SkjemaSet = new List<SkjemaSet> { gjennomfoeringsPlanSkjemaSet };

            forms.Add(midlertidigBrukstillatelseV2Form);

            // Ferdigattest versjon 2
            no.kxml.skjema.dibk.ferdigattestV2.FerdigattestType ferdigattestV2 =
                new FerdigattestV2ExampleGenerator().NyFerdigattest();
            var ferdigattestV2Form = new Skjema
            {
                FormId = "5167",
                ServiceCode = 4400,
                ServiceEditionCode = 2,
                DataFormatID = "5855",
                DataFormatVersion = "43192",
                Name = "Søknad om ferdigattest V2",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=23691266",
                SendTo = "Kommunen",
                FormType = "Hovedsøknad",
                Status = "Testmiljø og i produksjon"
            };
            
            if (data)
                ferdigattestV2Form.Data = Serialize(ferdigattestV2);

            if (formCollection)
                ferdigattestV2Form.SkjemaSet = new List<SkjemaSet> { gjennomfoeringsPlanSkjemaSet };

            forms.Add(ferdigattestV2Form);

            // Gjennomføringsplan V4
            no.kxml.skjema.dibk.gjennomforingsplanV4.GjennomfoeringsplanType gjennomforingsplanV4 =
                new GjennomforingsplanV4ExampleGenerator().NyGjennomforingsplanV4();

            var gjennomforingsplanFormV4 = new Skjema
            {
                FormId = "5185",
                ServiceCode = 4398,
                ServiceEditionCode = 5,
                DataFormatID = "5786",
                DataFormatVersion = "42725",
                Name = "Gjennomføringsplan V4",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=20021255",
                SendTo = "Kommunen",
                FormType = "Underskjema/søknad",
                Status = "Testmiljø og i produksjon"
            };
            
            if (data)
                gjennomforingsplanFormV4.Data = Serialize(gjennomforingsplanV4);

            forms.Add(gjennomforingsplanFormV4);

            //Gjennomføringsplan V6
            no.kxml.skjema.dibk.gjennomforingsplanV6.GjennomfoeringsplanType gjennomforingsplanV6 =
                new GjennomforingsplanV6ExampleGenerator().NyGjennomforingsplanV6();

            var gjennomforingsplanFormV6 = new Skjema
            {
                FormId = "5185",
                ServiceCode = 4398,
                ServiceEditionCode = 6,
                DataFormatID = "6146",
                DataFormatVersion = "44096",
                Name = "Gjennomføringsplan V6",
                AvailableService = true,
                OpenWikiUrl = "",
                SendTo = "Kommunen",
                FormType = "Underskjema/søknad",
                Status = "Under utvikling"
            };

            if (data)
                gjennomforingsplanFormV6.Data = Serialize(gjennomforingsplanV6);

            forms.Add(gjennomforingsplanFormV6);

            // Distribusjonstjeneste for erklæring om ansvarsrett
            DistribusjonAnsvarsrettType distribusjon_ansvarsrett =
                new DistribusjonAnsvarsrettExampleGenerator().NyDistribusjonAnsvarsrett();

            var distribusjon_ansvarsrettForm = new Skjema
            {
                FormId = "xx",
                ServiceCode = 4762,
                ServiceEditionCode = 1,
                DataFormatID = "5506",
                DataFormatVersion = "41798",
                Name = "Distribusjonstjeneste for erklæring om ansvarsrett",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=51391928",
                SendTo = "Ansvarlig foretak",
                FormType = "Distribusjon",
                Status = "Testmiljø og i produksjon"
            };
            
            if (data)
                distribusjon_ansvarsrettForm.Data = Serialize(distribusjon_ansvarsrett);

            forms.Add(distribusjon_ansvarsrettForm);

            // Erklæring om ansvarsrett brukt i distribusjonstjenesten
            no.kxml.skjema.dibk.ansvarsrett.ErklaeringAnsvarsrettType ansvarsrett =
                new AnsvarsrettExampleGenerator().NyErklaeringAnsvarsrett();
            var ansvarsrettForm = new Skjema
            {
                FormId = "5181",
                ServiceCode = 4419,
                ServiceEditionCode = 3,
                DataFormatID = "5505",
                DataFormatVersion = "41797",
                Name = "Erklæring om ansvarsrett (brukt i distribusjonstjenesten)",
                AvailableService = false,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=20021262",
                SendTo = "Ansvarlig søker",
                FormType = "Erklæring/vedlegg",
                Status = "Testmiljø og i produksjon"
            };

            if (data)
                ansvarsrettForm.Data = Serialize(ansvarsrett);

            forms.Add(ansvarsrettForm);

            // Erklæring om ansvarsrett direkte oppretting
            no.kxml.skjema.dibk.ansvarsrettDirekteOpprettet.ErklaeringAnsvarsrettType ansvarsrettDirekteOpprettet =
                new AnsvarsrettDirekteOpprettetExampleGenerator().NyErklaeringAnsvarsrettDirekteOpprettet();
            var ansvarsrettDirekteOpprettetForm = new Skjema
            {
                FormId = "5181",
                ServiceCode = 4965,
                ServiceEditionCode = 2,
                DataFormatID = "5700",
                DataFormatVersion = "42467",
                Name = "Erklæring om ansvarsrett (initiert fra ansvarlig foretak)",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=20021262",
                SendTo = "Ansvarlig søker",
                FormType = "Erklæring/vedlegg",
                Status = "Testmiljø. I produksjon som underskjema"
            };

            if (data)
                ansvarsrettDirekteOpprettetForm.Data = Serialize(ansvarsrettDirekteOpprettet);

            forms.Add(ansvarsrettDirekteOpprettetForm);

            // Samsvarserklæring direkte opprettet
            no.kxml.skjema.dibk.samsvarserklaeringDirekteOpprettet.SamsvarserklaeringDirekteType
                samsvarserklaeringDirekteOpprettet = new SamsvarserklaeringDirekteOpprettetExampleGenerator()
                    .NySamsvarserklaeringDirekteOpprettet();

            var samsvarserklaeringDirekteOpprettetForm = new Skjema
            {
                FormId = "5148",
                ServiceCode = 5315,
                ServiceEditionCode = 1,
                DataFormatID = "6167",
                DataFormatVersion = "44169",
                Name = "Samsvarserklæring (direkte opprettet)",
                AvailableService = true,
                OpenWikiUrl =
                    "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/40828964/Samsvarserkl+ring",
                SendTo = "Ansvarlig søker",
                FormType = "Erklæring",
                Status = "Under utvikling"
            };

            if (data)
                samsvarserklaeringDirekteOpprettetForm.Data = Serialize(samsvarserklaeringDirekteOpprettet);

            forms.Add(samsvarserklaeringDirekteOpprettetForm);

            // Kontrollerklæring direkte opprettet
            no.kxml.skjema.dibk.kontrollerklaeringDirekteOpprettet.KontrollerklaeringDirekteType
                kontrollerklaeringDirekteOpprettet = new KontrollerklaeringDirekteOpprettetExampleGenerator()
                    .NyKontrollerklaeringDirekteOpprettet();

            var kontrollerklaeringDirekteOpprettetForm = new Skjema
            {
                FormId = "5149",
                ServiceCode = 5444,
                ServiceEditionCode = 1,
                DataFormatID = "6341",
                DataFormatVersion = "45020",
                Name = "Kontrollerklæring (direkte opprettet)",
                AvailableService = true,
                OpenWikiUrl = "",
                SendTo = "Ansvarlig søker",
                FormType = "Erklæring",
                Status = "Under utvikling"
            };
            
            if (data)
                kontrollerklaeringDirekteOpprettetForm.Data = Serialize(kontrollerklaeringDirekteOpprettet);

            forms.Add(kontrollerklaeringDirekteOpprettetForm);

            // Samsvarserklæring opprettet i distribusjonstjenesten
            no.kxml.skjema.dibk.samsvarserklaeringVedDistribusjon.SamsvarserklaeringVedDistribusjonType
                samsvarserklaeringVedDistribusjon = new SamsvarserklaeringVedDistribusjonExampleGenerator()
                    .NySamsvarserklaeringVedDistribusjon();

            var samsvarserklaeringVedDistribusjonForm = new Skjema
            {
                FormId = "5148",
                ServiceCode = 5207,
                ServiceEditionCode = 1,
                DataFormatID = "5905",
                DataFormatVersion = "43432",
                Name = "Samsvarserklæring (opprettet av distribusjonstjenesten)",
                AvailableService = false,
                OpenWikiUrl =
                    "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/548503553/Distribusjonstjeneste+for+kontroll+og+samsvarserkl+ring",
                SendTo = "Ansvarlig søker",
                FormType = "Erklæring",
                Status = "Under utvikling"
            };

            if (data)
                samsvarserklaeringVedDistribusjonForm.Data = Serialize(samsvarserklaeringVedDistribusjon);

            forms.Add(samsvarserklaeringVedDistribusjonForm);

            // Kontrollerklæring opprettet i distribusjonstjenesten
            no.kxml.skjema.dibk.kontrollerklaeringVedDistribusjon.KontrollerklaeringType
                kontrollerklaeringVedDistribusjon = new KontrollerklaeringVedDistribusjonExampleGenerator()
                    .NyKontrollerklaeringVedDistribusjon();

            var kontrollerklaeringVedDistribusjonForm = new Skjema
            {
                FormId = "5149",
                ServiceCode = 5445,
                ServiceEditionCode = 1,
                DataFormatID = "6340",
                DataFormatVersion = "45019",
                Name = "Kontrollerklæring (opprettet av distribusjonstjenesten)",
                AvailableService = false,
                OpenWikiUrl =
                    "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/548503553/Distribusjonstjeneste+for+kontroll+og+samsvarserkl+ring",
                SendTo = "Ansvarlig søker",
                FormType = "Erklæring",
                Status = "Under utvikling"
            };

            if (data)
                kontrollerklaeringVedDistribusjonForm.Data = Serialize(kontrollerklaeringVedDistribusjon);

            forms.Add(kontrollerklaeringVedDistribusjonForm);

            // Distribusjonstjeneste for kontroll og Samsvarserklæring
            DistribusjonKontrollOgSamsvarserklaeringType distribusjon_kontrollOgSamsvarserklaering =
                new DistribusjonKontrollOgSamsvarserklaeringExampleGenerator()
                    .NyDistribusjonKontrollOgSamsvarserklaering();

            var distribusjon_kontrollOgSamsvarserklaeringForm = new Skjema
            {
                FormId = "xx",
                ServiceCode = 5194,
                ServiceEditionCode = 1,
                DataFormatID = "5908",
                DataFormatVersion = "43469",
                Name = "Distribusjonstjeneste for kontroll og samsvarserklæring",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=548503553",
                SendTo = "Ansvarlig foretak",
                FormType = "Distribusjon",
                Status = "Testmiljø"
            };

            if (data)
                distribusjon_kontrollOgSamsvarserklaeringForm.Data = Serialize(distribusjon_kontrollOgSamsvarserklaering);

            forms.Add(distribusjon_kontrollOgSamsvarserklaeringForm);

            // Søknad om personlig ansvarsrett
            SoeknadPersonligAnsvarsrettType personligAnsvarsrett = new PersonligAnsvarsrettExampleGenerator().NySoeknadPersonligAnsvarsrett();
            
            var personligAnsvarsrettForm = new Skjema
            {
                FormId = "5184",
                ServiceCode = 4701,
                ServiceEditionCode = 1,
                DataFormatID = "5500",
                DataFormatVersion = "41791",
                Name = "Søknad om ansvarsrett for selvbygger",
                AvailableService = true,
                TemplateUrl = "",
                WikiUrl = "",
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=51392611",
                ModelUrl = "",
                SendTo = "Kommunen",
                FormType = "Underskjema",
                Status = "Testmiljø"
            };

            if (data)
                personligAnsvarsrettForm.Data = Serialize(personligAnsvarsrett);

            if (formCollection)
                personligAnsvarsrettForm.SkjemaSet = new List<SkjemaSet> { gjennomfoeringsPlanSkjemaSet };

            forms.Add(personligAnsvarsrettForm);

            // Nabovarsel
            var nabovarselV4Form = new Skjema
            {
                FormId = "5154*",
                ServiceCode = 4655,
                ServiceEditionCode = 4,
                DataFormatID = "6303",
                DataFormatVersion = "44820",
                Name = "Distribusjon nabovarsel v4",
                AvailableService = true,
                OpenWikiUrl =
                    "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/52957874/Distribusjonstjeneste+for+nabovarsel",
                SendTo = "Naboer",
                FormType = "Distribusjon",
                Status = "Testmiljø"
            };
            
            if (data)
                nabovarselV4Form.Data = Serialize(new NabovarselExampleGeneratorV4().NyttNabovarsel());

            forms.Add(nabovarselV4Form);

            // Gjenpart nabovarsel V2
            var gjenpartnabovarselFormV2 = new Skjema
            {
                FormId = "5155*",
                ServiceCode = 4816,
                ServiceEditionCode = 2,
                DataFormatID = "5826",
                DataFormatVersion = "42895",
                Name = "Gjenpart nabovarsel V2",
                AvailableService = false,
                OpenWikiUrl = "",
                SendTo = "Kommunen",
                FormType = "Underskjema/vedlegg",
                Status = "Testmiljø"
            };

            if (data)
                gjenpartnabovarselFormV2.Data = Serialize(new GjenpartNabovarselV2ExampleGenerator().NyttGjenpartNabovarselV2());

            forms.Add(gjenpartnabovarselFormV2);

            // Endring av tillatelse
            var endringAvTillatelseForm = new Skjema
            {
                FormId = "5168",
                ServiceCode = 4402,
                ServiceEditionCode = 1,
                DataFormatID = "5689",
                DataFormatVersion = "42350",
                Name = "Søknad om endring av gitt tillatelse eller godkjenning",
                AvailableService = true,
                OpenWikiUrl =
                    "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/133365793/S+knad+om+endring+av+gitt+tillatelse",
                SendTo = "Kommunen",
                FormType = "Søknad",
                Status = "Testmiljø og i produksjon"
            };

            if (data)
                endringAvTillatelseForm.Data = Serialize((new EndringAvTillatelseExampleGenerator()).NyEndringAvTillatelse());

            forms.Add(endringAvTillatelseForm);

            // Melding om endring av ansvarsretter/ ansvarlig søker
            var meldingOmEndringForm = new Skjema
            {
                FormId = "5186",
                ServiceCode = 5298,
                ServiceEditionCode = 1,
                DataFormatID = "6108",
                DataFormatVersion = "44054",
                Name = "Melding om endring av ansvarsretter/ ansvarlig søker",
                AvailableService = true,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/646152199/Melding+om+endring+av+ansvarsrett+ansvarlig+s+ker",
                SendTo = "Kommunen",
                FormType = "Melding",
                Status = "Testmiljø/ under utvikling"
            };
            
            if (data)
                meldingOmEndringForm.Data = Serialize((new MeldingOmEndringExampleGenerator()).NyMeldingOmEndring());

            forms.Add(meldingOmEndringForm);

            // Arbeidstilsynets samtykke
            var arbeidstilsynetYrkesbygg = new Skjema
            {
                FormId = "5177",
                ServiceCode = 4845,
                ServiceEditionCode = 1,
                DataFormatID = "5547",
                DataFormatVersion = "41999",
                Name = "Søknad om Arbeidstilsynets samtykke",
                AvailableService = true,
                TemplateUrl = "",
                WikiUrl = "",
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/pages/viewpage.action?pageId=52269827",
                ModelUrl = "",
                SendTo = "Arbeidstilsynet",
                FormType = "Søknad",
                Status = "Testmiljø og i produksjon"
            };

            if (data) 
                arbeidstilsynetYrkesbygg.Data = Serialize(new ArbeidstilsynetExampleGenerator().NyttSamtykke());

            forms.Add(arbeidstilsynetYrkesbygg);

            // Arbeidstilsynets samtykke V2
            var arbeidstilsynetsSamtykke = new Skjema
            {
                FormId = "5177",
                ServiceCode = 5689,
                ServiceEditionCode = 2,
                DataFormatID = "6821",
                DataFormatVersion = "45957",
                Name = "Søknad om Arbeidstilsynets samtykke V2",
                AvailableService = true,
                TemplateUrl = "",
                WikiUrl = "",
                OpenWikiUrl = "",
                ModelUrl = "",
                SendTo = "Arbeidstilsynet",
                FormType = "Søknad",
                Status = "Testmiljø og i produksjon",
                ValidationUrl = validationApi
            };
            
            if (data) 
                arbeidstilsynetsSamtykke.Data = Serialize(new ArbeidstilsynetExampleGeneratorV2().NyttSamtykke());

            forms.Add(arbeidstilsynetsSamtykke);

            // Tiltakshavers signatur og samtykke ved arbeidstilsynets samtykke
            var signaturSamtykkeATIL = new Skjema
            {
                FormId = "",
                ServiceCode = 5697,
                ServiceEditionCode = 1,
                DataFormatID = "6951",
                DataFormatVersion = "46372",
                Name = "Tiltakshavers signatur og samtykke til Arbeidstilsynet",
                AvailableService = false,
                TemplateUrl = "",
                WikiUrl = "",
                OpenWikiUrl = "",
                ModelUrl = "",
                SendTo = "Arbeidstilsynet",
                FormType = "Samtykke",
                Status = "Under utvikling",
                ValidationUrl = validationApi
            };

            if (data) 
                signaturSamtykkeATIL.Data = Serialize(new TiltakshaverSignaturSamtykkeATILExampleGenerator().NyttSamtykke());

            forms.Add(signaturSamtykkeATIL);

            // Supplering Arbeidstilsynets 
            var suppleringArbeidstilsynet = new Skjema
            {
                FormId = "",
                ServiceCode = 5851,
                ServiceEditionCode = 1,
                DataFormatID = "7086",
                DataFormatVersion = "47365",
                Name = "Supplering Arbeidstilsynet",
                AvailableService = true,
                TemplateUrl = "",
                WikiUrl = "",
                OpenWikiUrl = "",
                ModelUrl = "",
                SendTo = "Arbeidstilsynet",
                FormType = "Søknad",
                Status = "Under utvikling",
                ValidationUrl = validationApi
            };

            if (data) 
                suppleringArbeidstilsynet.Data = Serialize(new ArbeidstilsynetSuppleringGenerator().NySupplering());
            
            forms.Add(suppleringArbeidstilsynet);

            var avfallsplan = new Skjema
            {
                FormId = "5178/5179",
                ServiceCode = 4864,
                ServiceEditionCode = 1,
                DataFormatID = "5584",
                DataFormatVersion = "42035",
                Name = "Sluttrapport med avfallsplan",
                AvailableService = false,
                TemplateUrl = "",
                WikiUrl = "",
                OpenWikiUrl = "",
                ModelUrl = "",
                SendTo = "Kommunen",
                FormType = "Underskjema/vedlegg",
                Status = "Testmiljø"
            };

            if (data) 
                avfallsplan.Data = Serialize(new AvfallsplanExampleGenerator().NyAvfallsplan());

            forms.Add(avfallsplan);

            var avfallsplan2 = new Skjema
            {
                FormId = "5178/5179",
                ServiceCode = 4864,
                ServiceEditionCode = 2,
                DataFormatID = "7063",
                DataFormatVersion = "47177",
                Name = "Sluttrapport med avfallsplan",
                AvailableService = false,
                TemplateUrl = "",
                WikiUrl = "",
                OpenWikiUrl = "",
                ModelUrl = "",
                SendTo = "Kommunen",
                FormType = "Underskjema/vedlegg",
                Status = "Testmiljø",
                ValidationUrl = validationApi
            };

            if (data) 
                avfallsplan2.Data = Serialize(new AvfallsplanV2ExampleGenerator().NyAvfallsplanV2());

            forms.Add(avfallsplan2);

            // Matrikkel
            var matrikkel = new Skjema
            {
                FormId = "5176",
                ServiceCode = 4920,
                ServiceEditionCode = 1,
                DataFormatID = "5625",
                DataFormatVersion = "42144",
                Name = "Matrikkelopplysninger",
                AvailableService = false,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/53415647/Matrikkelopplysninger",
                SendTo = "Kommunen",
                FormType = "Underskjema",
                Status = "Testmiljø og i produksjon"
            };
            if (data) matrikkel.Data = Serialize(new MatrikkelregistreringExampleGenerator().Matrikkelregistrering());
            forms.Add(matrikkel);

            // Vedleggsopplysninger
            var vedlegg = new Skjema
            {
                FormId = "*",
                ServiceCode = 5058,
                ServiceEditionCode = 1,
                DataFormatID = "5797",
                DataFormatVersion = "42813",
                Name = "Vedleggsopplysninger",
                AvailableService = false,
                OpenWikiUrl = "https://dibk-utvikling.atlassian.net/wiki/spaces/FB/pages/334954497/Vedleggsopplysninger",
                SendTo = "Kommunen",
                FormType = "Underskjema",
                Status = "Testmiljø og i produksjon"
            };

            if (data) 
                vedlegg.Data = Serialize(new VedleggsopplysningerExampleGenerator().NyttVedleggsopplysninger());

            forms.Add(vedlegg);

            // Samtykke tiltakshaver
            var tiltakshaversamtykke = new Skjema
            {
                FormId = "*",
                ServiceCode = 5303,
                ServiceEditionCode = 1,
                DataFormatID = "6147",
                DataFormatVersion = "44097",
                Name = "Samtykke tiltakshaver",
                AvailableService = false,
                OpenWikiUrl = "",
                SendTo = "Søker",
                FormType = "Samtykke",
                Status = "Testmiljø"
            };

            if (data) 
                tiltakshaversamtykke.Data = "";

            forms.Add(tiltakshaversamtykke);

            // Nabovarsel for plan
            var nabovarselPlanForm = new Skjema
            {
                FormId = "*",
                ServiceCode = 5418,
                ServiceEditionCode = 1,
                DataFormatID = "6325",
                DataFormatVersion = "44842",
                Name = "Nabovarsel for plan",
                AvailableService = true,
                OpenWikiUrl = "https://dibk.atlassian.net/wiki/spaces/FP/pages/734724097/Distribusjonstjeneste+for+varsling+av+reguleringsplanoppstart",
                SendTo = "Naboer",
                FormType = "Distribusjon",
                Status = "I test og produksjon"
            };

            if (data)
                nabovarselPlanForm.Data = Serialize(new NabovarselPlanExampleGenerator().NyttNabovarsel());

            forms.Add(nabovarselPlanForm);

            // Svar på nabovarsel for plan
            var nabovarselSvarPlanForm = new Skjema
            {
                FormId = "*",
                ServiceCode = 5419,
                ServiceEditionCode = 1,
                DataFormatID = "6326",
                DataFormatVersion = "44843",
                Name = "Svar på nabovarsel for plan",
                AvailableService = false,
                OpenWikiUrl = "https://dibk.atlassian.net/wiki/spaces/FP/pages/734724097/Distribusjonstjeneste+for+varsling+av+reguleringsplanoppstart",
                SendTo = "Forslagsstiller/Plankonsulent",
                FormType = "Kvittering/vedlegg",
                Status = "I test og produksjon"
            };

            if (data)
                nabovarselSvarPlanForm.Data = Serialize(new NabovarselSvarPlanExampleGenerator().NySvarPaaNabovarsel());

            forms.Add(nabovarselSvarPlanForm);

            // Varsel om planoppstart til høringsmyndigheter
            var planvarselHoringsmyndigheterForm = new Skjema
            {
                FormId = "*",
                ServiceCode = 5701,
                ServiceEditionCode = 1,
                DataFormatID = "6944",
                DataFormatVersion = "46313",
                Name = "Planvarsel høringsmyndigheter",
                AvailableService = false,
                OpenWikiUrl = "https://dibk.atlassian.net/wiki/spaces/FP/pages/734724097/Distribusjonstjeneste+for+varsling+av+reguleringsplanoppstart",
                SendTo = "Høringsmyndigheter",
                FormType = "Distribusjon",
                Status = "Under utvikling"
            };

            if (data)
                planvarselHoringsmyndigheterForm.Data = Serialize(new PlanvarselHoeringsmyndigheterExampleGenerator().NyttNabovarsel());

            forms.Add(planvarselHoringsmyndigheterForm);

            // Uttalelse fra høringsmyndigheter
            var planuttalelseHoringsmyndigheterForm = new Skjema
            {
                FormId = "*",
                ServiceCode = 5702,
                ServiceEditionCode = 1,
                DataFormatID = "6945",
                DataFormatVersion = "46314",
                Name = "Uttalelse fra høringsmyndigheter",
                AvailableService = false,
                OpenWikiUrl = "https://dibk.atlassian.net/wiki/spaces/FP/pages/734724097/Distribusjonstjeneste+for+varsling+av+reguleringsplanoppstart",
                SendTo = "Forslagsstiller/Plankonsulent",
                FormType = "Kvittering/vedlegg",
                Status = "Under utvikling"
            };

            if (data)
                planuttalelseHoringsmyndigheterForm.Data = Serialize(new NabovarselSvarPlanExampleGenerator().NySvarPaaNabovarsel());

            forms.Add(planuttalelseHoringsmyndigheterForm);

            foreach (var form in forms)
                if (form.ValidationUrl == null)
                    form.ValidationUrl = origValidationApi;

            return forms;
        }

        public ValidationApiUrl GetValidationURL(string dataFormatId)
        {
            var form = GetForm(dataFormatId);
            
            return form.ValidationUrl;
        }

        private Skjema GetForm(string dataFormatId)
        {
            List<Skjema> forms = GetForms();

            return forms.Find(f => f.DataFormatID == dataFormatId);
        }

        private string Serialize(object form)
        {
            var serializer = new System.Xml.Serialization.XmlSerializer(form.GetType());
            var stringWriter = new Utf8StringWriter();
            serializer.Serialize(stringWriter, form);
            return stringWriter.ToString();
        }
    }
}
