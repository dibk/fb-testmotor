﻿using no.kxml.skjema.dibk.avfallsplanV2;
using System;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class AvfallsplanV2ExampleGenerator
    {
        ExampleDataRandomizer rand = new ExampleDataRandomizer();

        public no.kxml.skjema.dibk.avfallsplanV2.AvfallsplanType NyAvfallsplanV2()
        {
            var avfallsplanV2 = new no.kxml.skjema.dibk.avfallsplanV2.AvfallsplanType();

            avfallsplanV2.metadata = new no.kxml.skjema.dibk.avfallsplanV2.MetadataType
            {
                fraSluttbrukersystem = "Fellestjenester bygg testmotor",
                prosjektnavn = "Nybygg Lyngvelta",
                ftbId = Guid.NewGuid().ToString(),
                sluttbrukersystemUrl = "https://fellesbygg.dibk.no",
                versjonsnummerAvfallsplan = "1",
                prosjektnr = "234567"
            };


            //Tiltakshaver
            avfallsplanV2.tiltakshaver =
                new PartType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Privatperson",
                        kodebeskrivelse = "Privatperson"
                    },
                    foedselsnummer = "14096301368",
                    navn = "Phillip Selvik",
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Storgata 5",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "toroskar@arkitektum.no",
                    kontaktperson = new KontaktpersonType()
                    {
                        navn = "Benjamin Fjell",
                        epost = "ftbtest@yahoo.com",
                        mobilnummer = "90909090",
                        telefonnummer = "90909090"
                    }
                };


            avfallsplanV2.kommunensSaksnummer = new[]
            {
                new SaksnummerType(){ saksaar = "2022", sakssekvensnummer = "12345"  },
                new SaksnummerType(){ saksaar = "2022", sakssekvensnummer = "67891"  }
            };


            // ansvarlig soeker
            avfallsplanV2.ansvarligSoeker =
                new PartType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    organisasjonsnummer = "910065203", // 914994780 Arkitektum
                    navn = "ARKITEKT FLINK",
                    kontaktperson = new KontaktpersonType()
                    {
                        navn = "Benjamin Fjell",
                        epost = "benjamin@fjell.no",
                        mobilnummer = "99977788",
                        telefonnummer = "55500055"
                    },
                    adresse = new EnkelAdresseType
                    {
                        adresselinje1 = "Lillegata 5",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "90909090",
                    epost = "ftbtest@yahoo.com"
                };


            // eiendomByggested
            avfallsplanV2.eiendomByggested = new[]
            {
                new EiendomType
                {
                    kommunenavn = "Kommunenavn",
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Nygate 33",
                        postnr = "3825",
                        poststed = "Lunde"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = "H0101"
                }
            };



            //var bruk2 = new FormaalType();
            //bruk2.bygningstype = new[] {
            //    new KodeType
            //    {
            //        kodeverdi = "111",
            //        kodebeskrivelse = "Enebolig"
            //    },
            //    new KodeType
            //    {
            //        kodeverdi = "231",
            //        kodebeskrivelse = "Lagerhall"
            //    }
            //};


            // beskrivelseAvTiltak
            avfallsplanV2.beskrivelseAvTiltak = new TiltakType
            {
                bruk = new[]
                {
                new KodeType
                {
                    kodeverdi = "111",
                    kodebeskrivelse = "Enebolig"
                },
                new KodeType
                {
                    kodeverdi = "231",
                    kodebeskrivelse = "Lagerhall"
                }
            },
                type = new[]
                {
                    new KodeType
                    {
                        kodeverdi = "nyttbyggboligformal",
                        kodebeskrivelse = "Nytt bygg - Boligformål"
                    },
                    new KodeType
                    {
                        kodeverdi = "fasade",
                        kodebeskrivelse = "Endring av bygg - utvendig - Fasade"
                    }
                }
            };


            // Bruksareal
            avfallsplanV2.bruksarealAvfall = "222";

            // Avfallplan
            avfallsplanV2.plan = new PlanType()
            {
                avfall = new[] {


                    new AvfallPlanType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1141", kodebeskrivelse = "Rent trevirke" },
                        mengdeFraksjon = "3,002"
                    },
                    new AvfallPlanType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1299", kodebeskrivelse = "Papir, papp og kartong" },
                        mengdeFraksjon = "0,5"
                    },
                    new AvfallPlanType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1615", kodebeskrivelse = "Gipsbaserte materialer" },
                        mengdeFraksjon = "2,0"
                    },



                    new AvfallPlanType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "farligAvfall", kodebeskrivelse = "Farlig avfall" },
                        fraksjon = new KodeType {kodeverdi = "7051", kodebeskrivelse = "Maling, lim og lakk" },
                        mengdeFraksjon = "1,0"
                    },
                    new AvfallPlanType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "farligAvfall", kodebeskrivelse = "Farlig avfall" },
                        fraksjon = new KodeType {kodeverdi = "9000", kodebeskrivelse = "Veldig farlig avfall" },
                        mengdeFraksjon = "0,5"
                    },
                    new AvfallPlanType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "farligAvfall", kodebeskrivelse = "Farlig avfall" },
                        fraksjon = new KodeType {kodeverdi = "7000", kodebeskrivelse = "Batterier" },
                        mengdeFraksjon = "1,01"
                    },


                    new AvfallPlanType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "blandetAvfall", kodebeskrivelse = "Blandet avfall" },
                        fraksjon = new KodeType {kodeverdi = "9912", kodebeskrivelse = "Blandet næringsavfall til sortering" },
                        mengdeFraksjon = "9"
                    }
                },
                avfallsklasseDelsum = new[]
                {
                        new AvfallsklasseDelsumType()
                        {
                            avfallsklasse = new KodeType() { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall"},
                            delsum = "5,502"
                        },
                        new AvfallsklasseDelsumType()
                        {
                            avfallsklasse = new KodeType() { kodeverdi = "farligAvfall", kodebeskrivelse = "Farlig avfall"},
                            delsum = "2,51"
                        },
                        new AvfallsklasseDelsumType()
                        {
                            avfallsklasse = new KodeType() { kodeverdi = "blandetAvfall", kodebeskrivelse = "Blandet avfall"},
                            delsum = "1,5"
                        }

                },
                sortering = new SorteringType()
                {
                    mengdeTotalt = "9,5",
                    mengdeSortert = "8,5",
                    sorteringsgrad = "84"
                }
            };






            avfallsplanV2.sluttrapport = new SluttrapportType()
            {
                avfall = new[]
                {
                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1615", kodebeskrivelse = "Gipsbaserte materialer" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Nabobygg AS", mottaksId = "123000"},
                            disponeringsmaate = new KodeType {kodeverdi = "ombrukDirekte", kodebeskrivelse = "Levert direkte til ombruk"}
                        },
                        mengdeFraksjon = "0,5"
                    },
                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1141", kodebeskrivelse = "Rent trevirke" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Nabobygg AS", mottaksId = "123000"},
                            disponeringsmaate = new KodeType {kodeverdi = "ombrukForberedelse", kodebeskrivelse = "Levert til forberedelse til ombruk"}
                        },
                        mengdeFraksjon = "0,002"
                    },
                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1141", kodebeskrivelse = "Rent trevirke" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Nabobygg AS", mottaksId = "123000"},
                            disponeringsmaate = new KodeType {kodeverdi = "ombrukForberedelse", kodebeskrivelse = "Levert til forberedelse til ombruk"}
                        },
                        mengdeFraksjon = "0,5"
                    },
                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1299", kodebeskrivelse = "Papir, papp og kartong" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Nabobygg AS", mottaksId = "123000"},
                            disponeringsmaate = new KodeType {kodeverdi = "ombrukForberedelse", kodebeskrivelse = "Levert til forberedelse til ombruk"}
                        },
                        mengdeFraksjon = "0,02"
                    },

                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1611", kodebeskrivelse = "Betong uten armeringsjern" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Norsk Gjennvinning", mottaksId = "124000"},
                            disponeringsmaate = new KodeType {kodeverdi = "direkteFormaal", kodebeskrivelse = "Levert direkte til gjenvinning til utfyllingsformål/teknisk formål (eks. betong/tegl)"}
                        },
                        mengdeFraksjon = "0,5"
                    },

                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1141", kodebeskrivelse = "Rent trevirke" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Bølstad søppelfylling", mottaksId = "125000"},
                            disponeringsmaate = new KodeType {kodeverdi = "direkteAnnen", kodebeskrivelse = "Levert direkte til annen gjenvinning"}
                        },
                        mengdeFraksjon = "0,3"
                    },


                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1615", kodebeskrivelse = "Gipsbaserte materialer" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Nabobygg AS", mottaksId = "123000"},
                            disponeringsmaate = new KodeType {kodeverdi = "godkjentMottak", kodebeskrivelse = "Godkjent avfallsmottak"}
                        },
                        mengdeFraksjon = "0,5"
                    },


                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1141", kodebeskrivelse = "Hardplast, emballasje" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Nabobygg AS", mottaksId = "123000"},
                            disponeringsmaate = new KodeType {kodeverdi = "godkjentMottak", kodebeskrivelse = "Godkjent avfallsmottak"}
                        },
                        mengdeFraksjon = "0,5"
                    },


                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall" },
                        fraksjon = new KodeType {kodeverdi = "1299", kodebeskrivelse = "Forurenset betong og tegl" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Bølstad søppelfylling", mottaksId = "125000"},
                            disponeringsmaate = new KodeType {kodeverdi = "godkjentMottak", kodebeskrivelse = "Godkjent avfallsmottak"}
                        },
                        mengdeFraksjon = "0,5"
                    },


                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "farligAvfall", kodebeskrivelse = "Farlig avfall" },
                        fraksjon = new KodeType {kodeverdi = "7015", kodebeskrivelse = "Maling, lim og lakk" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Nabobygg AS", mottaksId = "123000"},
                            disponeringsmaate = new KodeType {kodeverdi = "godkjentMottak", kodebeskrivelse = "Godkjent avfallsmottak"}
                        },
                        mengdeFraksjon = "0,5"
                    },


                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "farligAvfall", kodebeskrivelse = "Farlig avfall" },
                        fraksjon = new KodeType {kodeverdi = "9000", kodebeskrivelse = "Velding farlig avfall" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Nabobygg AS", mottaksId = "123000"},
                            disponeringsmaate = new KodeType {kodeverdi = "godkjentMottak", kodebeskrivelse = "Godkjent avfallsmottak"}
                        },
                        mengdeFraksjon = "0,5"
                    },


                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "farligAvfall", kodebeskrivelse = "Farlig avfall" },
                        fraksjon = new KodeType {kodeverdi = "7000", kodebeskrivelse = "Batterier" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Bølstad søppelfylling", mottaksId = "125000"},
                            disponeringsmaate = new KodeType {kodeverdi = "godkjentMottak", kodebeskrivelse = "Godkjent avfallsmottak"}
                        },
                        mengdeFraksjon = "0,5"
                    },

                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "blandetAvfall", kodebeskrivelse = "Blandet avfall" },
                        fraksjon = new KodeType {kodeverdi = "9912", kodebeskrivelse = "Blandet næringsavfall til sortering" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Bølstad søppelfylling", mottaksId = "125000"},
                            disponeringsmaate = new KodeType {kodeverdi = "godkjentMottak", kodebeskrivelse = "Godkjent avfallsmottak"}
                        },
                        mengdeFraksjon = "1,0"
                    },

                    new AvfallRapportType()
                    {
                        avfallsklasse = new KodeType { kodeverdi = "blandetAvfall", kodebeskrivelse = "Blandet avfall" },
                        fraksjon = new KodeType {kodeverdi = "9912", kodebeskrivelse = "Blandet næringsavfall til sortering" },
                        disponering = new DisponeringType()
                        {
                            leveringssted = new AvfallsanleggType(){mottaksNavn = "Nabobygg AS", mottaksId = "123000"},
                            disponeringsmaate = new KodeType {kodeverdi = "godkjentMottak", kodebeskrivelse = "Godkjent avfallsmottak"}
                        },
                        mengdeFraksjon = "0,5"
                    }


                },
                
                
                
                
                sortering = new SorteringType()
                {
                    mengdeTotalt = "8,5",
                    mengdeSortert = "7,0",
                    mengdeAvfallPerAreal = "500",
                    sorteringsgrad = "82",
                },
                disponeringsmaateDelsum = new[]
                   {
                       new DisponeringsmaateDelsumType()
                       {
                           disponeringsmaate = new KodeType {kodeverdi = "ombrukDirekte", kodebeskrivelse = "Levert direkte til ombruk"},
                           delsum = "0,5"
                       },
                       new DisponeringsmaateDelsumType()
                       {
                           disponeringsmaate = new KodeType {kodeverdi = "ombrukForberedelse", kodebeskrivelse = "Levert til forberedelse til ombruk"},
                           delsum = "0,522"
                       },
                       new DisponeringsmaateDelsumType()
                       {
                           disponeringsmaate = new KodeType {kodeverdi = "direkteFormaal", kodebeskrivelse = "Levert direkte til gjenvinning til utfyllingsformål/teknisk formål (eks. betong/tegl)"},
                           delsum = "0,003"
                       },
                       new DisponeringsmaateDelsumType()
                       {
                           disponeringsmaate = new KodeType {kodeverdi = "direkteAnnen", kodebeskrivelse = "Levert direkte til annen gjenvinning"},
                           delsum = "0,5"
                       },
                       new DisponeringsmaateDelsumType()
                       {
                           disponeringsmaate = new KodeType {kodeverdi = "godkjentMottak", kodebeskrivelse = "Levert til godkjent avfallsmottak"},
                           delsum = "1,5"
                       },
                   },
                avfallsklasseDelsum = new[]
                {
                       new AvfallsklasseDelsumType()
                        {
                            avfallsklasse = new KodeType() { kodeverdi = "farligAvfall", kodebeskrivelse = "Farlig avfall"},
                            delsum = "1,5"
                        },
                        new AvfallsklasseDelsumType()
                        {
                            avfallsklasse = new KodeType() { kodeverdi = "blandetAvfall", kodebeskrivelse = "Blandet avfall"},
                            delsum = "1,5"
                        },
                        new AvfallsklasseDelsumType()
                        {
                            avfallsklasse = new KodeType() { kodeverdi = "ordinaertAvfall", kodebeskrivelse = "Ordinært avfall"},
                            delsum = "5,5"
                        }

                },
                gjenstaaendeAvfall = "Det gjennstår ca 0,4 tonn asbest (fraksjon 9000). Det skal resirkulerer på Bølestad søpperlfylling etter at det er håndsortert på byggeplassen av frivillige barn. "
            };

            avfallsplanV2.avfallEtterRivingSpecified = true;
            avfallsplanV2.avfallEtterRiving = true;


            // return data model with example data
            return avfallsplanV2;
        }
    }
}