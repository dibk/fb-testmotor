﻿using no.kxml.skjema.dibk.tiltakutenansvarsrettV3;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class TiltakutenansvarsrettV3ExampleGenerator
    {
        public TiltakUtenAnsvarsrettType NyttTiltakUtenAnsvarsrett()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var tiltakutenansvarsrett = new TiltakUtenAnsvarsrettType
            {
                eiendomByggested = new[]
                {
                    new EiendomType()
                    {
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = "Storgata 19",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },

                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            kommunenummer = rand.GetRandomTestKommuneNummer(),
                            gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                            bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                            festenummer = "0",
                            seksjonsnummer = "0"
                        },
                        bygningsnummer = "123456789",
                        bolignummer = "H0201",
                        kommunenavn = "Nome"
                    }
                },

                beskrivelseAvTiltak = new TiltakType()
                {
                    bruk = new FormaalType()
                    {
                        beskrivPlanlagtFormaal = "Eksempelbeskrivelse av planlagt formål",
                        naeringsgruppe = new KodeType() {kodeverdi = "X", kodebeskrivelse = "Bolig"},
                        bygningstype = new KodeType {kodeverdi = "111", kodebeskrivelse = "Enebolig"},
                        tiltaksformaal = new[] {new KodeType {kodeverdi = "Bolig", kodebeskrivelse = "Bolig"}},
                    },

                    foelgebrev = "Det søkes om bygging av et eller annet.",

                    type = new TypeTiltakType()
                    {
                        beskrivelse = "Skal bygge noe som er under 70 m2, aner ikke hva det skal bli enda.",
                        type = new KodeType()
                        {
                            kodeverdi = "nyttbyggunder70m2",
                            kodebeskrivelse = "Nytt bygg - Under 70 m2 - ikke boligformål"
                        }
                    }
                },

                tiltakshaver = new PartType()
                {
                    partstype = new KodeType { kodeverdi = "Privatperson", kodebeskrivelse = "Privatperson" },
                    foedselsnummer = "28044102120",
                    navn = "Ludvig Moholt",
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Bøgata 1",
                        landkode = "NO",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                    },
                    telefonnummer = "12345678",
                    mobilnummer = "87654321",
                    epost = "navn@domene.no", 
                    kontaktperson = new KontaktpersonType()
                    {
                        navn = "Truls Trulsen",
                        telefonnummer = "12312345",
                        mobilnummer = "12312312",
                        epost = "truls@trulsen.no"
                    }
                },

                dispensasjon = new[]
                {
                    new DispensasjonType()
                    {
                        dispensasjonstype = new KodeType()
                        {
                            kodeverdi = "PLAN",
                            kodebeskrivelse = "Arealplaner"
                        },
                        begrunnelse = "Eksempelbegrunnelse - annen grad på takvinkel enn i reguleringsplan",
                        beskrivelse = "Begrunnelse for hvorfor"
                    }
                },

                kommunensSaksnummer = new SaksnummerType()
                {
                    saksaar = "2018",
                    sakssekvensnummer = "42"
                },

                varsling = new VarslingType()
                {
                    fritattFraNabovarsling = true,
                    fritattFraNabovarslingSpecified = true,
                    foreliggerMerknaderSpecified = true,
                    foreliggerMerknader = false,
                    antallMerknader = "2",
                    vurderingAvMerknader = "Eksempelvurdering av merknader",
                    soeknadensHjemmeside = "www.eksempeldomene.no"
                },

                metadata = new MetadataType()
                {
                    fraSluttbrukersystem = "Fellestjenester bygg testmotor",
                    ftbId = "1000000000001",
                    prosjektnavn = "Noens byggeprosjekt",
                    sluttbrukersystemUrl = "", 
                    klartForSigneringFraSluttbrukersystem = true,
                    klartForSigneringFraSluttbrukersystemSpecified = true
                },

                rammebetingelser = new RammerType()
                {
                    adkomst = new VegType()
                    {
                        nyeEndretAdkomst = true,
                        nyeEndretAdkomstSpecified = true,

                        vegrett = new[]
                        {
                            new VegrettType()
                            {
                                erTillatelseGittSpecified = true,
                                erTillatelseGitt = true,
                                vegtype = new KodeType() {kodeverdi = "KommunalVeg", kodebeskrivelse = "KommunalVeg"}
                            }
                        }
                    },
                    arealdisponering = new ArealdisponeringType()
                    {
                        tomtearealByggeomraade = 20.4,
                        tomtearealByggeomraadeSpecified = true,
                        tomtearealSomTrekkesFra = 4.2,
                        tomtearealSomTrekkesFraSpecified = true,
                        tomtearealSomLeggesTil = 12.6,
                        tomtearealSomLeggesTilSpecified = true,
                        tomtearealBeregnet = 24.1,
                        tomtearealBeregnetSpecified = true,
                        beregnetMaksByggeareal = 28.2,
                        beregnetMaksByggearealSpecified = true,
                        arealBebyggelseEksisterende = 89.2,
                        arealBebyggelseEksisterendeSpecified = true,
                        arealBebyggelseSomSkalRives = 8.4,
                        arealBebyggelseSomSkalRivesSpecified = true,
                        arealBebyggelseNytt = 16.3,
                        arealBebyggelseNyttSpecified = true,
                        parkeringsarealTerreng = 45.2,
                        parkeringsarealTerrengSpecified = true,
                        arealSumByggesak = 152.7,
                        arealSumByggesakSpecified = true,
                        beregnetGradAvUtnytting = 140.2,
                        beregnetGradAvUtnyttingSpecified = true
                    },
                    generelleVilkaar = new GenerelleVilkaarType()
                    {
                        oppfyllesVilkaarFor3Ukersfrist = false,
                        oppfyllesVilkaarFor3UkersfristSpecified = true,
                        beroererTidligere1850 = false,
                        beroererTidligere1850Specified = true,
                        norskSvenskDansk = true,
                        norskSvenskDanskSpecified = true,
                        behovForTillatelse = false,
                        behovForTillatelseSpecified = true
                    },
                    plan = new PlanType()
                    {
                        gjeldendePlan = new GjeldendePlanType()
                        {
                            utnyttingsgrad = 24.5,
                            utnyttingsgradSpecified = true,
                            plantype = new KodeType
                            {
                                kodeverdi = "RP",
                                kodebeskrivelse = "Reguleringsplan"
                            },
                            navn = "Eksemplelplannavn",
                            formaal = "Eksempelformål",
                            beregningsregelGradAvUtnytting = new KodeType
                            {
                                kodeverdi = "BYA",
                                kodebeskrivelse = "Bebygd areal"
                            }
                        },
                        andrePlaner = new AndrePlanerType[]
                        {
                            new AndrePlanerType()
                            {
                                navn = "Områderegulering Midt-Telemark",
                                plantype = new KodeType() {kodebeskrivelse = "Områderegulering", kodeverdi = "34"}
                            },

                            new AndrePlanerType()
                            {
                                navn = "Områderegulering Midt i midten-Telemark",
                                plantype = new KodeType() {kodebeskrivelse = "Områderegulering", kodeverdi = "34"}
                            }
                        },
                        andreRelevanteKrav = "Eksempel på andre relevante krav",
                    },
                  
                    vannforsyning = new VannforsyningType()
                    {
                        tilknytningstype =
                            new[]
                            {
                                //new KodeType {kodeverdi = "Offentlig vannverk", kodebeskrivelse = "Offentlig vannverk"} 
                                new KodeType {kodeverdi = "AnnenPrivatInnlagt", kodebeskrivelse = "Annen privat vannforsyning, innlagt vann"}
                            },
                        beskrivelse = "Brønn i hage",
                        krysserVannforsyningAnnensGrunn = false,
                        krysserVannforsyningAnnensGrunnSpecified = true,
                        tinglystErklaering = true,
                        tinglystErklaeringSpecified = true
                    },
                    avloep = new AvloepType()
                    {
                        tilknytningstype = new KodeType
                        {
                            //kodeverdi = "Offentlig avløpsanlegg",kodebeskrivelse = "Offentlig avløpsanlegg" 
                            kodeverdi = "PrivatKloakk",
                            kodebeskrivelse = "Privat avløpsanlegg"
                        },
                        installereVannklosett = true,
                        installereVannklosettSpecified = true,
                        utslippstillatelse = true,
                        utslippstillatelseSpecified = true,
                        krysserAvloepAnnensGrunn = false,
                        krysserAvloepAnnensGrunnSpecified = true,
                        tinglystErklaering = true,
                        tinglystErklaeringSpecified = true,
                        overvannTerreng = false,
                        overvannTerrengSpecified = true,
                        overvannAvloepssystem = true,
                        overvannAvloepssystemSpecified = true
                    },
                    kravTilByggegrunn = new KravTilByggegrunnType()
                    {
                        flomutsattOmraade = false,
                        flomutsattOmraadeSpecified = true,
                        skredutsattOmraade = false,
                        skredutsattOmraadeSpecified = true,
                        miljoeforhold = false,
                        miljoeforholdSpecified = true
                    },
                    plassering = new PlasseringType()
                    {
                        konfliktHoeyspentkraftlinje = false,
                        konfliktHoeyspentkraftlinjeSpecified = true,
                        minsteAvstandNabogrense = 5.2,
                        minsteAvstandNabogrenseSpecified = true,
                        konfliktVannOgAvloep = false,
                        konfliktVannOgAvloepSpecified = true,
                        minsteAvstandTilAnnenBygning = 10.3,
                        minsteAvstandTilAnnenBygningSpecified = true,
                        minsteAvstandTilMidtenAvVei = 25.7,
                        minsteAvstandTilMidtenAvVeiSpecified = true, 
                        bekreftetInnenforByggegrense = false,
                        bekreftetInnenforByggegrenseSpecified = true
                    }
                }
            };
            return tiltakutenansvarsrett;
        }
    }
}
