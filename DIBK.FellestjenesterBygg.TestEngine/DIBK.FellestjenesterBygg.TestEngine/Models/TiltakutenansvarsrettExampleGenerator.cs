﻿using no.kxml.skjema.dibk.tiltakutenansvarsrett;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class TiltakutenansvarsrettExampleGenerator
    {
        public TiltakUtenAnsvarsrettType NyttTiltakUtenAnsvarsrett()
        {

            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var tiltakutenansvarsrett = new TiltakUtenAnsvarsrettType();
            tiltakutenansvarsrett.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            tiltakutenansvarsrett.eiendomByggested = new[] {
                new EiendomType() {
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Storgata 19",
                        landkode = "NO",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                    },

                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = "H0201"
                }
            };

            tiltakutenansvarsrett.beskrivelseAvTiltak = new[] {
                new TiltakType()
                {
                    adkomst = new VegType()
                    {
                        nyeEndretAdkomst = false,
                        vegrett = new[] 
                        {
                            new VegrettType()
                            {
                                erTillatelseGitt = true,
                                vegtype = new KodeType() { kodeverdi = "KommunalVeg", kodebeskrivelse = "KommunalVeg"}
                            }
                        }
                    },
                    bruk = new FormaalType()
                    {
                        beskrivPlanlagtFormaal = "Eksempelbeskrivelse av planlagt formål",
                        naeringsgruppe = new KodeType() { kodeverdi = "X", kodebeskrivelse = "Bolig"},
                        bygningstype = new[] { new KodeType { kodeverdi = "111", kodebeskrivelse = "Enebolig"}},
                        tiltaksformaal = new[] { new KodeType { kodeverdi = "Bolig", kodebeskrivelse = "Bolig" } }, 
                    },
                    vannforsyning = new VannforsyningType()
                    {
                        tilknytningstype = new [] { new KodeType { kodeverdi = "Offentlig vannverk", kodebeskrivelse = "Offentlig vannverk" } }, // TODO må opprette kodeliste og sette inn eksempeldata
                        beskrivelse = "brønn i hage",
                        krysserVannforsyningAnnensGrunn = false,
                        tinglystErklaering = true
                    },
                    avloep = new AvloepType()
                    {
                        tilknytningstype = new KodeType { kodeverdi = "Offentlig avløpsanlegg", kodebeskrivelse = "Offentlig avløpsanlegg"},  // TODO må opprette kodeliste og sette inn eksempeldata
                        installereVannklosett = true,
                        utslippstillatelse = true,
                        krysserAvloepAnnensGrunn = false,
                        tinglystErklaering = true,
                        overvannTerreng = false,
                        overvannAvloepssystem = true
                    },
                    type = new [] { new KodeType { kodeverdi = "fasade", kodebeskrivelse = "Endring av bygg - utvendig - Fasade" } },
                    plassering = new PlasseringType()
                    {
                        konfliktHoeyspentkraftlinje = false,
                        minsteAvstandNabogrense = 5.2,
                        konfliktVannOgAvloep = false,
                        minsteAvstandTilAnnenBygning = 10.3,
                        minsteAvstandTilMidtenAvVei = 25.7
                    }

                }
            };

            tiltakutenansvarsrett.tiltakshaver = new PartType()
            {
                partstype = new KodeType { kodeverdi = "Privatperson", kodebeskrivelse= "Privatperson" }, 
                foedselsnummer = "25125401530", // FILIP MOHAMED (TT02)
                navn = "Ola Nordmann",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 1",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "12345678",
                mobilnummer = "87654321",
                epost = "navn@domene.no"
            };

            tiltakutenansvarsrett.dispensasjon = new DispensasjonType()
            {
                dispensasjonstype = new [] {new KodeType { kodeverdi = "PLAN", kodebeskrivelse = "Arealplaner"} },
                begrunnelse = "Eksempelbegrunnelse - annen grad på takvinkel enn i reguleringsplan",

            };

            tiltakutenansvarsrett.varsling = new VarslingType()
            {
                foreliggerMerknader = true,
                antallMerknader = "2",
                vurderingAvMerknader = "Eksempelvurdering av merknader",
                soeknadensHjemmeside = "www.eksempeldomene.no"
            };

            tiltakutenansvarsrett.kommunensSaksnummer = new SaksnummerType()
            {
                saksaar = "2016",
                sakssekvensnummer = "42"
            };

            tiltakutenansvarsrett.rammebetingelser = new RammerType()
            {
                arealdisponering = new ArealdisponeringType()
                {
                    tomtearealByggeomraade = 20.4,
                    gjeldendePlan = new PlanType()
                    {
                        utnyttingsgrad = 24.5,
                        plantype = new KodeType { kodeverdi = "RP", kodebeskrivelse = "Reguleringsplan"},
                        navn = "Eksemplelplannavn",
                        formaal = "Eksempelformål",
                        andreRelevanteKrav = "Eksempel på andre relevante krav",
                        beregningsregelGradAvUtnytting = new KodeType { kodeverdi = "BYA", kodebeskrivelse = "Bebygd areal"}
                    },
                    tomtearealSomTrekkesFra = 4.2,
                    tomtearealSomLeggesTil = 12.6,
                    tomtearealBeregnet = 24.1,
                    beregnetMaksByggeareal = 28.2,
                    arealBebyggelseEksisterende = 89.2,
                    arealBebyggelseSomSkalRives = 8.4,
                    arealBebyggelseNytt = 16.3,
                    parkeringsarealTerreng = 45.2,
                    arealSumByggesak = 152.7,
                    beregnetGradAvUtnytting = 140.2
                },
                generelleVilkaar = new GenerelleVilkaarType()
                {
                    oppfyllesVilkaarFor3Ukersfrist = true,
                    beroererTidligere1850 = false,
                    behovForTillatelse = true,
                    soekesOmDispensasjon = true,
                    soekesOmDispensasjonSpecified = true
                },
                kravTilByggegrunn = new KravTilByggegrunnType()
                {
                    flomutsattOmraade = false,
                    skredutsattOmraade = true,
                    miljoeforhold = false
                },
                loefteinnretninger = new LoefteinnretningerType()
                {
                    erLoefteinnretningIBygning = false,
                    planleggesLoefteinnretningIBygning = false,
                    planleggesHeis = true,
                    planleggesHeisSpecified = true,
                    planleggesTrappeheis = false,
                    planleggesTrappeheisSpecified = true,
                    planleggesRulletrapp = true,
                    planleggesRulletrappSpecified = true,
                    planleggesLoefteplattform = false,
                    planleggesLoefteplattformSpecified = true
                }
            };
            
            return tiltakutenansvarsrett;
        }
    }
}