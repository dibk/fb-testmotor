﻿using no.kxml.skjema.dibk.midlertidigbrukstillatelse;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class MidlertidigBrukstillatelseExampleGenerator
    {
        public MidlertidigBrukstillatelseType NyMidlertidigBrukstillatelse()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseType();
            midlertidigBrukstillatelse.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            midlertidigBrukstillatelse.gjelderHeleTiltaket = true;
            midlertidigBrukstillatelse.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2015",
                sakssekvensnummer = "124550"
            };

            midlertidigBrukstillatelse.tiltakshaver = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                foedselsnummer = "02038521576",
                navn = "Jens Jensen",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "soeker@domene.no"
            };
            midlertidigBrukstillatelse.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 77",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                       
                    },
                    bygningsnummer = "123456778",
                    bolignummer = "H0101"
                }
            };
            midlertidigBrukstillatelse.datoFerdigattest = new System.DateTime(2016, 03, 29);
            midlertidigBrukstillatelse.delsoeknad = new[]
            {
                new DelsoeknadMidlertidigBrukstillatelseType()
                {
                    delAvTiltaket = "Første byggetrinn",
                    gjenstaaendeInnenfor = "Overflatebehandling gulv",
                    utfoertInnen = new System.DateTime(2016, 02, 14),
                    utfoertInnenSpecified = true,
                    typeArbeider = "Montert rekkverk på trapp",
                    bekreftelseInnen = new System.DateTime(2016, 02, 04),
                    bekreftelseInnenSpecified = true,
                    gjenstaaendeUtenfor = "Byggetrinn 2",
                    harTilstrekkeligSikkerhet = true,
                    harTilstrekkeligSikkerhetSpecified = true
                }
            };
           
            midlertidigBrukstillatelse.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "911455307",
                navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Fjellveien 3",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "hans@domene.no"
            };

            return midlertidigBrukstillatelse;
        }
    }
}