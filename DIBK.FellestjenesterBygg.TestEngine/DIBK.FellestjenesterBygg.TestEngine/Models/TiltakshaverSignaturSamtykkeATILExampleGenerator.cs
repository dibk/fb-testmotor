﻿

using no.kxml.skjema.dibk.arbeidstilsynetSignaturSamtykke;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class TiltakshaverSignaturSamtykkeATILExampleGenerator
    {
        public ArbeidstilsynetSignaturSamtykkeType NyttSamtykke()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var samtykke = new ArbeidstilsynetSignaturSamtykkeType();

         
          
            samtykke.tiltakshaver = new PartType()
            {
                organisasjonsnummer = "910748548",
                navn = "BLOMSTERDALEN OG ØVRE SNERTINGDAL",
                
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                epost = "tine@arkitektum.no",
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Tine Høllre", 
                    telefonnummer = "98839131",
                    epost = "tine@arkitektum.no"
                }
            };

            samtykke.ansvarligSoeker = new PartType()
            {
                organisasjonsnummer = "910297937",
                navn = "FANA OG HAFSLO REVISJON"
            };

            samtykke.betaling = new BetalingType()
            {
                sum = "1500"
            };

            return samtykke;
        }
    }
}
