﻿using System;
using System.IO;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class AttachmentExampleGenerator
    {
        private const string FileNameSituasjonsplan = "attachment_situasjonsplan.pdf";
        private const string FileNameTegning = "attachment_tegning.pdf";
        private const string FileNameDispensasjonssoeknad = "attachment_dispensasjonssoeknad.pdf";
        private const string FileNameKartA = "attachment_KartA.pdf";
        private const string FileNameKartB = "attachment_KartB.pdf";

        public Vedlegg AttachmentSituasjonsplan()
        {
            return new Vedlegg("situasjonsplan.pdf", "Situasjonsplan", ToBase64String(FileNameSituasjonsplan));
        }

        public Vedlegg AttachmentNyTegningPlan()
        {
            return new Vedlegg("tegning-ny-plan.pdf", "TegningNyPlan", ToBase64String(FileNameTegning));
        }

        public Vedlegg AttachmentDispensasjonssoeknad()
        {
            return new Vedlegg("dispensasjonssoeknad.pdf", "Dispensasjonssoeknad", ToBase64String(FileNameDispensasjonssoeknad));
        }
        public Vedlegg AttachmentKartA()
        {
            return new Vedlegg("kartOmraadeA.pdf", "Kart", ToBase64String(FileNameKartA));
        }
        public Vedlegg AttachmentKartB()
        {
            return new Vedlegg("kartOmraadeB.pdf", "Kart", ToBase64String(FileNameKartB));
        }

        private string ToBase64String(string filename)
        {
            return Convert.ToBase64String(File.ReadAllBytes(System.Web.Hosting.HostingEnvironment.MapPath("/App_Data/") + filename));
        }

    }
}