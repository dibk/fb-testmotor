﻿using System;
using no.kxml.skjema.dibk.nabovarselV3;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class NabovarselExampleGeneratorV3List
    {
        public NabovarselType NyttNabovarsel()
        {
            var rand = new ExampleDataRandomizer();
            var nabovarsel = new NabovarselType();

            Encryption.Encryption encrypt = new Encryption.Encryption();


            nabovarsel.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Storgata 19",
                        landkode = "NO",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                    },
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "1",
                        seksjonsnummer = "1",
                        kommunenummer = rand.GetRandomTestKommuneNummer()
                    },
                    kommunenavn = "Bø i Telemark", 
                    eier = "Trond Trondsen"
                }
            };


            nabovarsel.beskrivelseAvTiltak = new[]
            {
                new TiltakType
                {
                    bruk = new FormaalType()
                    {
                        tiltaksformaal = new[]
                        {
                            new KodeType()
                            {
                                kodeverdi = "Annet",
                                kodebeskrivelse = "Annet"
                            }
                        },

                        beskrivPlanlagtFormaal = "Beskrivelse av annet formål"
                    },
                    type = new[]
                    {
                        new KodeType
                        {
                            kodeverdi = "fasade",
                            kodebeskrivelse = "Endring av bygg - utvendig - Fasade"
                        }
                    },
                }
            };


            nabovarsel.gjeldendePlan = new PlanType
            {
                plantype = new KodeType
                {
                    kodeverdi = "RP",
                    kodebeskrivelse = "Reguleringsplan"
                },
                navn = "rammebetingelser navn"
            };


            nabovarsel.tiltakshaver = new PartType
            {
                partstype = new KodeType
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                //foedselsnummer = "10023000208",
                foedselsnummer = encrypt.EncryptText("10023000208"),
                navn = "Tor Hammer",
                adresse = new no.kxml.skjema.dibk.nabovarselV3.EnkelAdresseType
                {
                    adresselinje1 = "Roald Dalhs gate 4",
                    poststed = "Skien",
                    postnr = "3701"
                },
                telefonnummer = "46576879",
                mobilnummer = "48955163",
                epost = "tor@arkitektum.no",
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Kari Nordmann",
                    telefonnummer = "12344567",
                    mobilnummer = "98945777",
                    epost = "tor@arkitektum.no"
                }
            };


            nabovarsel.naboeier = new[]
            {
                new NaboGjenboerType()
                {
                    partstype = new KodeType()
                    {
                        kodebeskrivelse = "Privatperson",
                        kodeverdi = "Privatperson"
                    },
                    navn = "Vigdis Vater",
                    //foedselsnummer = "10019000953",
                    foedselsnummer = encrypt.EncryptText("10019000953"),

                    adresse = new no.kxml.skjema.dibk.nabovarselV3.EnkelAdresseType()
                    {
                        adresselinje1 = "Storgata 3",
                        postnr = "7003",
                        poststed = "Trondheim",
                        landkode = "no"
                    },
                    gjelderNaboeiendom = new EiendomType()
                    {
                        eiendomsidentifikasjon = new MatrikkelnummerType
                        {
                            gaardsnummer = "109",
                            bruksnummer = "1",
                            kommunenummer = "5001",
                            seksjonsnummer = "1"
                        },
                        adresse = new EiendommensAdresseType
                        {
                            adresselinje1 = "Storgata 3",
                            postnr = "7003",
                            poststed = "Trondheim"
                        }
                    },
                    sluttbrukersystemVaarReferanse = "ref-1"
                },

                new NaboGjenboerType()
                {
                    partstype = new KodeType()
                    {
                        kodebeskrivelse = "Privatperson",
                        kodeverdi = "Privatperson"
                    },
                    navn = "Tom Tømrer",
                    //foedselsnummer = "10028600254",
                    foedselsnummer = encrypt.EncryptText("10028600254"),

                    adresse = new no.kxml.skjema.dibk.nabovarselV3.EnkelAdresseType()
                    {
                        adresselinje1 = "Storgata 3",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    gjelderNaboeiendom = new EiendomType()
                    {
                        eiendomsidentifikasjon = new MatrikkelnummerType
                        {
                            gaardsnummer = "109",
                            bruksnummer = "1",
                            kommunenummer = "5001",
                            seksjonsnummer = "1"
                        },
                        adresse = new EiendommensAdresseType
                        {
                            adresselinje1 = "Storgata 3",
                            postnr = "7003",
                            poststed = "Trondheim"
                        }
                    },
                    sluttbrukersystemVaarReferanse = "ref-2"
                },

                new NaboGjenboerType()
                {
                    partstype = new KodeType()
                    {
                        kodebeskrivelse = "Foretak",
                        kodeverdi = "Foretak"
                    },
                    navn = "FANA OG HAFSLO REVISJON",
                    organisasjonsnummer = "910297937",

                    adresse = new no.kxml.skjema.dibk.nabovarselV3.EnkelAdresseType()
                    {
                        adresselinje1 = "Rahangurra 2",
                        postnr = "9730",
                        poststed = "Karasjok"
                    },
                    gjelderNaboeiendom = new EiendomType()
                    {
                        eiendomsidentifikasjon = new MatrikkelnummerType
                        {
                            gaardsnummer = "109",
                            bruksnummer = "2",
                            kommunenummer = "5001",
                            seksjonsnummer = "1"
                        },
                        adresse = new EiendommensAdresseType
                        {
                            adresselinje1 = "Storgata 2",
                            postnr = "7003",
                            poststed = "Trondheim"
                        }
                    },
                    sluttbrukersystemVaarReferanse = "ref-4"
                }
            };


            nabovarsel.ansvarligSoeker = new PartType
            {
                partstype = new KodeType
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "911455307",
                navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                telefonnummer = "12345678",
                mobilnummer = "48955163",
                epost = "tor@arkitektum.no",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Foretningsgata 234",
                    postnr = "9520",
                    poststed = "KAUTOKEINO",
                    landkode = "no",
                }
            };

            // dispensasjon
            nabovarsel.dispensasjon = new[]
            {
                new DispensasjonType()
                {
                    dispensasjonstype = new KodeType()
                    {
                        kodeverdi = "PLAN",
                        kodebeskrivelse = "Arealplan"
                    },
                    beskrivelse = "Søknad om dispensasjon fra bestemmelsen NN i plan",
                    begrunnelse = "Begrunnelse for dispensasjon"
                }
            };

            nabovarsel.foelgebrev =
                "Det søkes om fasadeendring av gitt bygg som beskrevet i vedlagt materiale.";


            nabovarsel.signatur = new SignaturType
            {
                signaturdato = new DateTime(2016, 09, 08),
                signaturdatoSpecified = true,
                signertAv = "",
                signertPaaVegneAv = "",
                signeringssteg = ""
            };

            nabovarsel.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            nabovarsel.soeknadensHjemmeside = "www.hjemmeside.no";
            nabovarsel.prosjektnavn = "";

            return nabovarsel;
        }
    }
}
