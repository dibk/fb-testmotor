﻿using no.kxml.skjema.dibk.etttrinnsoknadV2;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class EttTrinnsoknadV2ExampleGenerator
    {
        public EttTrinnType NyEttTrinnSoknad()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            var ettTrinnSoknadV2 = new EttTrinnType();

            ettTrinnSoknadV2.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            ettTrinnSoknadV2.prosjektnavn = "Revitalisering av sentrum";

            // Tiltakshaver
            ettTrinnSoknadV2.tiltakshaver =
                new PartType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Privatperson",
                        kodebeskrivelse = "Privatperson"
                    },
                    foedselsnummer = "08021612345",
                    navn = "Hans Hansen",
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Storgata 5",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "hanshansen@bmail.no",
                    signaturdato = new System.DateTime(2016, 07, 28),
                    signaturdatoSpecified = true
                };


            // ansvarlig soeker
            ettTrinnSoknadV2.ansvarligSoeker =
                new PartType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    organisasjonsnummer = "974760673", // 974760673 BRREG, Altinn brukerservice
                    navn = "Siv. Ark. Fjell og Sønner",
                    kontaktperson = "Benjamin Fjell",
                    adresse = new EnkelAdresseType
                    {
                        adresselinje1 = "Lillegata 5",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "post@fjell.no",
                    signaturdato = new System.DateTime(2016, 07, 26),
                    signaturdatoSpecified = true
                };


            // eiendomByggested
            ettTrinnSoknadV2.eiendomByggested = new[]
            {
                new EiendomType
                {
                    kommunenavn = "Nome",
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Nygate 33",
                        postnr = "3825",
                        poststed = "Lunde"
                    },
                    bygningsnummer = "1234567896",
                    bolignummer = "H0101"
                }
            };

            var rammebetingelserGjeldendePlan = new PlanType
            {
                utnyttingsgrad = 23.0,
                utnyttingsgradSpecified = true,
                plantype = new KodeType
                {
                    kodeverdi = "RP",
                    kodebeskrivelse = "Reguleringsplan"
                },
                navn = "Naustgrendanabbevannet",
                formaal = "Fritidsbebyggelse",
                andreRelevanteKrav = "Andre relevante krav her",
                beregningsregelGradAvUtnytting = new KodeType
                {
                    kodeverdi = "%BYA",
                    kodebeskrivelse = "Prosent bebygd areal"
                }
            };
            
            // rammebetingelser
            var rammebetingelserAdkomst = new VegType
            {
                nyeEndretAdkomst = false,
                nyeEndretAdkomstSpecified = true,
                erTillatelseGittRiksFylkesveg = false,
                erTillatelseGittRiksFylkesvegSpecified = true,
                erTillatelseGittKommunalVeg = false,
                erTillatelseGittKommunalVegSpecified = true,
                erTillatelseGittPrivatVeg = true,
                erTillatelseGittPrivatVegSpecified = true,
                vegtype = new[]
                {
                    new KodeType
                    {
                        kodeverdi = "PrivatVeg",
                        kodebeskrivelse = "PrivatVeg"
                    }
                }
            };

            var rammebetingelserArealdisponering = new ArealdisponeringType
            {
                tomtearealByggeomraade = 1000.5,
                tomtearealByggeomraadeSpecified = true,
                tomtearealSomTrekkesFra = 30.2,
                tomtearealSomTrekkesFraSpecified = true,
                tomtearealSomLeggesTil = 50.3,
                tomtearealSomLeggesTilSpecified = true,
                tomtearealBeregnet = 1020,
                tomtearealBeregnetSpecified = true,
                beregnetMaksByggeareal = 300.2,
                beregnetMaksByggearealSpecified = true,
                arealBebyggelseEksisterende = 200.4,
                arealBebyggelseEksisterendeSpecified = true,
                arealBebyggelseSomSkalRives = 10.6,
                arealBebyggelseSomSkalRivesSpecified = true,
                arealBebyggelseNytt = 30.5,
                arealBebyggelseNyttSpecified = true,
                parkeringsarealTerreng = 18.0,
                parkeringsarealTerrengSpecified = true,
                arealSumByggesak = 240.5,
                arealSumByggesakSpecified = true,
                beregnetGradAvUtnytting = 23.36,
                beregnetGradAvUtnyttingSpecified = true
                
            };

            var rammebetingerlserGenerelleVilkaar = new GenerelleVilkaarType
            {
                oppfyllesVilkaarFor3Ukersfrist = false,
                oppfyllesVilkaarFor3UkersfristSpecified = true,
                beroererTidligere1850 = false,
                beroererTidligere1850Specified = true,
                forhaandskonferanseAvholdt = true,
                forhaandskonferanseAvholdtSpecified = true,
                paalagtUavhengigKontroll = true,
                paalagtUavhengigKontrollSpecified = true,
                beroererArbeidsplasser = false,
                beroererArbeidsplasserSpecified = true,
                utarbeideAvfallsplan = false,
                utarbeideAvfallsplanSpecified = true,
                behovForTillatelse = true,
                behovForTillatelseSpecified = true,
                fravikFraByggtekniskForskrift = false,
                fravikFraByggtekniskForskriftSpecified = true,
                prosjekteringTEK10 = false,
                prosjekteringTEK10Specified = true
                
            };

            var rammebetingelserVannforsyning = new VannforsyningType
            {
                tilknytningstype = new KodeType
                {
                    kodeverdi = "AnnenPrivatInnlagt",
                    kodebeskrivelse = "Annen privat vannforsyning, innlagt vann"
                },
                beskrivelse = "Felles privat vannforsyning i hele reguleringsområdet",
                krysserVannforsyningAnnensGrunn = true,
                krysserVannforsyningAnnensGrunnSpecified = true,
                tinglystErklaering = true,
                tinglystErklaeringSpecified = true
            };

            var rammebetingeslerAvloep = new AvloepType
            {
                tilknytningstype = new KodeType
                {
                    kodeverdi = "OffentligKloakk",
                    kodebeskrivelse = "Offentlig avløpsanlegg"
                },
                installereVannklosett = true,
                installereVannklosettSpecified = true,
                utslippstillatelse = true,
                utslippstillatelseSpecified = true,
                krysserAvloepAnnensGrunn = true,
                krysserAvloepAnnensGrunnSpecified = true,
                tinglystErklaering = true,
                tinglystErklaeringSpecified = true,
                overvannTerreng = false,
                overvannTerrengSpecified = true,
                overvannAvloepssystem = true,
                overvannAvloepssystemSpecified = true
            };

            var rammebetingelserKravTilByggegrunn = new KravTilByggegrunnType
            {
                flomutsattOmraade = false,
                flomutsattOmraadeSpecified = true,
                f1 = true,
                f1Specified = true,
                f2 = false,
                f2Specified = true,
                f3 = false,
                f3Specified = true,
                skredutsattOmraade = false,
                skredutsattOmraadeSpecified = true,
                s1 = false,
                s1Specified = true,
                s2 = false,
                s2Specified = true,
                s3 = false,
                s3Specified = true,
                miljoeforhold = false,
                miljoeforholdSpecified = true
            };


            var rammebetingelserLoftinredninger = new LoefteinnretningerType
            {
                erLoefteinnretningIBygning = true,
                erLoefteinnretningIBygningSpecified = true,
                planleggesLoefteinnretningIBygning = true,
                planleggesLoefteinnretningIBygningSpecified = true,
                planleggesHeis = true,
                planleggesHeisSpecified = true,
                planleggesTrappeheis = false,
                planleggesTrappeheisSpecified = true,
                planleggesRulletrapp = false,
                planleggesRulletrappSpecified = true,
                planleggesLoefteplattform = false,
                planleggesLoefteplattformSpecified = true
            };


            var rammebetingerlserPlassering = new PlasseringType
            {
                konfliktHoeyspentkraftlinje = false,
                konfliktHoeyspentkraftlinjeSpecified = true,
                konfliktVannOgAvloep = false,
                konfliktVannOgAvloepSpecified = true
            };

            ettTrinnSoknadV2.rammebetingelser = new RammerType
            {
                adkomst = rammebetingelserAdkomst,
                arealdisponering = rammebetingelserArealdisponering,
                generelleVilkaar = rammebetingerlserGenerelleVilkaar,
                vannforsyning = rammebetingelserVannforsyning,
                avloep = rammebetingeslerAvloep,
                kravTilByggegrunn = rammebetingelserKravTilByggegrunn,
                loefteinnretninger = rammebetingelserLoftinredninger,
                plassering = rammebetingerlserPlassering,
                gjeldendePlan = rammebetingelserGjeldendePlan
            };


            // beskrivelseAvTiltak
            ettTrinnSoknadV2.beskrivelseAvTiltak = new TiltakType[]
            {
                new TiltakType
                {
                    bruk = new FormaalType
                    {
                        anleggstype = new KodeType
                        {
                            kodeverdi = "andre",
                            kodebeskrivelse = "Andre"
                        },
                        naeringsgruppe = new KodeType
                        {
                            kodeverdi = "Y",
                            kodebeskrivelse = "Næringsgruppe for annet som ikke er næring"
                        },
                        bygningstype = new KodeType
                        {
                            kodeverdi = "161",
                            kodebeskrivelse = "Hytter, sommerhus ol fritidsbygning"
                        },
                        tiltaksformaal = new[]
                        {
                            new KodeType
                            {
                                kodeverdi = "Fritidsbolig",
                                kodebeskrivelse = "Fritidsbolig"
                            },
                            new KodeType
                            {
                                kodeverdi = "Annet",
                                kodebeskrivelse = "Annet"
                            },
                        },
                        beskrivPlanlagtFormaal = "Ny fritidsbolig med laftet anneks"
                    },
                    type = new[]
                    {
                        new KodeType
                        {
                            kodeverdi = "nyttbyggboligformal",
                            kodebeskrivelse = "Nytt bygg - Boligformål"
                        }
                    },
                    foelgebrev =
                        "Det søkes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges først og benyttes som \"arbeidsbrakke\" mens hovedhytta bygges."
                }
            };

            ettTrinnSoknadV2.soekesOmDispensasjon = false;
            ettTrinnSoknadV2.soekesOmDispensasjonSpecified = true;

            // dispensasjon
            ettTrinnSoknadV2.dispensasjon = new[]
            {
                new DispensasjonType()
                {
                    dispensasjonstype = new KodeType()
                    {
                        kodeverdi = "TEK",
                        kodebeskrivelse = "Byggteknisk forskrift med veiledning"
                    },
                    beskrivelse = "Søknad om dispensasjon fra bestemmelsen NN i plan",
                    begrunnelse = "Begrunnelse for dispensasjon"
                }
            };

            // soekesOmDispensasjon
            ettTrinnSoknadV2.soekesOmDispensasjon = true;
            ettTrinnSoknadV2.soekesOmDispensasjonSpecified = true;

            // Varsling
            ettTrinnSoknadV2.varsling = new VarslingType()
            {
                fritattFraNabovarsling = true,
                fritattFraNabovarslingSpecified = true,
                foreliggerMerknader = false,
                foreliggerMerknaderSpecified = true,
                antallMerknader = "1",
                vurderingAvMerknader = "Merknaden er tatt til følge",
                soeknadensHjemmeside = "http://www.dibk.no",
                soeknadSeesKontaktperson = "Hilde"
            };

            // return data model with example data
            return ettTrinnSoknadV2;
        }
    }
}