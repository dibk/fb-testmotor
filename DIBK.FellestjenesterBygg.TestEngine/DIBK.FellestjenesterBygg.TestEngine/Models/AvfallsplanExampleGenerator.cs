﻿using no.kxml.skjema.dibk.avfallsplan;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class AvfallsplanExampleGenerator
    {
        public AvfallsplanType NyAvfallsplan()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var avfallsplan = new AvfallsplanType();

            avfallsplan.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2017",
                sakssekvensnummer = "3456"
            };

            avfallsplan.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer =  rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 1",
                        postnr = "3800",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    bygningsnummer = "80466985",
                    bolignummer = "H0102"
                }
            };

            avfallsplan.avfall = new AvfallsTypeType
            {
                farlig = new FarligType
                {
                    FarligAvfall = new[]
                    {
                        new FarligAvfallType()
                        {
                            kodeliste = new KodeType()
                            {
                                kodeverdi = "7051 - Maling, lim, lakk som er farlig avfall",
                                kodebeskrivelse = "7051 - Maling, lim, lakk som er farlig avfall"
                            },
                            plan = new PlanType
                            {
                                beregnetMengde = new BeregnetMengdeType
                                {
                                    fraksjonerSomSkalKildesorteres = 0.1
                                }
                            },
                            sluttrapport = new SluttrapportType
                            {
                                disponeringsmaate = new DisponeringsmaateType
                                {
                                    mengdeLevertTilGodkjentAvfallsanlegg = 0.3,
                                    leveringsstedGodkjentAvfallsanlegg = "IATA, Stormo"
                                },
                                faktiskMengde = new FaktiskMengdeType
                                {
                                    fraksjonerSomErKildesortert = 0.3
                                }
                            }
                        }
                    }, 
                    sumPlan = 0.1,
                    sumSluttrapport = 0.3
                },
                ordinaer = new OrdinaertType
                {
                    OrdinaertAvfall = new[]
                    {
                        new OrdinaertAvfallType()
                        {
                            kodeliste = new KodeType()
                        {
                            kodeverdi = "Trevirke (ikke kreosot- og CCA-impregnert",
                            kodebeskrivelse = "Trevirke (ikke kreosot- og CCA-impregnert"
                        },
                        plan = new PlanType
                        {
                            beregnetMengde = new BeregnetMengdeType
                            {
                                fraksjonerSomSkalKildesorteres = 1.0
                            }
                        },
                        sluttrapport = new SluttrapportType
                        {
                            disponeringsmaate = new DisponeringsmaateType
                            {
                                mengdeLevertTilGodkjentAvfallsanlegg = 1.0,
                                leveringsstedGodkjentAvfallsanlegg = "IATA, Stormo",
                                mengdeLevertTilGjenvinning = 1.5,
                                leveringsstedGjenvinning = "IATA, Stormo"
                            },
                            faktiskMengde = new FaktiskMengdeType
                            {
                                fraksjonerSomErKildesortert = 2.5
                            }
                        }
                        },
                        new OrdinaertAvfallType()
                        {
                            kodeliste = new KodeType()
                        {
                            kodeverdi = "Papir, papp og kartong",
                            kodebeskrivelse = "Papir, papp og kartong"
                        },
                        plan = new PlanType
                        {
                            beregnetMengde = new BeregnetMengdeType
                            {
                                fraksjonerSomSkalKildesorteres = 0.5
                            }
                        }
                        }, 
                        new OrdinaertAvfallType()
                        {
                            kodeliste = new KodeType()
                            {
                                kodeverdi = "Glass",
                                kodebeskrivelse = "Glass"
                            },
                            plan = new PlanType()
                            {
                                beregnetMengde = new BeregnetMengdeType
                                {
                                    fraksjonerSomSkalKildesorteres = 0.8
                                }
                            }
                        },
                        new OrdinaertAvfallType()
                        {
                            kodeliste = new KodeType()
                        {
                            kodeverdi = "Gipsbaserte materialer",
                            kodebeskrivelse = "Gipsbaserte materialer"
                        },
                        plan = new PlanType
                        {
                            beregnetMengde = new BeregnetMengdeType
                            {
                                fraksjonerSomSkalKildesorteres = 3.0
                            }
                        },
                        sluttrapport = new SluttrapportType
                        {
                            disponeringsmaate = new DisponeringsmaateType
                            {
                                mengdeLevertTilGodkjentAvfallsanlegg = 0.2,
                                leveringsstedGodkjentAvfallsanlegg = "IATA, Stormo"
                            },
                            faktiskMengde = new FaktiskMengdeType
                            {
                                fraksjonerSomErKildesortert = 0.2
                            }
                        }
                        },
                    },

                    sumMengdeLevertGjenvinning = 1.5,
                    sumMengdeLevertGodkjentAvfallsanlegg = 1.2,
                    sumPlan = 5.3,
                    sumSluttrapport = 2.7,
                },
                                   
                blandet = new AvfallType
                {
                    plan = new PlanType
                    {
                        sumPlan = 2.0
                    },
                    sluttrapport = new SluttrapportType
                    {
                        sumSluttrapport = 2.4
                    }
                },
                gjenstaaende = "",
                areal = 9.0,
                sorteringsgrad = "56",
                totalsum = new AvfallType
                {
                    plan = new PlanType
                    {
                        sumPlan = 7.4

                    }, 
                    sluttrapport = new SluttrapportType
                    {
                        sumSluttrapport = 5.4
                    }
                }
            };

            avfallsplan.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910297937",
                navn = "FANA OG HAFSLO REVISJON",
                kontaktperson = "Siv. Ing. Borge",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "soeker@domene.no"
            };

            avfallsplan.signatur = new SignaturType
            {
                signaturdato = new System.DateTime(2016, 09, 08),
                signertAv = "",
                signertPaaVegneAv = "",
                signeringssteg = ""
            };


            return avfallsplan;
        }
    }
}