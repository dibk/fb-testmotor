﻿using System;
using no.kxml.skjema.dibk.gjennomforingsplanV2;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class GjennomforingsplanV3ExampleGenerator
    {

        public no.kxml.skjema.dibk.gjennomforingsplanV3.GjennomfoeringsplanType NyGjennomforingsplanV3()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            var gjennomforingsplanV3 = new no.kxml.skjema.dibk.gjennomforingsplanV3.GjennomfoeringsplanType();

            gjennomforingsplanV3.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            gjennomforingsplanV3.sluttbrukersystemUrl = "https://fellesbygg.dibk.no";
            gjennomforingsplanV3.prosjektnavn = "Hovedgata Nyutvikling";
            gjennomforingsplanV3.hovedinnsendingsnummer = "35001";
            gjennomforingsplanV3.kommunensSaksnummer = new no.kxml.skjema.dibk.gjennomforingsplanV3.SaksnummerType
            {
                saksaar = "2016",
                sakssekvensnummer = "555"
            };

            gjennomforingsplanV3.versjon = "v93";


            // ======= eiendomByggested ======= 
            gjennomforingsplanV3.eiendomByggested = new[]
            {
                new no.kxml.skjema.dibk.gjennomforingsplanV3.EiendomType()
                {
                    eiendomsidentifikasjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "5"
                    },
                    adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 1",
                        postnr = "3800",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    bygningsnummer = "80466985",
                    bolignummer = "H0101"
                }
            };


            // ======= gjennomfoeringsplan ======= 
            gjennomforingsplanV3.gjennomfoeringsplan = new[]
            {
                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    fagomraade = "Arkitektur",
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    arbeidsomraade = "Arkitektur prosjektering av bygg",
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new no.kxml.skjema.dibk.gjennomforingsplanV3.ForetakType()
                    {
                        partstype = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Bob Andersen",
                        organisasjonsnummer = "912660680", // 912660680 NÆRINGS- OG FISKERIDEPARTEMENTET
                        navn = "ARKITEKT ANDERSEN AS",
                        adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 1",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345678",
                        mobilnummer = "87654321",
                        epost = "bobby@domene.no",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        harSentralGodkjenningSpecified = true,
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,
                    samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,
                    //samsvarKontrollForeliggerVedIgangsettingstillatelse = new System.DateTime(2017, 04, 12),
                    //samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,
                    //samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = new System.DateTime(2017, 06, 12),
                    //samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedFerdigattest = false,
                    //samsvarKontrollForeliggerVedFerdigattest = new System.DateTime(2017, 08, 12),
                    //samsvarKontrollForeliggerVedFerdigattestSpecified = true,

                    ansvarsomraadetAvsluttet = true,

                    erklaeringSignert = true,
                    sluttbrukersystemReferanse = System.Guid.NewGuid().ToString(),
                    sluttbrukersystemStatus = "Signert",
                    erklaeringArkivreferanse = "AR132456"
                },

                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    fagomraade = "Brannalarmanlegg_PRO",
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    arbeidsomraade = "Design av brannalarmsystem",
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new no.kxml.skjema.dibk.gjennomforingsplanV3.ForetakType()
                    {
                        partstype = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Bill Andersen",
                        organisasjonsnummer = "974760223", 
                        navn = "Brann Systemer AS",
                        adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 2",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345678",
                        mobilnummer = "87654321",
                        epost = "billy@domene.no",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        harSentralGodkjenningSpecified = true,
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = false,
                    //samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    //samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = true,
                    samsvarKontrollForeliggerVedIgangsettingstillatelse = new System.DateTime(2017, 04, 12),
                    samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,
                    //samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = new System.DateTime(2017, 06, 12),
                    //samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedFerdigattest = false,
                    //samsvarKontrollForeliggerVedFerdigattest = new System.DateTime(2017, 08, 12),
                    //samsvarKontrollForeliggerVedFerdigattestSpecified = true,

                    ansvarsomraadetAvsluttet = true,

                    erklaeringSignert = true,
                    sluttbrukersystemReferanse = System.Guid.NewGuid().ToString(),
                    sluttbrukersystemStatus = "Signert",
                    erklaeringArkivreferanse = "AR132456"
                },


                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    fagomraade = "Ledesystem_PRO",
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    arbeidsomraade = "Planlegging av ledesystem",
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new no.kxml.skjema.dibk.gjennomforingsplanV3.ForetakType()
                    {
                        partstype = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Frank Sappar",
                        organisasjonsnummer = "974760223", // 974760223 DIBK
                        navn = "Ledende Lede Folk AS",
                        adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 3",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345679",
                        mobilnummer = "87654322",
                        epost = "hallo@byggselsk.no",
                        signaturdato = new System.DateTime(2016, 04, 11),
                        harSentralGodkjenningSpecified = true,
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = false,
                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,
                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,
                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    samsvarKontrollForeliggerVedFerdigattest = new System.DateTime(2016, 11, 29),
                    samsvarKontrollForeliggerVedFerdigattestSpecified = true,

                    ansvarsomraadetAvsluttet = true,

                    erklaeringSignert = true,
                    sluttbrukersystemReferanse = "ref2",
                    sluttbrukersystemStatus = "Signert",
                    erklaeringArkivreferanse = "AR132457"
                },
                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    fagomraade = "Oppmålingsteknisk prosjektering",
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    arbeidsomraade = "Oppmåling av tomt",
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new no.kxml.skjema.dibk.gjennomforingsplanV3.ForetakType()
                    {
                        kontaktperson = "Lil E. Taa",
                        partstype = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        organisasjonsnummer = "913255852", // 913255852 DIBK Gjøvik
                        navn = "JENSEN PLAN OG MÅL AS",
                        adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 2",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345679",
                        mobilnummer = "87654322",
                        epost = "dweezil@domene.no",
                        signaturdato = new System.DateTime(2016, 01, 11),
                        harSentralGodkjenningSpecified = true,
                        harSentralGodkjenning = false
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = false,
                    //samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    //samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = true,
                    samsvarKontrollForeliggerVedIgangsettingstillatelse = new System.DateTime(2017, 04, 12),
                    samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,
                    //samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = new System.DateTime(2017, 06, 12),
                    //samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedFerdigattest = false,
                    //samsvarKontrollForeliggerVedFerdigattest = new System.DateTime(2017, 08, 12),
                    //samsvarKontrollForeliggerVedFerdigattestSpecified = true,

                    ansvarsomraadetAvsluttet = true,

                    erklaeringSignert = true,
                    sluttbrukersystemReferanse = "ref2",
                    sluttbrukersystemStatus = "Signert",
                    erklaeringArkivreferanse = "AR132457"
                },
                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    fagomraade = "Vannforsynings- og avløpsanlegg_PRO",
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    arbeidsomraade = "Prosjektering av vann og avløp, samt sanitære installasjoner",
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new no.kxml.skjema.dibk.gjennomforingsplanV3.ForetakType()
                    {
                        partstype = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Harry Viskositet",
                        organisasjonsnummer = "974702665", // 974702665 DIBK Oslo
                        navn = "HERMANSEN VVS AS",
                        adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 3",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "92345679",
                        mobilnummer = "87688322",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        epost = "hermansenVvs@vvsH.no",
                        harSentralGodkjenningSpecified = true,
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = false,
                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,
                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = new System.DateTime(2016, 06, 29),
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true,
                    samsvarKontrollPlanlagtVedFerdigattest = false,

                    ansvarsomraadetAvsluttet = true,
                },
                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    arbeidsomraade = "Pumpegreier",
                    foretak = new no.kxml.skjema.dibk.gjennomforingsplanV3.ForetakType()
                    {
                        partstype = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Birger Pumpetang",
                        organisasjonsnummer = "914994780",  // 914994780 Arkitektum
                        navn = "Byggmester Bob AS",
                        adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 4",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "92345579",
                        mobilnummer = "87688822",
                        epost = "firmapost@ByggBob.no",
                        harSentralGodkjenningSpecified = true,
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = false,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,

                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    ansvarsomraadetAvsluttet = false
                },
                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    fagomraade = "Varme- og kuldeinstallasjoner_UTF",
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    arbeidsomraade = "Varme- og kuldeinstallasjoner",
                    samsvarKontrollPlanlagtVedRammetillatelse = false,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,

                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    ansvarsomraadetAvsluttet = false
                },
                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    fagomraade = "Veg- og grunnarbeider_UTF",
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    arbeidsomraade = "Annkomst veg ",
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = false,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,

                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    ansvarsomraadetAvsluttet = false
                },
                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    fagomraade = "Montering av bærende metall- eller betongkonstruksjoner_UTF",
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    arbeidsomraade = "Metall arbeid",
                    foretak = new no.kxml.skjema.dibk.gjennomforingsplanV3.ForetakType()
                    {
                        partstype = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Margit Berg",
                        organisasjonsnummer = "974760223", // 974760223 DIBK
                        navn = "Berg Oppmåling AS",
                        adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 7",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "97555579",
                        mobilnummer = "87611866",
                        epost = "berg@oppmaaling.no",
                        signaturdato = new System.DateTime(2016, 01, 04),
                        harSentralGodkjenningSpecified = true,
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = false,
                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,
                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,
                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    ansvarsomraadetAvsluttet = false
                },


                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    fagomraade = "Våtrom (i nye boliger)_KONTROLL",
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontrollerende"
                    },
                    arbeidsomraade = "Mebran inspeksjon og testing",
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new no.kxml.skjema.dibk.gjennomforingsplanV3.ForetakType()
                    {
                        partstype = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Bob Andersen",
                        organisasjonsnummer = "912660680", // 912660680 NÆRINGS- OG FISKERIDEPARTEMENTET
                        navn = "Kontroll Nisser 1 AS",
                        adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 24",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345678",
                        mobilnummer = "87654321",
                        epost = "bobby@domene.no",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        harSentralGodkjenningSpecified = true,
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,
                    //samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    //samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = true,
                    //samsvarKontrollForeliggerVedIgangsettingstillatelse = new System.DateTime(2017, 04, 12),
                    //samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = true,
                    //samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = new System.DateTime(2017, 06, 12),
                    //samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedFerdigattest = true,
                    //samsvarKontrollForeliggerVedFerdigattest = new System.DateTime(2017, 08, 12),
                    //samsvarKontrollForeliggerVedFerdigattestSpecified = true,

                    ansvarsomraadetAvsluttet = false,

                    erklaeringSignert = true,
                    sluttbrukersystemReferanse = System.Guid.NewGuid().ToString(),
                    sluttbrukersystemStatus = "Signert",
                    erklaeringArkivreferanse = "AR132456"
                },

                new no.kxml.skjema.dibk.gjennomforingsplanV3.AnsvarsomraadeType()
                {
                    fagomraade = "Våtrom (i nye boliger)_KONTROLL",
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontrollerende"
                    },
                    arbeidsomraade = "Mebran inspeksjon og testing",
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new no.kxml.skjema.dibk.gjennomforingsplanV3.ForetakType()
                    {
                        partstype = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Bob Andersen",
                        organisasjonsnummer = "912660680", // 912660680 NÆRINGS- OG FISKERIDEPARTEMENTET
                        navn = "Kontroll Nisser 2 AS",
                        adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 24",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345678",
                        mobilnummer = "87654321",
                        epost = "bobby@domene.no",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        harSentralGodkjenningSpecified = true,
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,
                    samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = true,
                    samsvarKontrollForeliggerVedIgangsettingstillatelse = new System.DateTime(2017, 04, 12),
                    samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = new System.DateTime(2017, 06, 12),
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedFerdigattest = true,
                    samsvarKontrollForeliggerVedFerdigattest = new System.DateTime(2017, 08, 12),
                    samsvarKontrollForeliggerVedFerdigattestSpecified = true,

                    ansvarsomraadetAvsluttet = true,

                    erklaeringSignert = true,
                    sluttbrukersystemReferanse = System.Guid.NewGuid().ToString(),
                    sluttbrukersystemStatus = "Signert",
                    erklaeringArkivreferanse = "AR132456"
                },



            };

            gjennomforingsplanV3.ansvarligSoeker = new no.kxml.skjema.dibk.gjennomforingsplanV3.PartType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV3.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "913255852", // 913255852 DIBK Gjøvik
                navn = "AS Foretak Ansv. Søk.",
                kontaktperson = "Ingvild Testperson Halland",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV3.EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "soeker@domene.no"
            };

            return gjennomforingsplanV3;
        }
    }
}
