﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.Encryption
{
    public class Encryption
    {

        internal static RSACryptoServiceProvider PublicKeyProvider;

        public Encryption()
        {
            string publicKeyString = ReadPublicKeyFromEmbeddedResorce();
            PublicKeyProvider = ImportPublicKey(publicKeyString);
        }



        public string EncryptText(string clearText)
        {
            return EncryptString(PublicKeyProvider, clearText);
        }



        private static string EncryptString(RSACryptoServiceProvider rsaCryptoPublicKey, string clearText)
        {
            UnicodeEncoding byteConverter = new UnicodeEncoding();
            byte[] clearTextBytes = byteConverter.GetBytes(clearText);
            byte[] cipherBytes = rsaCryptoPublicKey.Encrypt(clearTextBytes, false);
            string cipherTextBase64 = Convert.ToBase64String(cipherBytes);

            return cipherTextBase64;
        }



        private static string ReadPublicKeyFromEmbeddedResorce()
        {
            // Load public key from Embedded resource file
            string fileContent;
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "DIBK.FellestjenesterBygg.TestEngine.Models.Encryption.ftbtest_public_key.pem";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                fileContent = reader.ReadToEnd();
            }

            return fileContent;
        }

        private static RSACryptoServiceProvider ImportPublicKey(string pem)
        {
            PemReader pr = new PemReader(new StringReader(pem));
            AsymmetricKeyParameter publicKey = (AsymmetricKeyParameter)pr.ReadObject();
            RSAParameters rsaParams = DotNetUtilities.ToRSAParameters((RsaKeyParameters)publicKey);

            RSACryptoServiceProvider csp = new RSACryptoServiceProvider();// cspParams);
            csp.ImportParameters(rsaParams);
            return csp;
        }



    }
}