﻿using no.kxml.skjema.dibk.tiltakutenansvarsrettV2;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class TiltakutenansvarsrettV2ExampleGenerator
    {
        public TiltakUtenAnsvarsrettType NyttTiltakUtenAnsvarsrett()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var tiltakutenansvarsrett = new TiltakUtenAnsvarsrettType
            {
                fraSluttbrukersystem = "Fellestjenester bygg testmotor",
                eiendomByggested = new[]
                {
                    new EiendomType()
                    {
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = "Storgata 19",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },

                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            kommunenummer = rand.GetRandomTestKommuneNummer(),
                            gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                            bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                            festenummer = "0",
                            seksjonsnummer = "0"
                        },
                        bygningsnummer = "123456789",
                        bolignummer = "H0201",
                        kommunenavn = "Nome"
                    }
                },

                beskrivelseAvTiltak = new[]
                {
                    new TiltakType()
                    {
                        bruk = new FormaalType()
                        {
                            beskrivPlanlagtFormaal = "Eksempelbeskrivelse av planlagt formål",
                            naeringsgruppe = new KodeType() {kodeverdi = "X", kodebeskrivelse = "Bolig"},
                            bygningstype = new[] {new KodeType {kodeverdi = "111", kodebeskrivelse = "Enebolig"}},
                            tiltaksformaal = new[] {new KodeType {kodeverdi = "Bolig", kodebeskrivelse = "Bolig"}},
                        },

                        type = new[]
                        {
                            new KodeType {kodeverdi = "bruksendringhoveddel", kodebeskrivelse = "Bruksendring fra tilleggsdel til hoveddel"}
                        },
                    }
                },

                tiltakshaver = new PartType()
                {
                    partstype = new KodeType { kodeverdi = "Privatperson", kodebeskrivelse = "Privatperson" },
                    foedselsnummer = "25125401530", // FILIP MOHAMED (TT02)
                    navn = "Filip Mohamed",
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Bøgata 1",
                        landkode = "NO",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                    },
                    telefonnummer = "12345678",
                    mobilnummer = "87654321",
                    epost = "navn@domene.no"
                },

                varsling = new VarslingType()
                {
                    fritattFraNabovarsling = true,
                    fritattFraNabovarslingSpecified = true,
                    foreliggerMerknaderSpecified = true,
                    foreliggerMerknader = false
                },

                prosjektnavn = "Bruksendring hos Filip",

                rammebetingelser = new RammerType()
                {
                    adkomst = new VegType()
                    {
                        nyeEndretAdkomst = false,
                        nyeEndretAdkomstSpecified = true,

                        vegrett = new[]
                        {
                            new VegrettType()
                            {
                                erTillatelseGittSpecified = true,
                                erTillatelseGitt = true,
                                vegtype = new KodeType() {kodeverdi = "KommunalVeg", kodebeskrivelse = "KommunalVeg"}
                            }
                        }
                    },
                    arealdisponering = new ArealdisponeringType()
                    {
                        tomtearealByggeomraade = 20.4,
                        tomtearealByggeomraadeSpecified = true,
                        tomtearealSomTrekkesFra = 4.2,
                        tomtearealSomTrekkesFraSpecified = true,
                        tomtearealSomLeggesTil = 12.6,
                        tomtearealSomLeggesTilSpecified = true,
                        tomtearealBeregnet = 24.1,
                        tomtearealBeregnetSpecified = true,
                        beregnetMaksByggeareal = 28.2,
                        beregnetMaksByggearealSpecified = true,
                        arealBebyggelseEksisterende = 89.2,
                        arealBebyggelseEksisterendeSpecified = true,
                        arealBebyggelseSomSkalRives = 8.4,
                        arealBebyggelseSomSkalRivesSpecified = true,
                        arealBebyggelseNytt = 16.3,
                        arealBebyggelseNyttSpecified = true,
                        parkeringsarealTerreng = 45.2,
                        parkeringsarealTerrengSpecified = true,
                        arealSumByggesak = 152.7,
                        arealSumByggesakSpecified = true,
                        beregnetGradAvUtnytting = 140.2,
                        beregnetGradAvUtnyttingSpecified = true
                    },
                    generelleVilkaar = new GenerelleVilkaarType()
                    {
                        oppfyllesVilkaarFor3Ukersfrist = true,
                        oppfyllesVilkaarFor3UkersfristSpecified = true,
                        beroererTidligere1850 = false,
                        beroererTidligere1850Specified = true,
                        behovForTillatelse = false,
                        behovForTillatelseSpecified = true
                    },
                    gjeldendePlan = new PlanType()
                    {
                        utnyttingsgrad = 24.5,
                        utnyttingsgradSpecified = true,
                        plantype = new KodeType { kodeverdi = "RP", kodebeskrivelse = "Reguleringsplan" },
                        navn = "Eksemplelplannavn",
                        formaal = "Byggeområde for boligbebyggelse",
                        andreRelevanteKrav = "",
                        beregningsregelGradAvUtnytting =
                            new KodeType { kodeverdi = "BYA", kodebeskrivelse = "Bebygd areal" }
                    },
                    vannforsyning = new VannforsyningType()
                    {
                        tilknytningstype =
                            new[]
                            {
                                new KodeType {kodeverdi = "TilknyttetOffVannverk", kodebeskrivelse = "Offentlig vannverk"} 
                                //new KodeType {kodeverdi = "AnnenPrivatInnlagt", kodebeskrivelse = "Annen Privat Innlagt"}
                            },
                        beskrivelse = "",
                        krysserVannforsyningAnnensGrunn = false,
                        krysserVannforsyningAnnensGrunnSpecified = true
                    },
                    avloep = new AvloepType()
                    {
                        tilknytningstype = new KodeType
                        {
                            //kodeverdi = "Offentlig avløpsanlegg",kodebeskrivelse = "Offentlig avløpsanlegg" 
                            kodeverdi = "PrivatKloakk",
                            kodebeskrivelse = "Privat Kloakk"
                        },
                        installereVannklosett = true,
                        installereVannklosettSpecified = true,
                        utslippstillatelse = true,
                        utslippstillatelseSpecified = true,
                        krysserAvloepAnnensGrunn = false,
                        krysserAvloepAnnensGrunnSpecified = true,
                        overvannTerreng = true,
                        overvannTerrengSpecified = true
                    },
                    kravTilByggegrunn = new KravTilByggegrunnType()
                    {
                        flomutsattOmraade = false,
                        flomutsattOmraadeSpecified = true,
                        skredutsattOmraade = false,
                        skredutsattOmraadeSpecified = true,
                        miljoeforhold = false,
                        miljoeforholdSpecified = true
                    },
                    plassering = new PlasseringType()
                    {
                        konfliktHoeyspentkraftlinje = false,
                        konfliktHoeyspentkraftlinjeSpecified = true,
                        minsteAvstandNabogrense = 5.2,
                        minsteAvstandNabogrenseSpecified = true,
                        konfliktVannOgAvloep = false,
                        konfliktVannOgAvloepSpecified = true,
                        minsteAvstandTilAnnenBygning = 10.3,
                        minsteAvstandTilAnnenBygningSpecified = true,
                        minsteAvstandTilMidtenAvVei = 25.7,
                        minsteAvstandTilMidtenAvVeiSpecified = true
                    }
                }
            };
            return tiltakutenansvarsrett;
        }
    }
}
