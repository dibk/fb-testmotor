﻿using no.kxml.skjema.dibk.nabovarselsvarPlan;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class NabovarselSvarPlanExampleGenerator
    {
        public SvarPaaNabovarselPlanType NySvarPaaNabovarsel()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var svarnabovarselPlan = new SvarPaaNabovarselPlanType();


            svarnabovarselPlan.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            svarnabovarselPlan.planNavn = "PlanNavn";
            svarnabovarselPlan.saksnummer = "";
            svarnabovarselPlan.planid = "";
        
            svarnabovarselPlan.beroertPart = new BeroertPartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                navn = "Ole Olsen",
                foedselsnummer = "08117000290"

            };

            svarnabovarselPlan.signatur = new SignaturType
            {
                signaturdato = new System.DateTime(2016, 09, 08),
                signertAv = "",
                signertPaaVegneAv = "",
            };

            return svarnabovarselPlan;
        }
    }
}