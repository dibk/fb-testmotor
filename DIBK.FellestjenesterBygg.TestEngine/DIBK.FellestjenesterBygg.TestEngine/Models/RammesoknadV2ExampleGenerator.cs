﻿using no.kxml.skjema.dibk.rammesoknadV2;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class RammesoknadV2ExampleGenerator
    {
        ExampleDataRandomizer rand = new ExampleDataRandomizer();

        public no.kxml.skjema.dibk.rammesoknadV2.RammetillatelseType NyRammetillatelse()
        {
            var rammetillatelseV2 = new RammetillatelseType();

            rammetillatelseV2.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            rammetillatelseV2.prosjektnavn = "Prosjektnavn RammeV2";

            // Tiltakshaver
            rammetillatelseV2.tiltakshaver =
                new PartType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Privatperson",
                        kodebeskrivelse = "Privatperson"
                    },
                    foedselsnummer = "08022016123",
                    navn = "Hans Hansen",
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Storgata 5",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "toroskar@arkitektum.no",
                    signaturdato = new System.DateTime(2016, 07, 28),
                    signaturdatoSpecified = true
                };


// ansvarlig soeker
            rammetillatelseV2.ansvarligSoeker =
                new PartType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    organisasjonsnummer = "911455307", // 914994780 Arkitektum
                    navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                    kontaktperson = "Benjamin Fjell",
                    adresse = new EnkelAdresseType
                    {
                        adresselinje1 = "Lillegata 5",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "tine@arkitektum.no",
                    signaturdato = new System.DateTime(2016, 07, 26),
                    signaturdatoSpecified = true
                };


            // eiendomByggested
            rammetillatelseV2.eiendomByggested = new[]
            {
                new EiendomType
                {
                    kommunenavn = "Kommunenavn",
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Nygate 33",
                        postnr = "3825",
                        poststed = "Lunde"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = "H0101"
                }
            };


            // rammebetingelser
            var rammebetingelserAdkomst = new VegType
            {
                nyeEndretAdkomst = true,
                nyeEndretAdkomstSpecified = true,
                erTillatelseGittRiksFylkesveg = true,
                erTillatelseGittRiksFylkesvegSpecified = true,
                vegtype = new[]
                {

                    new KodeType
                    {
                        kodeverdi = "RiksFylkesveg",
                        kodebeskrivelse = "Riksveg/fylkesveg"
                    }
                }
            };

            var rammebetingelserArealdisponering = new ArealdisponeringType
            {
                tomtearealByggeomraade = 1000.5,
                tomtearealByggeomraadeSpecified = true,
                gjeldendePlan = new PlanType
                {
                    utnyttingsgrad = 23.0,
                    utnyttingsgradSpecified = true,
                    plantype = new KodeType
                    {
                        kodeverdi = "RP",
                        kodebeskrivelse = "Reguleringsplan"
                    },
                    navn = "Naustgrendanabbevannet",
                    formaal = "Fritidsbegyggelse",
                    andreRelevanteKrav = "andre relevante krav her",
                    beregningsregelGradAvUtnytting = new KodeType
                    {
                        kodeverdi = "%BYA",
                        kodebeskrivelse = "Prosent bebygd areal"
                    }
                },
                tomtearealSomTrekkesFra = 30.2,
                tomtearealSomTrekkesFraSpecified = true,
                tomtearealSomLeggesTil = 50.3,
                tomtearealSomLeggesTilSpecified = true,
                tomtearealBeregnet = 1020,
                tomtearealBeregnetSpecified = true,
                beregnetMaksByggeareal = 300.2,
                beregnetMaksByggearealSpecified = true,
                arealBebyggelseEksisterende = 200.4,
                arealBebyggelseEksisterendeSpecified = true,
                arealBebyggelseSomSkalRives = 10.6,
                arealBebyggelseSomSkalRivesSpecified = true,
                arealBebyggelseNytt = 30.5,
                arealBebyggelseNyttSpecified = true,
                parkeringsarealTerreng = 18.0,
                parkeringsarealTerrengSpecified = true,
                arealSumByggesak = 240.5,
                arealSumByggesakSpecified = true,
                beregnetGradAvUtnytting = 23.36,
                beregnetGradAvUtnyttingSpecified = true
            };

            var rammebetingerlserGenerelleVilkaar = new GenerelleVilkaarType
            {
                beroererTidligere1850 = false,
                beroererTidligere1850Specified = true,
                forhaandskonferanseAvholdt = true,
                forhaandskonferanseAvholdtSpecified = true,
                paalagtUavhengigKontroll = false,
                paalagtUavhengigKontrollSpecified = true,
                beroererArbeidsplasser = false,
                beroererArbeidsplasserSpecified = true,
                utarbeideAvfallsplan = false,
                utarbeideAvfallsplanSpecified = true,
                behovForTillatelse = false,
                behovForTillatelseSpecified = true, 
                fravikFraByggtekniskForskrift = false,
                fravikFraByggtekniskForskriftSpecified = true, 
                prosjekteringTEK10 = false,
                prosjekteringTEK10Specified = true
            };

            var rammebetingelserVannforsyning = new VannforsyningType
            {
                tilknytningstype = new KodeType
                {
                    kodeverdi = "AnnenPrivatInnlagt",
                    kodebeskrivelse = "Annen privat vannforsyning, innlagt vann"
                },

                beskrivelse = "Felles privat vannforsyning i hele reguleringsområdet",
                krysserVannforsyningAnnensGrunn = true,
                krysserVannforsyningAnnensGrunnSpecified = true,
                tinglystErklaering = true,
                tinglystErklaeringSpecified = true
            };

            var rammebetingeslerAvloep = new AvloepType
            {
                tilknytningstype = new KodeType
                {
                    kodeverdi = "OffentligKloakk",
                    kodebeskrivelse = "Offentlig avløpsanlegg"
                },
                installereVannklosett = true,
                installereVannklosettSpecified = true,
                utslippstillatelse = true,
                utslippstillatelseSpecified = true,
                krysserAvloepAnnensGrunn = true,
                krysserAvloepAnnensGrunnSpecified = true,
                tinglystErklaering = true,
                tinglystErklaeringSpecified = true,
                overvannTerreng = false,
                overvannTerrengSpecified = true,
                overvannAvloepssystem = true,
                overvannAvloepssystemSpecified = true
            };

            var rammebetingelserKravTilByggegrunn = new KravTilByggegrunnType
            {
                flomutsattOmraade = false,
                flomutsattOmraadeSpecified = true,
                f1 = false,
                f1Specified = true,
                f2 = false,
                f2Specified = true,
                f3 = false,
                f3Specified = true,
                skredutsattOmraade = false,
                skredutsattOmraadeSpecified = true,
                s1 = false,
                s1Specified = true,
                s2 = false,
                s2Specified = true,
                s3 = false,
                s3Specified = true,
                miljoeforhold = false, 
                miljoeforholdSpecified = true
            };


            var rammebetingelserLoftinredninger = new LoefteinnretningerType
            {
                erLoefteinnretningIBygning = true,
                erLoefteinnretningIBygningSpecified = true,
                planleggesLoefteinnretningIBygning = true,
                planleggesLoefteinnretningIBygningSpecified = true,
                planleggesHeis = true,
                planleggesHeisSpecified = true,
                planleggesTrappeheis = false,
                planleggesTrappeheisSpecified = true,
                planleggesRulletrapp = false,
                planleggesRulletrappSpecified = true,
                planleggesLoefteplattform = false,
                planleggesLoefteplattformSpecified = true
            };


            var rammebetingerlserPlassering = new PlasseringType
            {
                konfliktHoeyspentkraftlinje = false,
                konfliktHoeyspentkraftlinjeSpecified = true,
                konfliktVannOgAvloep = false,
                konfliktVannOgAvloepSpecified = true
            };

            rammetillatelseV2.rammebetingelser = new RammerType
            {
                adkomst = rammebetingelserAdkomst,
                arealdisponering = rammebetingelserArealdisponering,
                generelleVilkaar = rammebetingerlserGenerelleVilkaar,
                vannforsyning = rammebetingelserVannforsyning,
                avloep = rammebetingeslerAvloep,
                kravTilByggegrunn = rammebetingelserKravTilByggegrunn,
                loefteinnretninger = rammebetingelserLoftinredninger,
                plassering = rammebetingerlserPlassering
            };


            // beskrivelseAvTiltak
            rammetillatelseV2.beskrivelseAvTiltak = new TiltakType[]
            {
                new TiltakType
                {
                    bruk = new FormaalType
                    {
                        anleggstype = new KodeType
                        {
                            kodeverdi = "andre",
                            kodebeskrivelse = "andre"
                        },
                        naeringsgruppe = new KodeType
                        {
                            kodeverdi = "Y",
                            kodebeskrivelse = "Næringsgruppe for annet som ikke er næring"
                        },
                        bygningstype = new KodeType
                            {
                                kodeverdi = "161",
                                kodebeskrivelse = "Hytter, sommerhus ol fritidsbygning"
                            },
                        tiltaksformaal = new[]
                        {
                            new KodeType
                            {
                                kodeverdi = "Fritidsbolig",
                                kodebeskrivelse = "Fritidsbolig"
                            },
                            new KodeType
                            {
                                kodeverdi = "Annet",
                                kodebeskrivelse = "Annet"
                            },
                        },
                        beskrivPlanlagtFormaal = "Ny fritidsbolig med laftet anneks"
                    },
                    type = new[]
                    {
                        new KodeType
                        {
                            kodeverdi = "nyttbyggboligformal",
                            kodebeskrivelse = "Nytt bygg - Boligformål"
                        }
                    },
                    foelgebrev =
                        "Det søkes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges først og benyttes som \"arbeidsbrakke\" mens hovedhytta bygges."
                }
            };


            rammetillatelseV2.soekesOmDispensasjon = true;
            rammetillatelseV2.soekesOmDispensasjonSpecified = true;


            // dispensasjon
            rammetillatelseV2.dispensasjon = new[]
            {
                new DispensasjonType()
                {
                    dispensasjonstype = new KodeType()
                    {
                        kodeverdi = "PLAN",
                        kodebeskrivelse = "Arealplan"
                    },
                    beskrivelse = "Søknad om dispensasjon fra bestemmelsen NN i plan",
                    begrunnelse = "Begrunnelse for dispensasjon"
                }
            };


           

            // Varsling
            rammetillatelseV2.varsling = new VarslingType()
            {
                fritattFraNabovarsling = true,
                fritattFraNabovarslingSpecified = true,
                foreliggerMerknader = false,
                foreliggerMerknaderSpecified = true,
                antallMerknader = "0",
                vurderingAvMerknader = "Merknaden er tatt til følge",
                soeknadensHjemmeside = "http://www.dibk.no",
                soeknadSeesKontaktperson = "Hilde"
            };

            // return data model with example data
            return rammetillatelseV2;
        }
    }
}
