﻿using no.kxml.skjema.dibk.endringavtillatelse;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class EndringAvTillatelseExampleGenerator
    {
        ExampleDataRandomizer rand = new ExampleDataRandomizer();

        public EndringAvTillatelseType NyEndringAvTillatelse()
        {
            var endringAvTillatelse = new EndringAvTillatelseType();

            endringAvTillatelse.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            endringAvTillatelse.prosjektnavn = "FTB Endringssøknad";

            endringAvTillatelse.kommunensSaksnummer = new SaksnummerType()
            {
                saksaar = "2018",
                sakssekvensnummer = "12345"
            };

            endringAvTillatelse.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    kommunenavn = "Hammerfest",
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = "2004",
                        gaardsnummer = "1",
                        bruksnummer = "3",
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Båtsfjorden 6",
                        postnr = "9664",
                        poststed = "SANDØYBOTN",
                        gatenavn = "Båtsfjorden",
                        husnr = "6"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = "H0101"
                }
            };

            endringAvTillatelse.endringSomKreverDispensasjon = false;
            endringAvTillatelse.endringSomKreverDispensasjonSpecified = true;
            endringAvTillatelse.endringAreal = false;
            endringAvTillatelse.endringArealSpecified = true;
            endringAvTillatelse.endringPlassering = false;
            endringAvTillatelse.endringPlasseringSpecified = true;
            endringAvTillatelse.endringFormaal = false;
            endringAvTillatelse.endringFormaalSpecified = true;
            endringAvTillatelse.endringBruk = false;
            endringAvTillatelse.endringBrukSpecified = true;
            endringAvTillatelse.endringAnnet = true;
            endringAvTillatelse.endringAnnetSpecified = true;
           

            endringAvTillatelse.rammebetingelser = new RammerType
            {
                gjeldendePlan = new [] 
                { new PlanType
                    {
                    utnyttingsgrad = 23.37,
                    utnyttingsgradSpecified = true,
                    plantype = new KodeType
                    {
                        kodeverdi = "RP",
                        kodebeskrivelse = "Reguleringsplan"
                    },
                    navn = "Naustgrendanabbevannet",
                    formaal = "Fritidsbegyggelse",
                    andreRelevanteKrav = "andre relevante krav her",
                    beregningsregelGradAvUtnytting = new KodeType
                    {
                        kodeverdi = "%BYA",
                        kodebeskrivelse = "Prosent bebygd areal"
                    }
                }
                }, 

                arealdisponering = new ArealdisponeringType
                {
                    tomtearealByggeomraade = 1000.5,
                    tomtearealByggeomraadeSpecified = true,
                    tomtearealSomTrekkesFra = 30.2,
                    tomtearealSomTrekkesFraSpecified = true,
                    tomtearealSomLeggesTil = 50.3,
                    tomtearealSomLeggesTilSpecified = true,
                    tomtearealBeregnet = 1020,
                    tomtearealBeregnetSpecified = true,
                    beregnetMaksByggeareal = 300.2,
                    beregnetMaksByggearealSpecified = true,
                    arealBebyggelseEksisterende = 200.4,
                    arealBebyggelseEksisterendeSpecified = true,
                    arealBebyggelseSomSkalRives = 10.6,
                    arealBebyggelseSomSkalRivesSpecified = true,
                    arealBebyggelseNytt = 30.5,
                    arealBebyggelseNyttSpecified = true,
                    parkeringsarealTerreng = 18.0,
                    parkeringsarealTerrengSpecified = true,
                    arealSumByggesak = 240.5,
                    arealSumByggesakSpecified = true,
                    beregnetGradAvUtnytting = 23.36,
                    beregnetGradAvUtnyttingSpecified = true
                },

                kravTilByggegrunn = new KravTilByggegrunnType
                {
                    flomutsattOmraade = false,
                    flomutsattOmraadeSpecified = true,
                    f1 = false,
                    f1Specified = true,
                    f2 = false,
                    f2Specified = true,
                    f3 = false,
                    f3Specified = true,
                    skredutsattOmraade = true,
                    skredutsattOmraadeSpecified = true,
                    s1 = true,
                    s1Specified = true,
                    s2 = false,
                    s2Specified = true,
                    s3 = false,
                    s3Specified = false,
                    miljoeforhold = false,
                    miljoeforholdSpecified = true
                },

                plassering = new PlasseringType
                {
                    konfliktHoeyspentkraftlinje = false,
                    konfliktHoeyspentkraftlinjeSpecified = true,
                    konfliktVannOgAvloep = false,
                    konfliktVannOgAvloepSpecified = true
                },

                generelleVilkaar = new GenerelleVilkaarType
                {
                    paalagtUavhengigKontroll = false,
                    paalagtUavhengigKontrollSpecified = true,
                    beroererArbeidsplasser = false,
                    beroererArbeidsplasserSpecified = true
                },
                adkomst = new VegType
                {
                    nyeEndretAdkomst = true,
                    nyeEndretAdkomstSpecified = true,
                    erTillatelseGittRiksFylkesveg = false,
                    erTillatelseGittRiksFylkesvegSpecified = true,
                    erTillatelseGittKommunalVeg = false,
                    erTillatelseGittKommunalVegSpecified = true,
                    erTillatelseGittPrivatVeg = true,
                    erTillatelseGittPrivatVegSpecified = true,
                    vegtype = new[]
                    {
                        new KodeType
                        {
                            kodeverdi = "PrivatVeg",
                            kodebeskrivelse = "PrivatVeg"
                        }
                    }
                }
        };

            endringAvTillatelse.varsling = new VarslingType
            {
                fritattFraNabovarsling = true,
                fritattFraNabovarslingSpecified = true,
                foreliggerMerknader = false,
                foreliggerMerknaderSpecified = true,
                antallMerknader = "0",
                vurderingAvMerknader = "",
                varslingsMetode = new KodeType()
                {
                    kodeverdi = "Fellestjenester bygg",
                    kodebeskrivelse = "Fellestjenester bygg"
                }
            };

            endringAvTillatelse.beskrivelseAvTiltak = new TiltakType
                {
                    bruk = new FormaalType
                    {
                        anleggstype = new KodeType
                        {
                            kodeverdi = "andre",
                            kodebeskrivelse = "Andre"
                        },
                        naeringsgruppe = new KodeType
                        {
                            kodeverdi = "Y",
                            kodebeskrivelse = "Næringsgruppe for annet som ikke er næring"
                        },
                        bygningstype = new KodeType
                        {
                            kodeverdi = "161",
                            kodebeskrivelse = "Hytter, sommerhus ol fritidsbygning"
                        },
                        tiltaksformaal = new[]
                        {
                            new KodeType
                            {
                                kodeverdi = "Fritidsbolig",
                                kodebeskrivelse = "Fritidsbolig"
                            },
                            new KodeType
                            {
                                kodeverdi = "Annet",
                                kodebeskrivelse = "Annet"
                            },
                        },
                        beskrivPlanlagtFormaal = "Ny fritidsbolig med laftet anneks"
                    },
                    type = new[]
                    {
                        new KodeType
                        {
                            kodeverdi = "nyttbyggboligformal",
                            kodebeskrivelse = "Nytt bygg - Boligformål"
                        }
                    },
                foelgebrev = "Følgebrev"
                };

           
            endringAvTillatelse.dispensasjon = new []
            {
                new DispensasjonType()
                {
                    begrunnelse = "Begrunnelse", 
                    beskrivelse = "Beskrivelse",
                    dispensasjonstype = new KodeType()
                    {
                        kodebeskrivelse = "Arealplaner",
                        kodeverdi = "PLAN"
                    }
                }
            };

            endringAvTillatelse.tiltakshaver = new PartType
            {
                partstype = new KodeType
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                foedselsnummer = "25125401530",
                navn = "Filip Mohamed",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Storgata 5",
                    postnr = "7003",
                    poststed = "Bø i Telemark",

                },
                telefonnummer = "11223344",
                mobilnummer = "87654321",
                epost = "navn@domene.no",
                signaturdato = new System.DateTime(2016, 07, 28),
                signaturdatoSpecified = true
            };

            endringAvTillatelse.ansvarligSoeker =
                new PartType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    organisasjonsnummer = "910297937", 
                    navn = "FANA OG HAFSLO REVISJON",
                    kontaktperson = "Benjamin Fjell",
                    adresse = new EnkelAdresseType
                    {
                        adresselinje1 = "Lillegata 5",
                        postnr = "7003",
                        poststed = "Bø i Telemark"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "tine@arkitektum.no",
                    signaturdato = new System.DateTime(2016, 07, 26),
                    signaturdatoSpecified = true
                };

            return endringAvTillatelse;
        }

    }
}