﻿

using AutoMapper;
using DIBK.FellestjenesterBygg.TestEngine.Models.ModelData;
using no.kxml.skjema.dibk.suppleringArbeidstilsynet;
using System.Collections.Generic;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class ArbeidstilsynetSuppleringGenerator
    {
        private IMapper _formMapper;
        public SuppleringArbeidstilsynetType NySupplering()
        {
            FormMapperConfiguration();
            var atilSupplering = new no.kxml.skjema.dibk.suppleringArbeidstilsynet.SuppleringArbeidstilsynetType();

            atilSupplering.eiendomByggested = _formMapper.Map<no.kxml.skjema.dibk.suppleringArbeidstilsynet.EiendomType[]>(new List<EiendomByggested>() { new EiendomByggested() });
            atilSupplering.tiltakshaver = _formMapper.Map<no.kxml.skjema.dibk.suppleringArbeidstilsynet.PartType>(new Tiltakshaver());
            atilSupplering.ansvarligSoeker = _formMapper.Map<no.kxml.skjema.dibk.suppleringArbeidstilsynet.PartType>(new AnsvarligSoeker());
            atilSupplering.kommunensSaksnummer = _formMapper.Map<no.kxml.skjema.dibk.suppleringArbeidstilsynet.SaksnummerType>(new Saksnummer() { Saksaar = "2021", Sakssekvensnummer = "123" });
            atilSupplering.arbeidstilsynetsSaksnummer = _formMapper.Map<no.kxml.skjema.dibk.suppleringArbeidstilsynet.SaksnummerType>(new Saksnummer() { Saksaar = "2021", Sakssekvensnummer = "999" });
            atilSupplering.metadata = _formMapper.Map<no.kxml.skjema.dibk.suppleringArbeidstilsynet.MetadataType>(new Metadata());
            atilSupplering.ettersending = _formMapper.Map<no.kxml.skjema.dibk.suppleringArbeidstilsynet.EttersendingType[]>(new List<Ettersending>() { new Ettersending() });
            atilSupplering.mangelbesvarelse = _formMapper.Map<no.kxml.skjema.dibk.suppleringArbeidstilsynet.MangelbesvarelseType[]>(new List<Mangelbesvarelse>() {new Mangelbesvarelse() });
            atilSupplering.signatur = _formMapper.Map<no.kxml.skjema.dibk.suppleringArbeidstilsynet.SignaturType>(new Signatur());

            return atilSupplering;
        }

        private void FormMapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;
                cfg.CreateMap<Tiltakshaver, no.kxml.skjema.dibk.suppleringArbeidstilsynet.PartType>();
                cfg.CreateMap<EiendomByggested, no.kxml.skjema.dibk.suppleringArbeidstilsynet.EiendomType>();
                cfg.CreateMap<Ettersending, no.kxml.skjema.dibk.suppleringArbeidstilsynet.EttersendingType>();
                cfg.CreateMap<Mangelbesvarelse, no.kxml.skjema.dibk.suppleringArbeidstilsynet.MangelbesvarelseType>();
                cfg.CreateMap<AnsvarligSoeker, no.kxml.skjema.dibk.suppleringArbeidstilsynet.PartType>();
                cfg.CreateMap<Metadata, no.kxml.skjema.dibk.suppleringArbeidstilsynet.MetadataType>();
                cfg.CreateMap<Saksnummer, no.kxml.skjema.dibk.suppleringArbeidstilsynet.SaksnummerType>();
                cfg.CreateMap<Mangel, no.kxml.skjema.dibk.suppleringArbeidstilsynet.MangeltekstType>();

                cfg.CreateMap<EnkelAdresse, no.kxml.skjema.dibk.suppleringArbeidstilsynet.EnkelAdresseType>();
                cfg.CreateMap<EiendomsAdresse, no.kxml.skjema.dibk.suppleringArbeidstilsynet.EiendommensAdresseType>();
                cfg.CreateMap<Kode, no.kxml.skjema.dibk.suppleringArbeidstilsynet.KodeType>();
                cfg.CreateMap<Kontaktperson, no.kxml.skjema.dibk.suppleringArbeidstilsynet.KontaktpersonType>();
                cfg.CreateMap<Matrikkel, no.kxml.skjema.dibk.suppleringArbeidstilsynet.MatrikkelnummerType>();
                cfg.CreateMap<Signatur, no.kxml.skjema.dibk.suppleringArbeidstilsynet.SignaturType>();
                cfg.CreateMap<SuppleringVedlegg, no.kxml.skjema.dibk.suppleringArbeidstilsynet.VedleggType>();

            });

            _formMapper = config.CreateMapper();
        }

        private static List<Sjekklistekrav> LagSjekklistekrav()
        {
            return new List<Sjekklistekrav>() {
                    new Sjekklistekrav(true, new Kode("1.15", "Har representatene til de ansatte som skal arbeide i det omsøkte tiltaket (AMU, verneombud eller ansattes representant), medvirket i planleggingsprosessen?"), "Her er dokumentasjon for pkt 1.15..."),
                    new Sjekklistekrav(true, new Kode("1.18", "Har arbeidsgiver(e) som skal drive virksomhet i det omsøkte tiltaket, medvirket i planleggingsprosessen?"), "Her er dokumentasjon for pkt 1.18..."),
                    
                    new Sjekklistekrav(false, new Kode("2.1", "Er arbeidstakerne sikret mot fall i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("2.2", "Er fallfare en risiko i det søknadspliktige tiltaket?")),
                    new Sjekklistekrav(false, new Kode("2.3", "Er tiltaket tilrettelagt for sikkerhet ved renhold, vedlikehold, montering og liknende i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("2.4", "Innebærer arbeid med montering, justering, kontroll, renhold, vedlikehold og ettersyn av bygningen eller tekniske installasjoner en risiko ved det søknadspliktige tiltaket?")),
                    new Sjekklistekrav(false, new Kode("2.5", "Er arbeidslokalene plassert, innredet og utformet for å redusere fare for vold og trusler om vold i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("2.6", "Er vold eller trusler om vold et tema i det søknadspliktige tiltaket og en risiko ved driften til virksomheten(e) som skal bruke tiltaket?")),
                    new Sjekklistekrav(false, new Kode("2.7", "Er det planlagt med rømningsveier og nødutganger i tråd kravene?")),
                    new Sjekklistekrav(false, new Kode("2.8", "Er rømningsveier og nødutganger et tema i det søknadspliktige tiltaket?")),
                    new Sjekklistekrav(false, new Kode("2.9", "Er det planlagt med løfteinnretninger i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("2.10", "Er løfteinnretninger et tema i det søknadspliktige tiltaket?")),
                    
                    new Sjekklistekrav(false, new Kode("3.1", "Er det planlagt med handikaptoalett i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("3.2", "Er handikaptoalett et tema i det søknadspliktige tiltaket?")),
                    new Sjekklistekrav(false, new Kode("3.3", "Er arbeidslokalene planlagt med trinnfri atkomst og heis eller løfteplattform i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("3.4", "Er trinnfri adkomst og heis et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(false, new Kode("4.1", "Er det planlagt med løsninger for klima, ventilasjon og luftkvalitet mv. i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("4.2", "Er klima, ventilasjon og luftkvalitet et tema i det søknadspliktige tiltaket?")),
                    new Sjekklistekrav(false, new Kode("4.3", "Er forurensning i arbeidsatmosfæren ivaretatt i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("4.4", "Er forurensing i arbeidsatmosfæren et tema i det søknadspliktige tiltaket og en risiko ved driften til virksomheten(e) som skal bruke tiltaket?")),

                    new Sjekklistekrav(false, new Kode("5.1", "Er risiko for helsefarlig stråling ivaretatt i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("5.2", "Er stråling et tema i det søknadspliktige tiltaket og en risiko ved driften til virksomheten(e) som skal bruke tiltaket?")),
                    new Sjekklistekrav(false, new Kode("5.3", "Er risiko for biologisk helsefare ivaretatt i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("5.4", "Er biologisk helsefare et tema i det søknadspliktige tiltaket og en risiko ved driften til virksomheten(e) som skal bruke tiltaket?")),
                    new Sjekklistekrav(true, new Kode("5.5", "Er risiko for kjemisk helsefare ivaretatt i tråd med kravene?"), "Her er dokumentasjon for pkt 5.5"),
                    new Sjekklistekrav(false, new Kode("5.7", "Er det planlagt med løsninger for rent og urent område i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("5.8", "Er rent og urent område et tema i det søknadspliktige tiltaket og et krav som følger av driften til virksomheten(e) som skal bruke tiltaket?")),

                    new Sjekklistekrav(false, new Kode("6.1", "Er risiko for støy redusert i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("6.2", "Er støy et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(true, new Kode("7.1", "Er arealet som er avsatt per arbeidsplass, i tråd med kravene?"), "Dette er dokumentasjon på pkt 7.1..."),
                    new Sjekklistekrav(false, new Kode("7.3", "Er romhøyden i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("7.4", "Er romhøyde et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(false, new Kode("8.1", "Er ferdsel og atkomst planlagt i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("8.2", "Er ferdsel og atkomst et tema i det søknadspliktige tiltaket?")),
                    new Sjekklistekrav(false, new Kode("8.3", "Er varelevering planlagt i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("8.4", "Er varelevering et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(false, new Kode("9.1", "Er utendørs arbeids- og lagringsplasser planlagt i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("9.2", "Er utendørs arbeids- og lagringsplasser et tema i det søknadspliktige tiltaket?")),
                    
                    new Sjekklistekrav(false, new Kode("10.1", "Er de enkelte arbeidsplassene og personalrommene planlagt med tilgang til dagslys og utsyn i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("10.2", "Er dagslys og utsyn et tema i det søknadspliktige tiltaket?")),
                    new Sjekklistekrav(false, new Kode("10.3", "Er alle arbeidsplasser og ferdselsveier planlagt med belysning og synsforhold i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("10.4", "Er belysning og lysforhold et tema i det søknadspliktige tiltaket?")),
    
                    new Sjekklistekrav(false, new Kode("11.1", "Er det planlagt med garderober som er plassert, innredet og utformet i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("11.2", "Er kravene til garderober ivaretatt i eksisterende bygningsmasse?")),
                    new Sjekklistekrav(false, new Kode("11.3", "Er garderober et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(false, new Kode("11.4", "Er det planlagt med dusjrom som er plassert, innredet og utformet i tråd med kravene?")),
                    new Sjekklistekrav(true, new Kode("11.5", "Er dusjrom et tema i det søknadspliktige tiltaket og et krav som følger av driften til virksomheten(e) som skal bruke tiltaket?")),
                    new Sjekklistekrav(true, new Kode("11.6", "Er dusjrom ivaretatt i eksisterende bygningsmasse?"), "Dette er dokumentasjon av pkt 11.6"),
                    
                    new Sjekklistekrav(false, new Kode("11.7", "Er det planlagt med toaletter plassert, innredet og utformet i tråd med kravene?")),
                    new Sjekklistekrav(true, new Kode("11.8", "Er kravene til toaletter ivaretatt i eksisterende bygningsmasse?"), "Dette er dokumentasjon for pkt 11.8"),

                    new Sjekklistekrav(false, new Kode("11.10", "Er det planlagt med rom for renholdsutstyr plassert, innredet og utformet i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("11.11", "Er renholdsrom et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(false, new Kode("11.12", "Er det planlagt med spiserom plassert, innredet og utformet i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("11.13", "Er kravene til spiserom ivaretatt i eksisterende bygningsmasse?")),
                    new Sjekklistekrav(false, new Kode("11.14", "Er spiserom et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(false, new Kode("11.15", "Er spiserom tilgjengelig hele arbeidsdagen og plassert mindre enn 100 meter fra arbeidsstedet?")),
                    new Sjekklistekrav(false, new Kode("11.16", "Er det planlagt med pauserom innredet og utformet i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("11.17", "Er kravene til pauserom ivaretatt i eksisterende bygningsmasse?")),
                    new Sjekklistekrav(false, new Kode("11.18", "Er pauserom et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(true, new Kode("11.19", "Må arbeidstakere kunne sove på, eller i tilknytning til arbeidsstedet på grunn av arbeidets art?")),
                    new Sjekklistekrav(false, new Kode("11.20", "Er det planlagt med soverom som er plassert, innredet og utformet i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("11.21", "Er soverom ivaretatt i eksisterende bygningsmasse?")),
                    new Sjekklistekrav(false, new Kode("11.22", "Er soverom et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(true, new Kode("11.23", "Har virksomheten(e) utendørs eller annet arbeid som medfører behov for tørking av klær eller fottøy?")),
                    new Sjekklistekrav(false, new Kode("11.24", "Er det planlagt med tørkerom eller liknende som er plassert, innredet og utformet i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("11.25", "Er tørkerom eller likende ivaretatt i eksisterende bygningsmasse?")),
                    new Sjekklistekrav(false, new Kode("11.26", "Er tørkerom eller likende et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(false, new Kode("12.1", "Er risiko for ergonomisk helsefare redusert i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("12.2", "Er ergonomisk helsefare et tema i det søknadspliktige tiltaket og en risiko ved driften til virksomheten(e) som skal bruke tiltaket?")),

                    new Sjekklistekrav(true, new Kode("13.1", "Er det søknadspliktige tiltaket en helseinstitusjon?")),
                    new Sjekklistekrav(false, new Kode("13.2", "Er det planlagt med pasientrom som har fri plass rundt seng mv. i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("13.3", "Er pasientrom et tema i det søknadspliktige tiltaket?")),
                    new Sjekklistekrav(false, new Kode("13.4", "Er det planlagt med baderom i tilknytning til beboer- eller pasientrom i helseinstitusjoner i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("13.5", "Er baderom et tema i det søknadspliktige tiltaket?")),
                    new Sjekklistekrav(false, new Kode("13.6", "Er det planlagt med tilgjengelighet i tråd med kravene?")),
                    new Sjekklistekrav(false, new Kode("13.7", "Er tilgjengelighet et tema i det søknadspliktige tiltaket?")),

                    new Sjekklistekrav(true, new Kode("14.1", "Har arbeidsgiver(e) som skal bruke tiltaket, plikt til å være tilknyttet godkjent bedriftshelsetjeneste?"), "Dette er dokumentasjon for pkt 14.1..."),

                    new Sjekklistekrav(true, new Kode("15.1", "Er bygning(er) vernet?"), "Dette er dokumentasjon for pkt 15.1..."),
                };
        }
    }
}
