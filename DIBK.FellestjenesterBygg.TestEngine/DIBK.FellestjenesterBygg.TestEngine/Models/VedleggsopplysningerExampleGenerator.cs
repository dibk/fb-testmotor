﻿using no.kxml.skjema.dibk.vedlegg;
using System;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class VedleggsopplysningerExampleGenerator
    {
        

        public VedleggsopplysningerType NyttVedleggsopplysninger()
        {
            VedleggsopplysningerType vedlegg = new VedleggsopplysningerType();
            vedlegg.vedlegg = new VedleggType[]
            {
              new VedleggType() {
                  vedleggsid = "1234",
                  vedleggstype = "Situasjonsplan",
                  vedleggsUrl = "filnavn.pdf",
                  vedleggskategori = "D",
                  beskrivelse = "Eksempeldata med beskrivelse av vedlegget",
                  vedleggFulgtNabovarsel = true,
                  vedleggFulgtNabovarselSpecified = true
              },
              new VedleggType() {
                  vedleggsid = "12345",
                  vedleggstype = "TegningNyFasade",
                  vedleggsUrl = "tegning.pdf",
                  vedleggskategori = "E",
                  beskrivelse = "Eksempeldata med beskrivelse av vedlegget",
                  tegningsnr = "32",
                  tegningsdato = new DateTime(2018,3,10),
                  tegningsdatoSpecified = true,
                  vedleggFulgtNabovarsel = true,
                  vedleggFulgtNabovarselSpecified = true
              },
              new VedleggType() {
                  vedleggsid = "12346",
                  vedleggstype = "UttalelseVedtakAnnenOffentligMyndighet",
                  vedleggsUrl = "detteErDenLengsteVedleggstypenIAntallBokstaver.pdf",
                  vedleggskategori = "E",
                  beskrivelse = "Denne beskrivelsen er også litt lenger en de andre for å sjekke hvordan linjeskift ser ut i PDFen. ",
                  tegningsnr = "32",
                  tegningsdato = new DateTime(2018,3,25),
                  tegningsdatoSpecified = true
              }
            };
            return vedlegg;
        }
    }
}