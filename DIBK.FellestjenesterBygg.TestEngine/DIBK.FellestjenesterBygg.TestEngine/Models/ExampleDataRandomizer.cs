﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class ExampleDataRandomizer
    {

        private static List<MunicipalityDataModel> _municipalityDataList = new MunicipalityDataFill().MunicipalityDataList();

        private static List<MunicipalityDataModel> _municipalityTestDataList = new MunicipalityDataFill().MunicipalityListForTestSystems();
        private static Random _rnd = new Random();

        public string GetRandomKommuneNummer()
        {
            int randomIndex = _rnd.Next(_municipalityDataList.Count);
            return _municipalityDataList[randomIndex].KommuneNummer;
        }

        public string GetRandomGaardsOrBruksNummer()
        {
            int randomNumber = _rnd.Next(999) + 1;
            return (randomNumber.ToString());            
        }


        public string GetRandomTestKommuneNummer()
        {
            int randomIndex = _rnd.Next(_municipalityTestDataList.Count);
            return _municipalityTestDataList[randomIndex].KommuneNummer;
        }


        public DateTime GetRandomDaysInThePast()
        {
            int randomDays = 0 - _rnd.Next(33);
            DateTime now = DateTime.Now;
            return now.AddDays(randomDays);
        }

        public string GetTiltaksklasse()
        {
            int tt = _rnd.Next(2) + 1;
            return tt.ToString();
        }

    }



    public class MunicipalityDataModel
    {

        public string FylkesNummer { get; set; }
        public string Fylke { get; set; }
        public string KommuneNummer { get; set; }
        public string Kommune { get; set; }


        public MunicipalityDataModel(string fylkesNummer, string fylke, string kommuneNummer, string kommune)
        {
            FylkesNummer = fylkesNummer;
            Fylke = fylke;
            KommuneNummer = kommuneNummer;
            Kommune = kommune;
        }

        public MunicipalityDataModel()
        {

        }

    }


    public class MunicipalityDataFill
    {


        public List<MunicipalityDataModel> MunicipalityListForTestSystems()
        {
           List<MunicipalityDataModel> municipalityListForTestSystems = new List<MunicipalityDataModel>();
            municipalityListForTestSystems.Add(new MunicipalityDataModel("01", "FTB", "9996", "LIME KOMMUNE (Acos)"));
            municipalityListForTestSystems.Add(new MunicipalityDataModel("01", "FTB", "9997", "SNEKKRE KOMMUNE (Evry)"));
            municipalityListForTestSystems.Add(new MunicipalityDataModel("01", "FTB", "9998", "MURE KOMMUNE (Tieto)"));
            municipalityListForTestSystems.Add(new MunicipalityDataModel("01", "FTB", "9999", "Dibk testkommune"));

            return municipalityListForTestSystems;
        }


        public List <MunicipalityDataModel> MunicipalityDataList ()
        {
            List < MunicipalityDataModel > municipalityDataList = new List<MunicipalityDataModel>();

            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0101", "Halden"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0104", "Moss"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0105", "Sarpsborg"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0106", "Fredrikstad"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0111", "Hvaler"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0118", "Aremark"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0119", "Marker"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0121", "Rømskog"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0122", "Trøgstad"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0123", "Spydeberg"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0124", "Askim"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0125", "Eidsberg"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0127", "Skiptvet"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0128", "Rakkestad"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0135", "Råde"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0136", "Rygge"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0137", "Våler"));
            municipalityDataList.Add(new MunicipalityDataModel("01", "Østfold", "0138", "Hobøl"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0211", "Vestby"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0213", "Ski"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0214", "Ås"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0215", "Frogn"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0216", "Nesodden"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0217", "Oppegård"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0219", "Bærum"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0220", "Asker"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0221", "Aurskog-Høland"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0226", "Sørum"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0227", "Fet"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0228", "Rælingen"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0229", "Enebakk"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0230", "Lørenskog"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0231", "Skedsmo"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0233", "Nittedal"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0234", "Gjerdrum"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0235", "Ullensaker"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0236", "Nes"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0237", "Eidsvoll"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0238", "Nannestad"));
            municipalityDataList.Add(new MunicipalityDataModel("02", "Akershus", "0239", "Hurdal"));
            municipalityDataList.Add(new MunicipalityDataModel("03", "Oslo", "0301", "Oslo"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0402", "Kongsvinger"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0403", "Hamar"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0412", "Ringsaker"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0415", "Løten"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0417", "Stange"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0418", "Nord-Odal"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0419", "Sør-Odal"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0420", "Eidskog"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0423", "Grue"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0425", "Åsnes"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0426", "Våler"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0427", "Elverum"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0428", "Trysil"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0429", "Åmot"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0430", "Stor-Elvdal"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0432", "Rendalen"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0434", "Engerdal"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0436", "Tolga"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0437", "Tynset"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0438", "Alvdal"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0439", "Folldal"));
            municipalityDataList.Add(new MunicipalityDataModel("04", "Hedmark", "0441", "Os"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0501", "Lillehammer"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0502", "Gjøvik"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0511", "Dovre"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0512", "Lesja"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0513", "Skjåk"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0514", "Lom"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0515", "Vågå"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0516", "Nord-Fron"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0517", "Sel"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0519", "Sør-Fron"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0520", "Ringebu"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0521", "Øyer"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0522", "Gausdal"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0528", "Østre Toten"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0529", "Vestre Toten"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0532", "Jevnaker"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0533", "Lunner"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0534", "Gran"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0536", "Søndre Land"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0538", "Nordre Land"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0540", "Sør-Aurdal"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0541", "Etnedal"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0542", "Nord-Aurdal"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0543", "Vestre Slidre"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0544", "Øystre Slidre"));
            municipalityDataList.Add(new MunicipalityDataModel("05", "Oppland", "0545", "Vang"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0602", "Drammen"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0604", "Kongsberg"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0605", "Ringerike"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0612", "Hole"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0615", "Flå"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0616", "Nes"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0617", "Gol"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0618", "Hemsedal"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0619", "Ål"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0620", "Hol"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0621", "Sigdal"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0622", "Krødsherad"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0623", "Modum"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0624", "Øvre Eiker"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0625", "Nedre Eiker"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0626", "Lier"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0627", "Røyken"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0628", "Hurum"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0631", "Flesberg"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0632", "Rollag"));
            municipalityDataList.Add(new MunicipalityDataModel("06", "Buskerud", "0633", "Nore og Uvdal"));
            municipalityDataList.Add(new MunicipalityDataModel("07", "Vestfold", "0701", "Horten"));
            municipalityDataList.Add(new MunicipalityDataModel("07", "Vestfold", "0715", "Holmestrand"));
            municipalityDataList.Add(new MunicipalityDataModel("07", "Vestfold", "0704", "Tønsberg"));
            municipalityDataList.Add(new MunicipalityDataModel("07", "Vestfold", "0710", "Sandefjord"));
            municipalityDataList.Add(new MunicipalityDataModel("07", "Vestfold", "0712", "Larvik"));
            municipalityDataList.Add(new MunicipalityDataModel("07", "Vestfold", "0711", "Svelvik"));
            municipalityDataList.Add(new MunicipalityDataModel("07", "Vestfold", "0713", "Sande"));
            municipalityDataList.Add(new MunicipalityDataModel("07", "Vestfold", "0716", "Re"));
            municipalityDataList.Add(new MunicipalityDataModel("07", "Vestfold", "0729", "Færder"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0805", "Porsgrunn"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0806", "Skien"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0807", "Notodden"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0811", "Siljan"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0814", "Bamble"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0815", "Kragerø"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0817", "Drangedal"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0819", "Nome"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0821", "Bø"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0822", "Sauherad"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0826", "Tinn"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0827", "Hjartdal"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0828", "Seljord"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0829", "Kviteseid"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0830", "Nissedal"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0831", "Fyresdal"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0833", "Tokke"));
            municipalityDataList.Add(new MunicipalityDataModel("08", "Telemark", "0834", "Vinje"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0901", "Risør"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0904", "Grimstad"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0906", "Arendal"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0911", "Gjerstad"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0912", "Vegårshei"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0914", "Tvedestrand"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0919", "Froland"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0926", "Lillesand"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0928", "Birkenes"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0929", "Åmli"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0935", "Iveland"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0937", "Evje og Hornnes"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0938", "Bygland"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0940", "Valle"));
            municipalityDataList.Add(new MunicipalityDataModel("09", "Aust-Agder", "0941", "Bykle"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1001", "Kristiansand"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1002", "Mandal"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1003", "Farsund"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1004", "Flekkefjord"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1014", "Vennesla"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1017", "Songdalen"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1018", "Søgne"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1021", "Marnardal"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1026", "Åseral"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1027", "Audnedal"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1029", "Lindesnes"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1032", "Lyngdal"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1034", "Hægebostad"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1037", "Kvinesdal"));
            municipalityDataList.Add(new MunicipalityDataModel("10", "Vest-Agder", "1046", "Sirdal"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1101", "Eigersund"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1102", "Sandnes"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1103", "Stavanger"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1106", "Haugesund"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1111", "Sokndal"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1112", "Lund"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1114", "Bjerkreim"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1119", "Hå"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1120", "Klepp"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1121", "Time"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1122", "Gjesdal"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1124", "Sola"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1127", "Randaberg"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1129", "Forsand"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1130", "Strand"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1133", "Hjelmeland"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1134", "Suldal"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1135", "Sauda"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1141", "Finnøy"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1142", "Rennesøy"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1144", "Kvitsøy"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1145", "Bokn"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1146", "Tysvær"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1149", "Karmøy"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1151", "Utsira"));
            municipalityDataList.Add(new MunicipalityDataModel("11", "Rogaland", "1160", "Vindafjord"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1201", "Bergen"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1211", "Etne"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1216", "Sveio"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1219", "Bømlo"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1221", "Stord"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1222", "Fitjar"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1223", "Tysnes"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1224", "Kvinnherad"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1227", "Jondal"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1228", "Odda"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1231", "Ullensvang"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1232", "Eidfjord"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1233", "Ulvik"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1234", "Granvin"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1235", "Voss"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1238", "Kvam"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1241", "Fusa"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1242", "Samnanger"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1243", "Os"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1244", "Austevoll"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1245", "Sund"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1246", "Fjell"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1247", "Askøy"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1251", "Vaksdal"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1252", "Modalen"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1253", "Osterøy"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1256", "Meland"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1259", "Øygarden"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1260", "Radøy"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1263", "Lindås"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1264", "Austrheim"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1265", "Fedje"));
            municipalityDataList.Add(new MunicipalityDataModel("12", "Hordaland", "1266", "Masfjorden"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1401", "Flora"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1411", "Gulen"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1412", "Solund"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1413", "Hyllestad"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1416", "Høyanger"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1417", "Vik"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1418", "Balestrand"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1419", "Leikanger"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1420", "Sogndal"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1421", "Aurland"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1422", "Lærdal"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1424", "Årdal"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1426", "Luster"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1428", "Askvoll"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1429", "Fjaler"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1430", "Gaular"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1431", "Jølster"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1432", "Førde"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1433", "Naustdal"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1438", "Bremanger"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1439", "Vågsøy"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1441", "Selje"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1443", "Eid"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1444", "Hornindal"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1445", "Gloppen"));
            municipalityDataList.Add(new MunicipalityDataModel("14", "Sogn og fjordane", "1449", "Stryn"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1502", "Molde"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1504", "Ålesund"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1505", "Kristiansund"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1511", "Vanylven"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1514", "Sande"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1515", "Herøy"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1516", "Ulstein"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1517", "Hareid"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1519", "Volda"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1520", "Ørsta"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1523", "Ørskog"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1524", "Norddal"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1525", "Stranda"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1526", "Stordal"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1528", "Sykkylven"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1529", "Skodje"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1531", "Sula"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1532", "Giske"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1534", "Haram"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1535", "Vestnes"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1539", "Rauma"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1543", "Nesset"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1545", "Midsund"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1546", "Sandøy"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1547", "Aukra"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1548", "Fræna"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1551", "Eide"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1554", "Averøy"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1557", "Gjemnes"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1560", "Tingvoll"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1563", "Sunndal"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1566", "Surnadal"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1567", "Rindal"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1571", "Halsa"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1573", "Smøla"));
            municipalityDataList.Add(new MunicipalityDataModel("15", "Møre og Romsdal", "1576", "Aure"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5001", "Trondheim"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5004", "Steinkjer"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5005", "Namsos"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5011", "Hemne"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5012", "Snillfjord"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5013", "Hitra"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5014", "Frøya"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5015", "Ørland"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5016", "Agdenes"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5017", "Bjugn"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5018", "Åfjord"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5019", "Roan"));


          
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5020", "Osen"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5021", "Oppdal"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5022", "Rennebu"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5023", "Meldal"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5024", "Orkdal"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5025", "Røros"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5026", "Holtålen"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5027", "Midtre Gauldal"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5028", "Melhus"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5029", "Skaun"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5030", "Klæbu"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5031", "Malvik"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5032", "Selbu"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5033", "Tydal"));

            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5034", "Meråker"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5035", "Stjørdal"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5036", "Frosta"));

            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5037", "Levanger"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5038", "Verdal"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5039", "Verran"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5040", "Namdalseid"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5041", "Snåsa"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5042", "Lierne"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5043", "Røyrvik"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5044", "Namsskogan"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5045", "Grong"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5046", "Høylandet"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5047", "Overhalla"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5048", "Fosnes"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5049", "Flatanger"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5050", "Vikna"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5051", "Nærøy"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5052", "Leka"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5053", "Inderøy"));
            municipalityDataList.Add(new MunicipalityDataModel("50", "Trøndelag", "5054", "Indre Fosen"));

            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1804", "Bodø"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1805", "Narvik"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1811", "Bindal"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1812", "Sømna"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1813", "Brønnøy"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1815", "Vega"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1816", "Vevelstad"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1818", "Herøy"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1820", "Alstahaug"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1822", "Leirfjord"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1824", "Vefsn"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1825", "Grane"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1826", "Hattfjelldal"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1827", "Dønna"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1828", "Nesna"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1832", "Hemnes"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1833", "Rana"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1834", "Lurøy"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1835", "Træna"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1836", "Rødøy"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1837", "Meløy"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1838", "Gildeskål"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1839", "Beiarn"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1840", "Saltdal"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1841", "Fauske"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1845", "Sørfold"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1848", "Steigen"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1849", "Hamarøy – Hábmer"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1850", "Divtasvuodna – Tysfjord"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1851", "Lødingen"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1852", "Tjeldsund"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1853", "Evenes"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1854", "Ballangen"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1856", "Røst"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1857", "Værøy"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1859", "Flakstad"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1860", "Vestvågøy"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1865", "Vågan"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1866", "Hadsel"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1867", "Bø"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1868", "Øksnes"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1870", "Sortland"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1871", "Andøy"));
            municipalityDataList.Add(new MunicipalityDataModel("18", "Nordland", "1874", "Moskenes"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1903", "Harstad"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1902", "Tromsø"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1911", "Kvæfjord"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1913", "Skånland"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1917", "Ibestad"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1919", "Gratangen"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1920", "Lavangen"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1922", "Bardu"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1923", "Salangen"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1924", "Målselv"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1925", "Sørreisa"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1926", "Dyrøy"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1927", "Tranøy"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1928", "Torsken"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1929", "Berg"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1931", "Lenvik"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1933", "Balsfjord"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1936", "Karlsøy"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1938", "Lyngen"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1939", "Storfjord"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1940", "Kåfjord"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1941", "Skjervøy"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1942", "Nordreisa"));
            municipalityDataList.Add(new MunicipalityDataModel("19", "Troms", "1943", "Kvænangen"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2002", "Vardø"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2003", "Vadsø"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2004", "Hammerfest"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2011", "Kautokeino"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2012", "Alta"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2014", "Loppa"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2015", "Hasvik"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2017", "Kvalsund"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2018", "Måsøy"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2019", "Nordkapp"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2020", "Porsanger"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2021", "Karasjok"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2022", "Lebesby"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2023", "Gamvik"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2024", "Berlevåg"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2025", "Tana"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2027", "Nesseby"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2028", "Båtsfjord"));
            municipalityDataList.Add(new MunicipalityDataModel("20", "Finnmark", "2030", "Sør-Varanger"));

            return municipalityDataList;

        }
    }


}