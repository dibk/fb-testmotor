﻿

using AutoMapper;
using DIBK.FellestjenesterBygg.TestEngine.Models.ModelData;
using no.kxml.skjema.dibk.arbeidstilsynetsSamtykke;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class ArbeidstilsynetExampleGenerator
    {
        private IMapper _formMapper;

        public ArbeidstilsynetsSamtykkeType NyttSamtykke()
        {
            FormMapperConfiguration();
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var samtykke = new ArbeidstilsynetsSamtykkeType();

            samtykke.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        kommunenummer = "3817",
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Bøgata 1",
                        postnr = "3800",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    bygningsnummer = "80466985",
                    bolignummer = "H0102",
                    kommunenavn = "Midt Telemark"
                }
            };

            samtykke.arbeidsplasser = new ArbeidsplasserType()
            {
                framtidige = false,
                framtidigeSpecified = true,
                faste = false,
                fasteSpecified = true,
                midlertidige = false,
                midlertidigeSpecified = true,
                antallAnsatte = "30",
                eksisterende = true,
                eksisterendeSpecified = true,
                utleieBygg = true,
                utleieByggSpecified = true,
                antallVirksomheter = "2",
                beskrivelse = "Beskrivelse....."

            };

            //Replaced data mapping for tiltakshaver and fakturamottaker
            samtykke.tiltakshaver = _formMapper.Map<no.kxml.skjema.dibk.arbeidstilsynetsSamtykke.PartType>(new Tiltakshaver());
            samtykke.fakturamottaker = _formMapper.Map<no.kxml.skjema.dibk.arbeidstilsynetsSamtykke.FakturamottakerType>(new Fakturamottaker());


            //samtykke.tiltakshaver = new PartType()
            //{
            //    organisasjonsnummer = "910748548",
            //    navn = "BLOMSTERDALEN OG ØVRE SNERTINGDAL",

            //    adresse = new EnkelAdresseType()
            //    {
            //        adresselinje1 = "Bøgata 16",
            //        postnr = "3802",
            //        poststed = "Bø i Telemark",
            //        landkode = "NO"
            //    },
            //    telefonnummer = "11223344",
            //    epost = "tine@arkitektum.no",
            //    kontaktperson = new KontaktpersonType()
            //    {
            //        navn = "Tine Høllre",
            //        telefonnummer = "98839131",
            //        epost = "tine@arkitektum.no"
            //    }
            //};

            //samtykke.fakturamottaker = new FakturamottakerType()
            //{
            //    organisasjonsnummer = "910297937",
            //    navn = "FANA OG HAFSLO REVISJON",

            //    adresse = new EnkelAdresseType()
            //    {
            //        adresselinje1 = "Bøgata 16",
            //        postnr = "3802",
            //        poststed = "Bø i Telemark",
            //        landkode = "NO"
            //    },
            //    prosjektnummer = "12345",
            //    fakturaPapir = false,
            //    fakturaPapirSpecified = true,
            //    ehfFaktura = true,
            //    ehfFakturaSpecified = true
            //};

            samtykke.hovedinnsendingsnummer = "";
            samtykke.sluttbrukersystemUrl = "";
            samtykke.fraSluttbrukersystem = "";
            samtykke.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2020",
                sakssekvensnummer = "123"
            };
            samtykke.prosjektnavn = "Nytt prosjekt";
            samtykke.arbeidstilsynetsSaksnummer = new SaksnummerType
            {
                saksaar = "2020",
                sakssekvensnummer = "321"
            };

            return samtykke;
        }
        private void FormMapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;
                cfg.CreateMap<Tiltakshaver, no.kxml.skjema.dibk.arbeidstilsynetsSamtykke.PartType>();
                cfg.CreateMap<EnkelAdresse, no.kxml.skjema.dibk.arbeidstilsynetsSamtykke.EnkelAdresseType>();
                cfg.CreateMap<Kontaktperson, no.kxml.skjema.dibk.arbeidstilsynetsSamtykke.KontaktpersonType>();
                cfg.CreateMap<Fakturamottaker, no.kxml.skjema.dibk.arbeidstilsynetsSamtykke.FakturamottakerType>();
                cfg.CreateMap<Kode, no.kxml.skjema.dibk.arbeidstilsynetsSamtykke.KodeType>();

            });

            _formMapper = config.CreateMapper();
        }
    }
}
