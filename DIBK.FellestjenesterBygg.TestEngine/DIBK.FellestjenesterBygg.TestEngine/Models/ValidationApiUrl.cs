﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class ValidationApiUrl
    {
        public string Url { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ValidationApiType Type { get; set; }
    }

    public enum ValidationApiType
    {
        New,
        Original
    }
}