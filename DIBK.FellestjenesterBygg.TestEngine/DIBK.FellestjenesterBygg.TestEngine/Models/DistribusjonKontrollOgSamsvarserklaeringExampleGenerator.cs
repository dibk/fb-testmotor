using System;
using no.kxml.skjema.dibk.distribusjonKontrollOgSamsvarserklaering;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class DistribusjonKontrollOgSamsvarserklaeringExampleGenerator
    {
        public DistribusjonKontrollOgSamsvarserklaeringType NyDistribusjonKontrollOgSamsvarserklaering()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            var distribusjonKontrollOgSamsvarserklaering = new DistribusjonKontrollOgSamsvarserklaeringType();

            distribusjonKontrollOgSamsvarserklaering.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            distribusjonKontrollOgSamsvarserklaering.hovedinnsendingsnummer = Guid.NewGuid().ToString();
            distribusjonKontrollOgSamsvarserklaering.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2017",
                sakssekvensnummer = "123"
            };

            distribusjonKontrollOgSamsvarserklaering.versjon = "v92";
            distribusjonKontrollOgSamsvarserklaering.prosjektnavn = "Bakkebygrenda";


            // ======= eiendomByggested ======= 
            distribusjonKontrollOgSamsvarserklaering.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "5"
                    },
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 1",
                        postnr = "3800",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    bygningsnummer = "80466985",
                    bolignummer = "H0101",
                    kommunenavn = "Midt Telemark"
                }
            };

            distribusjonKontrollOgSamsvarserklaering.ansvarsomraade = new[]
            {
                new AnsvarsomraadeType(){
                    funksjon = new KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    beskrivelseAvAnsvarsomraade = "Arkitekturprosjektering",
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = new KontaktpersonType()
                        {
                            epost = "tor@arkitektum.no",
                            mobilnummer = "90900999",
                            navn = "Tage Binders",
                            telefonnummer = "36584123"
                        },
                        organisasjonsnummer = "910297937",
                        navn = "FANA OG HAFSLO REVISJON",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 1",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345678",
                        mobilnummer = "87654321",
                        epost = "tor@arkitektum.no",
                        signaturdato = new DateTime(2016, 06, 23),
                        signaturdatoSpecified = true,
                        harSentralGodkjenning = true,
                    },


                    fristDato = DateTime.Now.AddDays(10),
                    fristDatoSpecified = true,
                    soeknadssystemetsReferanse = Guid.NewGuid().ToString(),
                    varsling = false,
                    varslingSpecified = true,
                    

                    utfoerende = new UtfoerendeType()
                    {
                        okMidlertidigBrukstillatelse = false,
                        okMidlertidigBrukstillatelseSpecified = true,
                        midlertidigBrukstillatelseGjenstaaende = new DelsoeknadMidlertidigBrukstillatelseType()
                        {
                            gjenstaaendeInnenfor = "Gjenstående arbeider av mindre vesentlig betydning, innenfor den delen av tiltaket det søkes midlertidig brukstillatelse for",
                            gjenstaaendeUtenfor = "Angi resterende deler av tiltaket hvor det her ikke søkes om midlertidig brukstillatelse"
                        },
                        okForFerdigattest = false,
                        okForFerdigattestSpecified = true,
                        harTilstrekkeligSikkerhet = false,
                        harTilstrekkeligSikkerhetSpecified = true,
                        typeArbeider = "For å oppnå tilstrekkelig sikkerhetsnivå, vil følgende arbeider bli utført: rekkverk på trapp",
                        utfoertInnen = new DateTime(2016, 06, 23),
                        utfoertInnenSpecified = true,
                    },

                    ansvarsomraadetAvsluttet = false,
                    ansvarsomraadetAvsluttetSpecified = true,
                    ansvarsrettErklaert = new DateTime(2016, 06, 23),
                    ansvarsrettErklaertSpecified = true
                },
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    beskrivelseAvAnsvarsomraade = "Arkitekturprosjektering",
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = new KontaktpersonType()
                        {
                            epost = "tor@arkitektum.no",
                            mobilnummer = "90900999",
                            navn = "Tage Binders",
                            telefonnummer = "36584123"
                        },
                        organisasjonsnummer = "910297937",
                        navn = "FANA OG HAFSLO REVISJON",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 1",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345678",
                        mobilnummer = "87654321",
                        epost = "tor@arkitektum.no",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        signaturdatoSpecified = true,
                        harSentralGodkjenning = true,
                    },


                    fristDato = DateTime.Now.AddDays(10),
                    fristDatoSpecified = true,
                    soeknadssystemetsReferanse = Guid.NewGuid().ToString(),
                    varsling = false,
                    varslingSpecified = true,

                    prosjekterende = new ProsjekterendeType()
                    {
                        okForFerdigattest = false,
                        okForFerdigattestSpecified = true,
                        okForIgangsetting = false,
                        okForIgangsettingSpecified = true,
                        okForMidlertidigBrukstillatelse = true,
                        okForMidlertidigBrukstillatelseSpecified = true,
                        okForRammetillatelse = false,
                        okForRammetillatelseSpecified = true
                    },

                    utfoerende = new UtfoerendeType()
                    {
                        okMidlertidigBrukstillatelse = false,
                        okMidlertidigBrukstillatelseSpecified = true,
                        midlertidigBrukstillatelseGjenstaaende = new DelsoeknadMidlertidigBrukstillatelseType()
                        {
                            gjenstaaendeInnenfor = "Gjenstående arbeider av mindre vesentlig betydning, innenfor den delen av tiltaket det søkes midlertidig brukstillatelse for",
                            gjenstaaendeUtenfor = "Angi resterende deler av tiltaket hvor det her ikke søkes om midlertidig brukstillatelse"
                        },
                        okForFerdigattest = false,
                        okForFerdigattestSpecified = true
                    },
                    
                    ansvarsomraadetAvsluttet = false,
                    ansvarsomraadetAvsluttetSpecified = true,
                    ansvarsrettErklaert = new System.DateTime(2016, 06, 23),
                    ansvarsrettErklaertSpecified = true
                },
                new AnsvarsomraadeType()
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Kontrollering av vann og avløp, samt sanitære installasjoner",
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType()
                        {
                            kodeverdi = "Foretak",
                            kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = new KontaktpersonType()
                        {
                            epost = "tor@arkitektum.no",
                            mobilnummer = "90900444",
                            navn = "Bjarne Røros",
                            telefonnummer = "36584444"
                        },
                        organisasjonsnummer = "911455307",
                        navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Mediumgate 23",
                            landkode = "NO",
                            postnr = "3740",
                            poststed = "Skien"
                        },
                        telefonnummer = "12345679",
                        mobilnummer = "87654322",
                        epost = "tor@arkitektum.no",
                        signaturdato = new DateTime(2016, 04, 11),
                        signaturdatoSpecified = true,
                        harSentralGodkjenning = true
                    },

                    fristDato = DateTime.Now.AddDays(10),
                    fristDatoSpecified = true,
                    soeknadssystemetsReferanse = Guid.NewGuid().ToString(),
                    varsling = false,
                    varslingSpecified = true,
                    ansvarsomraadetAvsluttet = false,
                    ansvarsomraadetAvsluttetSpecified = true,
                    ansvarsrettErklaert = new DateTime(2016, 06, 23),
                    ansvarsrettErklaertSpecified = true
                }
            };

            distribusjonKontrollOgSamsvarserklaering.erTEK10 = false;
            distribusjonKontrollOgSamsvarserklaering.erTEK10Specified = true;

            distribusjonKontrollOgSamsvarserklaering.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "911455307", 
                navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                kontaktperson = new KontaktpersonType()
                {
                    epost = "tor@arkitektum.no",
                    mobilnummer = "90900454",
                    navn = "Ingvild Testperson Halland",
                    telefonnummer = "36588888"
                },
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "tor@arkitektum.no"
            };

            return distribusjonKontrollOgSamsvarserklaering;
        }
    }
}
