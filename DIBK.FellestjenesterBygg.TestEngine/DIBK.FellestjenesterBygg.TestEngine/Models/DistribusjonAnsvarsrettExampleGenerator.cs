﻿using System;
using no.kxml.skjema.dibk.distribusjon_ansvarsrett;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class DistribusjonAnsvarsrettExampleGenerator
    {
        public DistribusjonAnsvarsrettType NyDistribusjonAnsvarsrett()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            var distribusjon_ansvarsrett = new DistribusjonAnsvarsrettType();

            distribusjon_ansvarsrett.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            distribusjon_ansvarsrett.hovedinnsendingsnummer = Guid.NewGuid().ToString();
            distribusjon_ansvarsrett.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2017",
                sakssekvensnummer = "123"
            };

            distribusjon_ansvarsrett.versjon = "v92";


            // ======= eiendomByggested ======= 
            distribusjon_ansvarsrett.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "5"
                    },
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 1",
                        postnr = "3800",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    bygningsnummer = "80466985",
                    bolignummer = "H0101"
                }
            };

            distribusjon_ansvarsrett.gjennomfoeringsplan = new[]
            {
                new AnsvarsomraadeType()
                {
                    fagomraade = "Prosjektering",
                    funksjon = new KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    beskrivelseAvAnsvarsomraade = "Arkitekturprosjektering",
                    tiltaksklasse = new KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Tage Binders",
                        organisasjonsnummer = "910297937",
                        navn = "FANA OG HAFSLO REVISJON",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 1",
                            landkode = "47",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345678",
                        mobilnummer = "87654321",
                        epost = "tor@arkitektum.no",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        harSentralGodkjenning = true
                    },


                    fristDato = DateTime.Now.AddDays(10),
                    vaarReferanse = Guid.NewGuid().ToString(),
                    varsling = false,

                    samsvarKontrollPlanlagtVedRammetillatelse = false,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = true,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,

                    samsvarKontrollPlanlagtVedFerdigattest = false,

                    ansvarsomraadetAvsluttet = false
                },
                new AnsvarsomraadeType()
                {
                    fagomraade = "Utførelse",
                    funksjon = new KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    beskrivelseAvAnsvarsomraade = "Prosjektering av vann og avløp, samt sanitære installasjoner",
                    tiltaksklasse = new KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Bjarne Røros",
                        organisasjonsnummer = "911455307", 
                        navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Mediumgate 23",
                            landkode = "47",
                            postnr = "3740",
                            poststed = "Skien"
                        },
                        telefonnummer = "12345679",
                        mobilnummer = "87654322",
                        epost = "tor@arkitektum.no",
                        signaturdato = new System.DateTime(2016, 04, 11),
                        harSentralGodkjenning = true
                    },

                    fristDato = DateTime.Now.AddDays(10),
                    vaarReferanse = Guid.NewGuid().ToString(),
                    varsling = true,

                    samsvarKontrollPlanlagtVedRammetillatelse = false,
                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,
                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollPlanlagtVedFerdigattest = false,

                    ansvarsomraadetAvsluttet = false
                },
            };

            distribusjon_ansvarsrett.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "911455307", 
                navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                kontaktperson = "Ingvild Testperson Halland",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "tor@arkitektum.no"
            };

            return distribusjon_ansvarsrett;
        }
    }
}
