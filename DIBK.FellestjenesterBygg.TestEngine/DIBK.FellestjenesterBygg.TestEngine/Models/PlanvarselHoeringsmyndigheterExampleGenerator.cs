﻿using System;
using no.kxml.skjema.dibk.planvarselHoeringsmyndigheter;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class PlanvarselHoeringsmyndigheterExampleGenerator
    {
        public PlanvarselHMType NyttNabovarsel()
        {
            var rand = new ExampleDataRandomizer();
            var nabovarselPlan = new PlanvarselHMType();

            Encryption.Encryption encrypt = new Encryption.Encryption();

            nabovarselPlan.kommunenavn = "Ringerike";
            nabovarselPlan.metadata = new MetadataType
            {
                fraSluttbrukersystem = "Fellestjenester bygg og plan testmotor"
            };
            
            nabovarselPlan.forslagsstiller = new PartType()
            {
                partstype = new KodeType
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910467905",
                navn = "BRYGGJA OG RANHEIM AS",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Sentrum",
                    poststed = "Hønefoss",
                    postnr = "3502"
                },
                telefon = "98839131",
                epost = "norah@bye.no",
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Kari Nordmann",
                    telefonnummer = "98839131",
                    mobilnummer = "98839131",
                    epost = "tine@arkitektum.no"
                }
            };

            nabovarselPlan.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Gata 19",
                        landkode = "NO",
                        postnr = "3502",
                        poststed = "Hønefoss"
                    },
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        gaardsnummer = "318",
                        bruksnummer = "97",
                        festenummer = "1",
                        seksjonsnummer = "1",
                        kommunenummer = "3007"
                    },
                    kommunenavn = "Ringerike"
                }
            };

            nabovarselPlan.gjeldendePlan = new []
            {
                new GjeldendePlanType()
                {
                    navn = "Reguleringsplan for Hønefoss",
                    plantype = new KodeType()
                    {
                        kodeverdi = "35",
                        kodebeskrivelse = "Detaljregulering"
                    }
                }
            };

            nabovarselPlan.beroerteParter = new[]
            {
                new BeroertPartType()
                {
                    partstype = new KodeType()
                    {
                        kodebeskrivelse = "Privatperson",
                        kodeverdi = "Privatperson"
                    },
                    navn = "FRIDTJOF KJEILEN",
                    //foedselsnummer = "13047301569",
                    foedselsnummer = encrypt.EncryptText("13047301569"),

                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Storgata 3",
                        postnr = "3502",
                        poststed = "Hønefoss",
                        landkode = "no"
                    },
                    telefon = "12312399",
                    epost = "tine@arkitektum.no",
                    systemReferanse = "ref-1"
                },

                new BeroertPartType()
                {
                    partstype = new KodeType()
                    {
                        kodebeskrivelse = "Foretak",
                        kodeverdi = "Foretak"
                    },
                    navn = "KRANGLE VELFORENING",
                    organisasjonsnummer = "910041126",

                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Rahangurra 2",
                        postnr = "3502",
                        poststed = "Hønefoss"
                    },
                    telefon = "12312399",
                    epost = "tine@arkitektum.no",
                    systemReferanse = "ref-2",
                    gjelderEiendom = new []
                    {
                        new GjelderEiendomType()
                        {
                            eiendomsidentifikasjon = new MatrikkelnummerType()
                            {
                                kommunenummer = "5001",
                                gaardsnummer = "110",
                                bruksnummer = "3",
                                festenummer = "385",
                                seksjonsnummer = "36"

                            },
                            adresse = new EiendommensAdresseType()
                            {
                                adresselinje1 = "Storgata 5",
                                postnr = "3502",
                                poststed = "Hønefoss"
                            }
                        }
                    }
                },

                new BeroertPartType()
                {
                    partstype = new KodeType()
                    {
                        kodebeskrivelse = "Foretak",
                        kodeverdi = "Foretak"
                    },
                    navn = "LØTEN OG BORGEN",
                    organisasjonsnummer = "810064412",

                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Rahangurra 2",
                        postnr = "3502",
                        poststed = "Hønefoss"
                    },
                    telefon = "12312399",
                    epost = "tine@arkitektum.no",
                    systemReferanse = "ref-3",
                    gjelderEiendom = new []
                    {
                        new GjelderEiendomType()
                        {
                            eiendomsidentifikasjon = new MatrikkelnummerType()
                            {
                                kommunenummer = "5001",
                                gaardsnummer = "109",
                                bruksnummer = "2",
                                festenummer = "345",
                                seksjonsnummer = "34"

                            },
                            adresse = new EiendommensAdresseType()
                            {
                                adresselinje1 = "Storgata 1",
                                postnr = "3502",
                                poststed = "Hønefoss"
                            }
                        },
                        new GjelderEiendomType()
                        {
                            eiendomsidentifikasjon = new MatrikkelnummerType()
                            {
                                kommunenummer = "5001",
                                gaardsnummer = "110",
                                bruksnummer = "3",
                                festenummer = "385",
                                seksjonsnummer = "36"

                            },
                            adresse = new EiendommensAdresseType()
                            {
                                adresselinje1 = "Storgata 5",
                                postnr = "3502",
                                poststed = "Hønefoss"
                            }
                        }
                    }
                }
            };

            nabovarselPlan.planforslag = new PlanType()
            {
                plannavn = "Citygården og hotellkvartalet",
                hjemmesidePlanforslag = new[]
                {
                    "www.kommunensHjemmeside.no",
                    "www.planforslag.no"
                },
                arealplanId = "464",
                kravKonsekvensUtredning = false,
                kravKonsekvensUtredningSpecified = true,
                begrunnelseKU = "Begrunnelse for hvorfor det ikke er krav om konsekvensutredning.",
                hjemmesidePlanprogram = "www.planprogram.no",
                planHensikt = 
                    "I tråd med områdereguleringsplanens målsettinger om å skape et levende og attraktivt " +
                    "bysentrum i Hønefoss, ønskes området detaljregulert for å legge til rette for et " +
                    "bærekraftig og realistisk byutviklingsprosjekt.  Aktuelle funksjoner er utvidelse av hotell/konferansesenter, serveringsteder, " +
                    "handel og boliger. Eksisterende kulturmiljø vil være et viktig tema i planarbeidet. " +
                    "Det er ikke krav om planprogram eller konsekvensutredning i tilknytning til planforslaget.",
                fristForInnspill = new DateTime(2020, 08, 01),
                fristForInnspillSpecified = true,
                saksgangOgMedvirkning = "Saksgang og medvirkning....",
                
                plantype = new KodeType
                {
                    kodeverdi = "35",
                    kodebeskrivelse = "Detaljregulering"
                },
                kommunensSaksnummer = new SaksnummerType()
                {
                    saksaar = "2020",
                    sakssekvensnummer = "11111"
                }
            };

            nabovarselPlan.plankonsulent = new PartType()
            {
                partstype = new KodeType
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "911455307",
                navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Sentrum",
                    poststed = "Hønefoss",
                    postnr = "3502"
                },
                telefon = "98839131",
                epost = "post@post.no",
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Ola Nordmann",
                    telefonnummer = "98839131",
                    mobilnummer = "98839131",
                    epost = "tine@arkitektum.no"
                }
            };

            nabovarselPlan.signatur = new SignaturType
            {
                signaturdato = new DateTime(2019, 09, 18),
                signaturdatoSpecified = true,
                signertAv = "",
                signertPaaVegneAv = "",
            };

            return nabovarselPlan;
        }
    }
}