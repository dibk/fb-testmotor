﻿using no.kxml.skjema.dibk.midlertidigbrukstillatelseV2;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class MidlertidigBrukstillatelseV2ExampleGenerator
    {
        public MidlertidigBrukstillatelseType NyMidlertidigBrukstillatelse()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            var midlertidigBrukstillatelse = new MidlertidigBrukstillatelseType();
            midlertidigBrukstillatelse.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            midlertidigBrukstillatelse.gjelderHeleTiltaket = true;
            midlertidigBrukstillatelse.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2015",
                sakssekvensnummer = "124550"
            };

            midlertidigBrukstillatelse.prosjektnavn = "FtB Panorama";

            midlertidigBrukstillatelse.tiltakshaver = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                foedselsnummer = "02038521576",
                navn = "Jens Jensen",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "soeker@domene.no"
            };
            midlertidigBrukstillatelse.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 77",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                       
                    },
                    bygningsnummer = "123456778",
                    bolignummer = "H0101",
                    kommunenavn = "Bø i Telemark"
                 
                }
            };
            midlertidigBrukstillatelse.datoFerdigattest = new System.DateTime(2016, 03, 29);
            midlertidigBrukstillatelse.delsoeknad = new[]
            {
                new DelsoeknadMidlertidigBrukstillatelseType()
                {
                    delAvTiltaket = "Første byggetrinn",
                    tillatelsesdato = new System.DateTime(2016, 01, 24 ),
                    tillatelsesdatoSpecified = true
                }
            };

            midlertidigBrukstillatelse.vilkaar = new[]
            {
                new VilkaarType
                {
                    vilkaarID = "1242",
                    beskrivelse = "Tiltaksplan for forurenset grunn, se eget brev",
                    utsettes = false,
                    utsettesSpecified = true,
                    besvart = true,
                    besvartSpecified = true, 
                    kommentar = "Eget vedlegg"
                }
            };
            midlertidigBrukstillatelse.gjenstaaendeInnenfor = "Overflatebehandling gulv";
            midlertidigBrukstillatelse.utfoertInnen = new System.DateTime(2016, 02, 14);
            midlertidigBrukstillatelse.utfoertInnenSpecified = true;
            midlertidigBrukstillatelse.typeArbeider = "Montert rekkverk på trapp";
            midlertidigBrukstillatelse.bekreftelseInnen = new System.DateTime(2016, 02, 04);
            midlertidigBrukstillatelse.bekreftelseInnenSpecified = true;
            midlertidigBrukstillatelse.gjenstaaendeUtenfor = "Byggetrinn 2";
            midlertidigBrukstillatelse.harTilstrekkeligSikkerhet = true;
            midlertidigBrukstillatelse.harTilstrekkeligSikkerhetSpecified = true;
            midlertidigBrukstillatelse.delAvTiltaket = "Andre byggetrinn";

            midlertidigBrukstillatelse.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "914994780", // 914994780 Arkitektum
                navn = "Byggmester Bob AS",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Fjellveien 3",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "hans@domene.no"
            };

            return midlertidigBrukstillatelse;
        }
    }
}