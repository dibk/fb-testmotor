﻿using System;
using System.Collections.Generic;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class Skjema
    {
        public Skjema()
        {
        }

        public Skjema(string formId, int serviceCode, int serviceEditionCode, string dataFormatID, string dataFormatVersion, string name, bool availableService, string templateUrl, string wikiUrl, string openWikiUrl, string modelUrl, string data, string sendTo, string formtype, List<SkjemaSet> skjemaSet, params Vedlegg[] vedlegg)
        {
            FormId = formId;
            ServiceCode = serviceCode;
            ServiceEditionCode = serviceEditionCode;
            DataFormatID = dataFormatID;
            DataFormatVersion = dataFormatVersion;
            Name = name;
            AvailableService = availableService;
            TemplateUrl = templateUrl;
            WikiUrl = wikiUrl;
            OpenWikiUrl = openWikiUrl;
            ModelUrl = modelUrl;
            Data = data;
            SendTo = sendTo;
            FormType = formtype;
            SkjemaSet = new List<SkjemaSet>(skjemaSet);
            Vedlegg = new List<Vedlegg>(vedlegg);
        }

        public string FormId { get; set; }
        public int ServiceCode { get; set; }
        public int ServiceEditionCode { get; set; }
        public string DataFormatID { get; set; }
        public string DataFormatVersion { get; set; }
        public string Name { get; set; }
        public bool AvailableService { get; set; }
        public string TemplateUrl { get; set; }
        public string WikiUrl { get; set; }
        public string OpenWikiUrl { get; set; }
        public ValidationApiUrl ValidationUrl { get; set; }
        public string ModelUrl { get; set; }
        public string Data { get; set; }
        public string SendTo { get; set; }
        public string FormType { get; set; }
        public string Status { get; set; }
        public List<Vedlegg> Vedlegg { get; set; }
        public List<SkjemaSet> SkjemaSet { get; set; }

        public bool HasDocumentation
        { 
            get { return !string.IsNullOrEmpty(OpenWikiUrl); }
        }
    }
}