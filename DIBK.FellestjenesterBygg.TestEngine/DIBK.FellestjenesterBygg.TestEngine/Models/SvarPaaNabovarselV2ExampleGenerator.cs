﻿using no.kxml.skjema.dibk.nabovarselsvarV2;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class SvarPaaNabovarselV2ExampleGenerator
    {
        public SvarPaaNabovarselType NySvarPaaNabovarsel()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var svarnabovarsel = new SvarPaaNabovarselType();
            svarnabovarsel.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            svarnabovarsel.prosjektnavn = "Prosjektnavn";
            svarnabovarsel.samtykkeTilTiltaket = true;
            svarnabovarsel.samtykkeTilTiltaketSpecified = true;
           
            var adresseTilHansen = new EnkelAdresseType()
            {
                adresselinje1 = "Storgata 5",
                postnr = "7003",
                poststed = "Trondheim"
            };
            svarnabovarsel.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Storgata 3",
                        gatenavn = "Storgata",
                        husnr = "3",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "1",
                        seksjonsnummer = "1",
                        kommunenummer = rand.GetRandomTestKommuneNummer()
                    }, 
                    kommunenavn = "Trondheim"
                }
            };

            svarnabovarsel.nabo = new NaboGjenboerType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                navn = "Ole Olsen",
                foedselsnummer = "08117000290",
                gjelderNaboeiendom = new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        gaardsnummer = "109",
                        bruksnummer = "1",
                        kommunenummer = "1601"
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Storgata 3",
                        postnr = "7003",
                        poststed = "Trondheim"
                    }
                },
                harPersonligMottattVarsel = true,
                harPersonligSamtykkeTilTiltaket = true
            };

            svarnabovarsel.tiltakshaver = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                foedselsnummer = "08117000290",
                navn = "MAJA Testperson KLEPPA",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Kirkegt 4",
                    poststed = "Bø i Telemark",
                    postnr = "3800"
                },
                telefonnummer = "56749845",
                mobilnummer = "44552211",
                epost = "tor@arkitektum.no"
            };

            svarnabovarsel.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910297937",
                navn = "FANA OG HAFSLO REVISJON",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "tor@arkitektum.no"
            };

            svarnabovarsel.signatur = new SignaturType
            {
                signaturdato = new System.DateTime(2016, 09, 08),
                signertAv = "",
                signertPaaVegneAv = "",
                signeringssteg = ""
            };

            return svarnabovarsel;
        }
    }
}
