﻿using no.kxml.skjema.dibk.gjennomforingsplan;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class GjennomforingsplanExampleGenerator
    {
        public GjennomfoeringsplanType NyGjenomforingsplan()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            var gjennomforingsplan = new GjennomfoeringsplanType();

            gjennomforingsplan.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            gjennomforingsplan.hovedinnsendingsnummer = "35001";
            gjennomforingsplan.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2016",
                sakssekvensnummer = "555"
            };

            gjennomforingsplan.versjon = "v92";


            // ======= eiendomByggested ======= 
            gjennomforingsplan.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "5"
                    },
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 1",
                        postnr = "3800",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    bygningsnummer = "80466985",
                    bolignummer = "H0101"
                }
            };


            // ======= gjennomfoeringsplan ======= 
            gjennomforingsplan.gjennomfoeringsplan = new[]
            {
                new AnsvarsomraadeType()
                {
                    fagomraade = "Prosjektering",
                    funksjon = new KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    arbeidsomraade = "Arkitekturprosjektering",
                    tiltaksklasse = new KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Bob Andersen",
                        organisasjonsnummer = "912660680", // 912660680 NÆRINGS- OG FISKERIDEPARTEMENTET
                        navn = "ARKITEKT ANDERSEN AS",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 1",
                            landkode = "47",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345678",
                        mobilnummer = "87654321",
                        epost = "bobby@domene.no",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,

                    samsvarKontrollPlanlagtVedFerdigattest = false,

                    ansvarsomraadetAvsluttet = true
                },
                new AnsvarsomraadeType()
                {
                    fagomraade = "Prosjektering",
                    funksjon = new KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    arbeidsomraade = "Konstruksjonssikkerhet",
                    tiltaksklasse = new KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Frank Sappar",
                        organisasjonsnummer = "974760223", // 974760223 DIBK
                        navn = "BYGGMESTRSELSKAPET AS",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 2",
                            landkode = "47",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345679",
                        mobilnummer = "87654322",
                        epost = "hallo@byggselsk.no",
                        signaturdato = new System.DateTime(2016, 04, 11),
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,
                    samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,

                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    ansvarsomraadetAvsluttet = false
                },
                new AnsvarsomraadeType()
                {
                    fagomraade = "Prosjektering",
                    funksjon = new KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    arbeidsomraade = "Oppmålingsteknisk prosjektering",
                    tiltaksklasse = new KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new ForetakType()
                    {
                        kontaktperson = "Lil E. Taa",
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        organisasjonsnummer = "913255852", // 913255852 DIBK Gjøvik
                        navn = "JENSEN PLAN OG MÅL AS",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 2",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "12345679",
                        mobilnummer = "87654322",
                        epost = "dweezil@domene.no",
                        signaturdato = new System.DateTime(2016, 01, 11),
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,
                    samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = true,
                    samsvarKontrollForeliggerVedIgangsettingstillatelse = new System.DateTime(2016, 04, 02),
                    samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = new System.DateTime(2016, 04, 03),
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedFerdigattest = true,
                    samsvarKontrollForeliggerVedFerdigattest = new System.DateTime(2016, 04, 15),
                    samsvarKontrollForeliggerVedFerdigattestSpecified = true,

                    ansvarsomraadetAvsluttet = false
                },
                new AnsvarsomraadeType()
                {
                    fagomraade = "Prosjektering",
                    funksjon = new KodeType()
                    {
                        kodeverdi = "PRO",
                        kodebeskrivelse = "Ansvarlig prosjektering"
                    },
                    arbeidsomraade = "Prosjektering av vann og avløp, samt sanitære installasjoner",
                    tiltaksklasse = new KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Harry Viskositet",
                        organisasjonsnummer = "974702665", // 974702665 DIBK Oslo
                        navn = "HERMANSEN VVS AS",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 3",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "92345679",
                        mobilnummer = "87688322",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        epost = "hermansenVvs@vvsH.no",
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,
                    samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = new System.DateTime(2016, 04, 03),
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    ansvarsomraadetAvsluttet = true
                },
                new AnsvarsomraadeType()
                {
                    fagomraade = "Utførelse",
                    funksjon = new KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    arbeidsomraade = "All utførelse med unntak av rørleggerarbeider samt utstikking og oppmåling",
                    tiltaksklasse = new KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Birger Pumpetang",
                        organisasjonsnummer = "914994780",  // 914994780 Arkitektum
                        navn = "Byggmester Bob AS",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 4",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "92345579",
                        mobilnummer = "87688822",
                        epost = "firmapost@ByggBob.no",
                        signaturdato = new System.DateTime(2016, 02, 23),
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,
                    samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = true,
                    samsvarKontrollForeliggerVedIgangsettingstillatelse = new System.DateTime(2016, 04, 02),
                    samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = new System.DateTime(2016, 04, 03),
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedFerdigattest = true,
                    samsvarKontrollForeliggerVedFerdigattest = new System.DateTime(2016, 04, 15),
                    samsvarKontrollForeliggerVedFerdigattestSpecified = true,

                    ansvarsomraadetAvsluttet = false
                },
                new AnsvarsomraadeType()
                {
                    fagomraade = "Utførelse",
                    funksjon = new KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    arbeidsomraade = "Konstruksjonssikkerhet",
                    tiltaksklasse = new KodeType() {
                        kodeverdi = "2",
                        kodebeskrivelse = "2"
                    },
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Ante Oppi",
                        organisasjonsnummer = "974760673", // 974760673 BRREG, Altinn brukerservice
                        navn = "A+ Konstruksjon",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 5",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "92345579",
                        mobilnummer = "87688822",
                        epost = "info@Apluss.no",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,

                    samsvarKontrollPlanlagtVedFerdigattest = false,

                    ansvarsomraadetAvsluttet = false
                },
                new AnsvarsomraadeType()
                {
                    fagomraade = "Utførelse",
                    funksjon = new KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    arbeidsomraade = "Oppmålingsteknisk prosjektering",
                    tiltaksklasse = new KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Marsek Jensen",
                        organisasjonsnummer = "912660680", // 912660680 NÆRINGS- OG FISKERIDEPARTEMENTET
                        navn = "JENSEN PLAN OG MÅL AS",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 6",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "97345579",
                        mobilnummer = "87611822",
                        epost = "jepm@jepp.no",
                        signaturdato = new System.DateTime(2016, 06, 23),
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,
                    samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = true,
                    samsvarKontrollForeliggerVedIgangsettingstillatelse = new System.DateTime(2016, 04, 02),
                    samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,

                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    ansvarsomraadetAvsluttet = false
                },
                new AnsvarsomraadeType()
                {
                    fagomraade = "Utførelse",
                    funksjon = new KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    arbeidsomraade = "Oppmålingsteknisk prosjektering",
                    tiltaksklasse = new KodeType() {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    foretak = new ForetakType()
                    {
                        partstype = new KodeType() {
                           kodeverdi = "Foretak" ,
                           kodebeskrivelse = "Foretak"
                        },
                        kontaktperson = "Margit Berg",
                        organisasjonsnummer = "974760223", // 974760223 DIBK
                        navn = "Berg Oppmåling AS",
                        adresse = new EnkelAdresseType()
                        {
                            adresselinje1 = "Bøgata 7",
                            landkode = "NO",
                            postnr = "3800",
                            poststed = "Bø i Telemark"
                        },
                        telefonnummer = "97555579",
                        mobilnummer = "87611866",
                        epost = "berg@oppmaaling.no",
                        signaturdato = new System.DateTime(2016, 01, 04),
                        harSentralGodkjenning = true
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = true,
                    samsvarKontrollForeliggerVedRammetillatelse = new System.DateTime(2016, 03, 29),
                    samsvarKontrollForeliggerVedRammetillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = true,
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = new System.DateTime(2016, 04, 03),
                    samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true,

                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    ansvarsomraadetAvsluttet = false
                }
            };

            gjennomforingsplan.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "913255852", // 913255852 DIBK Gjøvik
                navn = "AS Foretak Ansv. Søk.",
                kontaktperson = "Ingvild Testperson Halland",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "soeker@domene.no"
            };

            return gjennomforingsplan;
        }
    }
}
