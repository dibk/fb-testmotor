﻿using no.kxml.skjema.dibk.opphoerAvAnsvarsrett;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class OpphoerAnsvarsrettExampleGenerator
    {
        public OpphoerAnsvarsettType NyOpphoerAnsvarsrett()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var opphoerAnsvarsrett = new OpphoerAnsvarsettType();

            opphoerAnsvarsrett.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2017",
                sakssekvensnummer = "3456"
            };

            opphoerAnsvarsrett.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer =  rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 1",
                        postnr = "3800",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    bygningsnummer = "80466985",
                    bolignummer = "H0102"
                }
            };

            opphoerAnsvarsrett.opphoerAnsvarsrett = new AnsvarsrettType[]
            {
               new AnsvarsrettType()
               {
                   ansvarsomraader= new AnsvarsomraadeType
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "KONTROLL",
                        kodebeskrivelse = "Ansvarlig kontroll"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for kontroll",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },

                    vaarReferanse = System.Guid.NewGuid().ToString()
               },
                   foretaketMelderOmOpphoer = true
            },

               new AnsvarsrettType()
               {
                   ansvarsomraader= new AnsvarsomraadeType
                {
                    funksjon = new KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    beskrivelseAvAnsvarsomraade = "Overordnet ansvar for utførelse (bygning, anlegg eller konstruksjon, tekniske installasjoner)",
                    tiltaksklasse = new KodeType()
                    {
                        kodeverdi = "2",
                        kodebeskrivelse = "Tiltaksklasse 2"
                    },

                    vaarReferanse = System.Guid.NewGuid().ToString()
               }
            }             

            };

            opphoerAnsvarsrett.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910297937",
                navn = "FANA OG HAFSLO REVISJON",
                kontaktperson = "Siv. Ing. Borge",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "soeker@domene.no"
            };

            opphoerAnsvarsrett.tiltakshaver = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                foedselsnummer = "08117000290",
                navn = "MAJA Testperson KLEPPA",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Kirkegt 4",
                    poststed = "Bø i Telemark",
                    postnr = "3800"
                },
                telefonnummer = "56749845",
                mobilnummer = "44552211",
                epost = "mj@domene.no"
            };

            opphoerAnsvarsrett.signatur = new SignaturType
            {
                signaturdato = new System.DateTime(2016, 09, 08),
                signertAv = "",
                signertPaaVegneAv = "",
                signeringssteg = ""
            };


            return opphoerAnsvarsrett;
        }
    }
}