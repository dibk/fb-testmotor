﻿using no.kxml.skjema.dibk.ferdigattestV2;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class FerdigattestV2ExampleGenerator
    {

        public FerdigattestType NyFerdigattest()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var ferdigattestV2 = new FerdigattestType();
            ferdigattestV2.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            ferdigattestV2.prosjektnavn = "FTB Panorama";

            ferdigattestV2.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2015",
                sakssekvensnummer = "123"
            };

            ferdigattestV2.tilfredstillerTiltaketKraveneFerdigattestSpecified = true;
            ferdigattestV2.tilfredstillerTiltaketKraveneFerdigattest = true;
            

            ferdigattestV2.tiltakshaver = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                foedselsnummer = "08117000290",
                navn = "MAJA Testperson KLEPPA",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Kirkegt 4",
                    poststed = "Bø i Telemark",
                    postnr = "3800"
                },
                telefonnummer = "56749845",
                mobilnummer = "44552211",
                epost = "mj@domene.no"
            };

            ferdigattestV2.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    },
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Gamlegt 4",                     
                        poststed = "Bø i Telemark",
                        postnr = "3800"
                    },
                    bygningsnummer = "123456789",
                    bolignummer = "H0102", 
                    kommunenavn = "Bø i Telemark"
                },
            };

            ferdigattestV2.foretattIkkeSoeknadspliktigeJusteringerSpecified = true;
            ferdigattestV2.foretattIkkeSoeknadspliktigeJusteringer = true;
            ferdigattestV2.tilstrekkeligDokumentasjonOverlevertEierSpecified = true;
            ferdigattestV2.tilstrekkeligDokumentasjonOverlevertEier = false;
            ferdigattestV2.utfoertInnenSpecified = true;
            ferdigattestV2.utfoertInnen = new System.DateTime(2016, 12, 15);
            ferdigattestV2.typeArbeider = "Dette er en beskrivelse av det gjenstående arbeidet før kravene til ferdigattest er tilfredstilt.";
            ferdigattestV2.bekreftelseInnenSpecified = true;
            ferdigattestV2.bekreftelseInnen = new System.DateTime(2016, 12, 01);




            ferdigattestV2.energiforsyning = new EnergiforsyningType()
            {
                varmefordeling = new []
                {
                    new KodeType()
                    {
                        kodeverdi = "elektriske panelovner",
                        kodebeskrivelse = "elektriske panelovner"
                    },
                    new KodeType()
                    {
                        kodeverdi = "elektriske varmekabler",
                        kodebeskrivelse = "elektriske varmekabler"
                    },
               },
                energiforsyning = new []
                {
                    new KodeType()
                    {
                        kodeverdi = "biobrensel",
                        kodebeskrivelse = "biobrensel"
                    },
                    new KodeType()
                    {
                        kodeverdi = "elektrisitet",
                        kodebeskrivelse = "elektrisitet"
                    },
                },
                relevant = false,
                relevantSpecified = true
            };


            ferdigattestV2.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "974702665", // 974702665 DIBK Oslo
                navn = "Byggmester Bob AS",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Christian V gate 34",
                    poststed = "Bø i Telemark",
                    postnr = "3800"
                },
                telefonnummer = "45678900",
                mobilnummer = "44667788",
                epost = "jan@domene.no"
            };

            return ferdigattestV2;
        }
    }
}
