﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class SkjemaSet
    {
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public string XmlData { get; set; }
        public string Name { get; set; }

        public SkjemaSet(string name, string dataFormatId, string dataFormatVersion, string xmlData)
        {
            Name = name;
            DataFormatId = dataFormatId;
            DataFormatVersion = dataFormatVersion;
            XmlData = xmlData;
        }

        public SkjemaSet()
        {
        }
    }
}