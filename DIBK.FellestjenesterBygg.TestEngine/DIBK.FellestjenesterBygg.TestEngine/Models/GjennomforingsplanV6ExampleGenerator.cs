﻿using System;
using System.Collections.Generic;
using DIBK.FellestjenesterBygg.TestEngine.Models.ModelData;
using System.Web.Services.Description;
using no.kxml.skjema.dibk.gjennomforingsplanV6;
using System.Web.UI;
using Org.BouncyCastle.Bcpg.OpenPgp;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class GjennomforingsplanV6ExampleGenerator
    {
        public GjennomfoeringsplanType NyGjennomforingsplanV6()
        {
            // Todo!
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            Random random = new Random();

            var gjennomforingsplanV6 = new GjennomfoeringsplanType();

            gjennomforingsplanV6.metadata = new MetadataType()
            {
                fraSluttbrukersystem = "Fellestjenester bygg testmotor",
                ftbId = "707338700000000",
                sluttbrukersystemUrl = "https://fellesbygg.dibk.no",
                prosjektnavn = "Hovedgata Nyutvikling",
                hovedinnsendingsnummer = "35001",
                klartForSigneringFraSluttbrukersystem = true,
                klartForSigneringFraSluttbrukersystemSpecified = true
            };

            gjennomforingsplanV6.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2020",
                sakssekvensnummer = "555"
            };

            gjennomforingsplanV6.versjon = "6.0";


            // ======= eiendomByggested ======= 
            gjennomforingsplanV6.eiendomByggested = new[]
            {
                new no.kxml.skjema.dibk.gjennomforingsplanV6.EiendomType()
                {
                    eiendomsidentifikasjon = new no.kxml.skjema.dibk.gjennomforingsplanV6.MatrikkelnummerType
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "5"
                    },
                    adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EiendommensAdresseType
                    {
                        adresselinje1 = "Bøgata 1",
                        postnr = "3800",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    bygningsnummer = "80466985",
                    bolignummer = "H0101"
                }
            };


            // ======= gjennomfoeringsplan ======= 
            gjennomforingsplanV6.gjennomfoeringsplan = new[]
            {
                new no.kxml.skjema.dibk.gjennomforingsplanV6.AnsvarsomraadeType()
                {
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                    {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    ansvarsomraade = "Varme- og kuldeinstallasjoner",
                    samsvarKontrollPlanlagtVedRammetillatelse = false,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,

                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    ansvarsomraadetAvsluttet = false
                },
                new no.kxml.skjema.dibk.gjennomforingsplanV6.AnsvarsomraadeType()
                {
                    funksjon = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                    {
                        kodeverdi = "UTF",
                        kodebeskrivelse = "Ansvarlig utførelse"
                    },
                    ansvarsomraade = "Annkomst veg ",
                    tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                    {
                        kodeverdi = "1",
                        kodebeskrivelse = "1"
                    },
                    samsvarKontrollPlanlagtVedRammetillatelse = false,

                    samsvarKontrollPlanlagtVedIgangsettingstillatelse = false,

                    samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = false,

                    samsvarKontrollPlanlagtVedFerdigattest = true,

                    ansvarsomraadetAvsluttet = false
                },
                GetAnsvarsomraade(FunctionType.UTF, "Utførende grunnarbeider", random),
                GetAnsvarsomraade(FunctionType.UTF, "Utførende tømrerarbeid", random),
                GetAnsvarsomraade(FunctionType.UTF, "Utførende elektriskeinstallasjonser", random),

                GetAnsvarsomraade(FunctionType.PRO, "Prosjekterende akritektur", random),
                GetAnsvarsomraade(FunctionType.PRO, "Prosjekterende brannsikkerhet", random),
                GetAnsvarsomraade(FunctionType.PRO, "Prosjekterende VVS", random),
                GetAnsvarsomraade(FunctionType.KONTROLL, "Kontrollerende brannkrav", random),
            };

            



            gjennomforingsplanV6.ansvarligSoeker = new no.kxml.skjema.dibk.gjennomforingsplanV6.PartType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910065203", // 913255852 DIBK Gjøvik
                navn = "ARKITEKT FLINK",
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Ingvild Halland",
                    epost = "ingvild@halland.no",
                    mobilnummer = "97979797"
                },
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "soeker@domene.no"
            };
            gjennomforingsplanV6.ansvarligSoekerTiltaksklasse = new KodeType()
            {
                kodeverdi = "1",
                kodebeskrivelse = "1"
            };

            return gjennomforingsplanV6;
        }




        private no.kxml.skjema.dibk.gjennomforingsplanV6.AnsvarsomraadeType GetAnsvarsomraade(FunctionType functionType, string ansvarsomraadeBeskrivelse, Random random)
        {

            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            string tt = rand.GetTiltaksklasse();

            var randomForetak = ansvarligeForetak[random.Next(ansvarligeForetak.Count)];


            var ansvarsomraade = new no.kxml.skjema.dibk.gjennomforingsplanV6.AnsvarsomraadeType()
            {

                funksjon = GetKodeType(functionType),

                ansvarsomraade = ansvarsomraadeBeskrivelse,
                tiltaksklasse = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = tt,
                    kodebeskrivelse = tt
                },
                foretak = randomForetak,
             };

            AddDatesAndPhase(ansvarsomraade);


            return ansvarsomraade;

        }



        private enum FunctionType
        {
            PRO,
            UTF,
            KONTROLL
        }


        private KodeType GetKodeType(FunctionType functionType)
        {
            var funksjon = new KodeType();

            switch (functionType)
            {
                case FunctionType.PRO:
                    funksjon.kodeverdi = "PRO";
                    funksjon.kodebeskrivelse = "Ansvarlig prosjekterende";
                    break;
                case FunctionType.UTF:
                    funksjon.kodeverdi = "UTF";
                    funksjon.kodebeskrivelse = "Ansvarlig utførende";
                    break;
                case FunctionType.KONTROLL:
                    funksjon.kodeverdi = "KONTROLL";
                    funksjon.kodebeskrivelse = "Ansvarlig kontrollerende";
                    break;
                default:
                    funksjon.kodeverdi = "UTF";
                    funksjon.kodebeskrivelse = "Ansvarlig utførende";
                    break;
            }
            return funksjon;    
        }


        private AnsvarsomraadeType AddDatesAndPhase(AnsvarsomraadeType ansvarsomraadeType)
        {
            Random random = new Random();
            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            List<string> proPhases = new List<string>() {"RS", "IG"};
            string proPhase = proPhases[random.Next(proPhases.Count)];
                            
            List<string> utfPhases = new List<string>() {"MB", "FA"};
            string utfPhase = utfPhases[random.Next(utfPhases.Count)];

            List<string> kontrollPhases = new List<string>() {"MB", "FA"};
            string kontrollPhase = kontrollPhases[random.Next(kontrollPhases.Count)];

            List<string> fileNames = new List<string>() { "", "file.pdf", "kart.png"};



            ansvarsomraadeType.ansvarsomraadetAvsluttet = random.Next(100) < 50;


            // Preconfig some stuff
            ansvarsomraadeType.samsvarKontrollPlanlagtVedRammetillatelseSpecified = false;
            ansvarsomraadeType.samsvarKontrollForeliggerVedRammetillatelseSpecified = false;
            ansvarsomraadeType.samsvarKontrollPlanlagtVedIgangsettingstillatelseSpecified = false;
            ansvarsomraadeType.samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = false;
            ansvarsomraadeType.samsvarKontrollPlanlagtVedMidlertidigBrukstillatelseSpecified = false;
            ansvarsomraadeType.samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = false;
            ansvarsomraadeType.samsvarKontrollPlanlagtVedFerdigattestSpecified = false;
            ansvarsomraadeType.samsvarKontrollForeliggerVedFerdigattestSpecified = false;

            ansvarsomraadeType.erklaeringSignert = true;
            ansvarsomraadeType.sluttbrukersystemReferanse = System.Guid.NewGuid().ToString();
            ansvarsomraadeType.sluttbrukersystemStatus = "Signert";
            ansvarsomraadeType.erklaeringArkivreferanse = "AR" + random.Next(99999999).ToString();
            ansvarsomraadeType.filnavnVedlegg = fileNames[random.Next(fileNames.Count)];



            if (ansvarsomraadeType.funksjon.kodeverdi == "PRO" && proPhase == "RS" && ansvarsomraadeType.ansvarsomraadetAvsluttet == true ) 
            { 
                ansvarsomraadeType.samsvarKontrollForeliggerVedRammetillatelse = rand.GetRandomDaysInThePast();
                ansvarsomraadeType.samsvarKontrollForeliggerVedRammetillatelseSpecified = true;
            }
            if (ansvarsomraadeType.funksjon.kodeverdi == "PRO" && proPhase == "RS" && ansvarsomraadeType.ansvarsomraadetAvsluttet == false )
            {
                ansvarsomraadeType.samsvarKontrollPlanlagtVedRammetillatelse = true;
                ansvarsomraadeType.samsvarKontrollPlanlagtVedRammetillatelseSpecified = true;
            }
            if (ansvarsomraadeType.funksjon.kodeverdi == "PRO" && proPhase == "IG" && ansvarsomraadeType.ansvarsomraadetAvsluttet == true )
            {
                ansvarsomraadeType.samsvarKontrollForeliggerVedIgangsettingstillatelse = rand.GetRandomDaysInThePast();
                ansvarsomraadeType.samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true;
            }
            if (ansvarsomraadeType.funksjon.kodeverdi == "PRO" && proPhase == "IG" && ansvarsomraadeType.ansvarsomraadetAvsluttet == false)
            {
                ansvarsomraadeType.samsvarKontrollPlanlagtVedIgangsettingstillatelse = true;
                ansvarsomraadeType.samsvarKontrollPlanlagtVedIgangsettingstillatelseSpecified = true;
            }


            if ((ansvarsomraadeType.funksjon.kodeverdi == "UTF" || ansvarsomraadeType.funksjon.kodeverdi == "KONTROLL") && proPhase == "MB" && ansvarsomraadeType.ansvarsomraadetAvsluttet == true)
            {
                ansvarsomraadeType.samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = rand.GetRandomDaysInThePast();
                ansvarsomraadeType.samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true;
            }
            if ((ansvarsomraadeType.funksjon.kodeverdi == "UTF" || ansvarsomraadeType.funksjon.kodeverdi == "KONTROLL") && proPhase == "MB" && ansvarsomraadeType.ansvarsomraadetAvsluttet == false)
            {
                ansvarsomraadeType.samsvarKontrollPlanlagtVedIgangsettingstillatelse = true;
                ansvarsomraadeType.samsvarKontrollPlanlagtVedIgangsettingstillatelseSpecified = true;
            }
            if ((ansvarsomraadeType.funksjon.kodeverdi == "UTF" || ansvarsomraadeType.funksjon.kodeverdi == "KONTROLL") && proPhase == "FA" && ansvarsomraadeType.ansvarsomraadetAvsluttet == true)
            {
                ansvarsomraadeType.samsvarKontrollForeliggerVedFerdigattest = rand.GetRandomDaysInThePast();
                ansvarsomraadeType.samsvarKontrollForeliggerVedFerdigattestSpecified = true;
            }
            if ((ansvarsomraadeType.funksjon.kodeverdi == "UTF" || ansvarsomraadeType.funksjon.kodeverdi == "KONTROLL") && proPhase == "FA" && ansvarsomraadeType.ansvarsomraadetAvsluttet == false)
            {
                ansvarsomraadeType.samsvarKontrollPlanlagtVedFerdigattest = true;
                ansvarsomraadeType.samsvarKontrollPlanlagtVedFerdigattestSpecified = true;
            }

            return ansvarsomraadeType;
        }








        private readonly List<no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType> ansvarligeForetak = new List<ForetakType> {
            {
                new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
                {
                    partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    kontaktperson = new KontaktpersonType()
                    {
                        navn = "Kontakt Person",
                        epost = "kp@firma.no",
                        mobilnummer = "10101010"
                    },
                    organisasjonsnummer = "912660680", // 912660680 NÆRINGS- OG FISKERIDEPARTEMENTET
                    navn = "ARKITEKT ANDERSEN AS",
                    adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                    {
                        adresselinje1 = "Bøgata 1",
                        landkode = "NO",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                    },
                    telefonnummer = "12345678",
                    mobilnummer = "87654321",
                    epost = "bobby@domene.no",
                    harSentralGodkjenningSpecified = true,
                    harSentralGodkjenning = true
                }

             },
            new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Lina Linse", epost = "lina@linse.no", mobilnummer = "74747474"
                },
                organisasjonsnummer = "912660680", // 912660680 NÆRINGS- OG FISKERIDEPARTEMENTET
                navn = "Kontroll Nisser 2 AS",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 24",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "12345678",
                mobilnummer = "87654321",
                epost = "bobby@domene.no",
                harSentralGodkjenningSpecified = true,
                harSentralGodkjenning = true
            },
            new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Bob Andersen",
                    epost = "bob@andersen.no",
                    mobilnummer = "10101010"
                },
                organisasjonsnummer = "912660680", // 912660680 NÆRINGS- OG FISKERIDEPARTEMENTET
                navn = "ARKITEKT ANDERSEN AS",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 1",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "12345678",
                mobilnummer = "87654321",
                epost = "bobby@domene.no",
                harSentralGodkjenningSpecified = true,
                harSentralGodkjenning = true
            },
            new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Bill Andersen", epost = "bill@andersen.no", mobilnummer = "99925874"
                },
                organisasjonsnummer = "974760223",
                navn = "Brann Systemer AS",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 2",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "12345678",
                mobilnummer = "87654321",
                epost = "billy@domene.no",
                harSentralGodkjenningSpecified = true,
                harSentralGodkjenning = true
            },

            new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Frank Sappar", epost = "frank@sappar.no", mobilnummer = "12121212"
                },
                organisasjonsnummer = "974760223", // 974760223 DIBK
                navn = "Ledende Lede Folk AS",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 3",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "12345679",
                mobilnummer = "87654322",
                epost = "hallo@byggselsk.no",
                harSentralGodkjenningSpecified = true,
                harSentralGodkjenning = true
            },

            new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
            {
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Lil E. Taa", epost = "lil@taa.no", mobilnummer = "36925874"
                },
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "913255852", // 913255852 DIBK Gjøvik
                navn = "JENSEN PLAN OG MÅL AS",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 2",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "12345679",
                mobilnummer = "87654322",
                epost = "dweezil@domene.no",
                harSentralGodkjenningSpecified = true,
                harSentralGodkjenning = false
            },
            new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Harry Viskositet", epost = "harry@viskositet.no", mobilnummer = "96969696"
                },
                organisasjonsnummer = "974702665", // 974702665 DIBK Oslo
                navn = "HERMANSEN VVS AS",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 3",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "92345679",
                mobilnummer = "87688322",
                epost = "hermansenVvs@vvsH.no",
                harSentralGodkjenningSpecified = true,
                harSentralGodkjenning = true
            },
            new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Birger Pumpetang", epost = "birger@pumpetang.no", mobilnummer = "36929999"
                },
                organisasjonsnummer = "914994780", // 914994780 Arkitektum
                navn = "Byggmester Bob AS",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 4",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "92345579",
                mobilnummer = "87688822",
                epost = "firmapost@ByggBob.no",
                harSentralGodkjenningSpecified = true,
                harSentralGodkjenning = true
            },
            new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Margit Berg", epost = "margit@berg.no", mobilnummer = "98898898"
                },
                organisasjonsnummer = "974760223", // 974760223 DIBK
                navn = "Berg Oppmåling AS",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 7",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "97555579",
                mobilnummer = "87611866",
                epost = "berg@oppmaaling.no",
                harSentralGodkjenningSpecified = true,
                harSentralGodkjenning = true
            },
            new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Harry Viskositet", epost = "harry@viskositet.no", mobilnummer = "96969696"
                },
                organisasjonsnummer = "974702665", // 974702665 DIBK Oslo
                navn = "HERMANSEN VVS AS",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 3",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "92345679",
                mobilnummer = "87688322",
                epost = "hermansenVvs@vvsH.no",
                harSentralGodkjenningSpecified = true,
                harSentralGodkjenning = true
            },
            new no.kxml.skjema.dibk.gjennomforingsplanV6.ForetakType()
            {
                partstype = new no.kxml.skjema.dibk.gjennomforingsplanV6.KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                kontaktperson = new KontaktpersonType()
                {
                    navn = "Lina Linse", epost = "lina@linse.no", mobilnummer = "74747474"
                },
                organisasjonsnummer = "912660680", // 912660680 NÆRINGS- OG FISKERIDEPARTEMENTET
                navn = "Kontroll Nisser 2 AS",
                adresse = new no.kxml.skjema.dibk.gjennomforingsplanV6.EnkelAdresseType()
                {
                    adresselinje1 = "Bøgata 24",
                    landkode = "NO",
                    postnr = "3800",
                    poststed = "Bø i Telemark"
                },
                telefonnummer = "12345678",
                mobilnummer = "87654321",
                epost = "bobby@domene.no",
                harSentralGodkjenningSpecified = true,
                harSentralGodkjenning = true
            },
        };
    }

}
