﻿using no.kxml.skjema.dibk.matrikkelregistrering;
using System;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class MatrikkelregistreringExampleGenerator
    {
        public MatrikkelregistreringType Matrikkelregistrering()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var form = new MatrikkelregistreringType
            {
                eiendomsidentifikasjon = new[]
                {
                    new MatrikkelnummerType()
                    {
                        kommunenummer = rand.GetRandomTestKommuneNummer(),
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "0",
                        seksjonsnummer = "0"
                    }
                },
                kommunenavn = "Bergen",

                adresse = new[]
                {
                    new AdresseType()
                    {
                        adressekode = "kode",
                        adressenavn = "Gatenavn",
                        adressenummer = "1",
                        adressebokstav = "B",
                        seksjonsnummer = "0"
                    },
                    new AdresseType()
                    {
                        adressekode = "kode",
                        adressenavn = "Gatenavn",
                        adressenummer = "2",
                        adressebokstav = "A",
                        seksjonsnummer = "0"
                    }
                },
                bygning = new[]
                {
                    new BygningType
                    {
                        bygningsnummer = "1213234",
                        naeringsgruppe = new KodeType()
                        {
                            kodeverdi = "X",
                            kodebeskrivelse = "Bolig"
                        },
                        bygningstype = new KodeType()
                        {
                            kodeverdi = "111",
                            kodebeskrivelse = "Enebolig"
                        },
                        bebygdAreal = 160,
                        bebygdArealSpecified = true,
                        etasjer = new[]
                        {
                            new EtasjeType
                            {
                                etasjeopplysning = BygningsOpplysningType.Ny,
                                etasjeplan = new KodeType
                                {
                                    kodeverdi = "U",
                                    kodebeskrivelse = "Underetasje"
                                },
                                etasjenummer = "01",
                                antallBoenheter = "1",
                                bruksarealTotalt = 100,
                                bruksarealTotaltSpecified = true,
                                bruksarealTilAnnet = 0,
                                bruksarealTilAnnetSpecified = true,
                                bruksarealTilBolig = 100,
                                bruksarealTilBoligSpecified = true,
                                bruttoarealTilBolig = 0,
                                bruttoarealTilBoligSpecified = true,
                                bruttoarealTilAnnet = 0,
                                bruttoarealTilAnnetSpecified = true,
                                bruttoarealTotalt = 0,
                                bruttoarealTotaltSpecified = true,
                            },
                            new EtasjeType
                            {
                                etasjeopplysning = BygningsOpplysningType.Ny,
                                etasjeplan = new KodeType
                                {
                                    kodeverdi = "H",
                                    kodebeskrivelse = "Hovedetasje"
                                },
                                etasjenummer = "01",
                                antallBoenheter = "2",
                                bruksarealTotalt = 150,
                                bruksarealTotaltSpecified = true,
                                bruksarealTilAnnet = 0,
                                bruksarealTilAnnetSpecified = true,
                                bruksarealTilBolig = 150,
                                bruksarealTilBoligSpecified = true,
                                bruttoarealTilBolig = 0,
                                bruttoarealTilBoligSpecified = true,
                                bruttoarealTilAnnet = 0,
                                bruttoarealTilAnnetSpecified = true,
                                bruttoarealTotalt = 0,
                                bruttoarealTotaltSpecified = true,
                            }
                        },
                        bruksenheter = new[]
                        {
                            new BruksenhetType
                            {
                                boligOpplysning = BygningsOpplysningType.Ny,

                                bruksenhetsnummer = new BruksenhetsnummerType
                                {
                                    etasjeplan = new KodeType
                                    {
                                        kodeverdi = "U",
                                        kodebeskrivelse = "Underetasje"
                                    },
                                    etasjenummer = "01",
                                    loepenummer = "01"
                                },
                                bruksenhetstype = new KodeType
                                {
                                    kodeverdi = "B",
                                    kodebeskrivelse = "Bolig"
                                },
                                kjoekkentilgang = new KodeType
                                {
                                    kodeverdi = "0",
                                    kodebeskrivelse = "Ikke oppgitt"
                                },
                                bruksareal = 100,
                                bruksarealSpecified = true,
                                antallRom = "3",
                                antallBad = "1",
                                antallWC = "1",
                                adresse = new BoligadresseType
                                {
                                    adressekode = "kode",
                                    adressenavn = "Gatenavn",
                                    adressenummer = "1",
                                    adressebokstav = "A",
                                    seksjonsnummer = "0"
                                }
                            },
                            new BruksenhetType
                            {
                                boligOpplysning = BygningsOpplysningType.Ny,

                                bruksenhetsnummer = new BruksenhetsnummerType
                                {
                                    etasjeplan = new KodeType
                                    {
                                        kodeverdi = "H",
                                        kodebeskrivelse = "Hovedetasje"
                                    },
                                    etasjenummer = "02",
                                    loepenummer = "01"
                                },
                                bruksenhetstype = new KodeType
                                {
                                    kodeverdi = "B",
                                    kodebeskrivelse = "Bolig"
                                },
                                kjoekkentilgang = new KodeType
                                {
                                    kodeverdi = "0",
                                    kodebeskrivelse = "Ikke oppgitt"
                                },
                                bruksareal = 150,
                                bruksarealSpecified = true,
                                antallRom = "4",
                                antallBad = "2",
                                antallWC = "0",
                                adresse = new BoligadresseType
                                {
                                    adressekode = "kode",
                                    adressenavn = "Gatenavn",
                                    adressenummer = "1",
                                    adressebokstav = "A",
                                    seksjonsnummer = "0"
                                }
                            }
                        },
                        avlop = new KodeType()
                        {
                            kodeverdi = "OffentligKloakk",
                            kodebeskrivelse = "Offentlig avlÃ¸psanlegg"
                        },

                        vannforsyning = new KodeType()
                        {
                            kodeverdi = "AnnenPrivatInnlagt",
                            kodebeskrivelse = "Annen privat vannforsyning, innlagt vann"
                        },

                        energiforsyning = new EnergiforsyningType()
                        {
                            varmefordeling = new[]
                            {
                                new KodeType()
                                {
                                    kodeverdi = "elektriskePanelovner",
                                    kodebeskrivelse = "Elektriske panelovner"
                                },
                                new KodeType()
                                {
                                    kodeverdi = "elektriskeVarmekabler",
                                    kodebeskrivelse = "Elektriske varmekabler"
                                },
                            },
                            energiforsyning = new[]
                            {
                                new KodeType()
                                {
                                    kodeverdi = "biobrensel",
                                    kodebeskrivelse = "Biobrensel"
                                },
                                new KodeType()
                                {
                                    kodeverdi = "elektrisitet",
                                    kodebeskrivelse = "Elektrisitet"
                                },
                            },
                            relevant = false,
                            relevantSpecified = true
                        },
                        harHeis = false,
                        harHeisSpecified = true
                    }
                },
                signatur = new SignaturType
                {
                    signaturdato = DateTime.Now,
                    signaturdatoSpecified = true,
                    signertAv = "Eksempel"
                }
            };


            return form;
        }
    }
}