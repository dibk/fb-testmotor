﻿using System;
using no.kxml.skjema.dibk.nabovarsel;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class NabovarselExampleGenerator
    {
        public NabovarselType NyttNabovarsel()
        {
            var rand = new ExampleDataRandomizer();
            var nabovarsel = new NabovarselType();

            var adresseTilHansen = new EnkelAdresseType
            {
                adresselinje1 = "Storgata 5",
                postnr = "7003",
                poststed = "Trondheim"
            };

            nabovarsel.eiendomByggested = new[]
            {
                new EiendomType
                {
                    adresse = new EiendommensAdresseType
                    {
                        adresselinje1 = "Storgata 19",
                        landkode = "NO",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                    },
                    eiendomsidentifikasjon = new MatrikkelnummerType
                    {
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "1",
                        seksjonsnummer = "1",
                        kommunenummer = rand.GetRandomTestKommuneNummer()
                    }
                }
            };


            nabovarsel.beskrivelseAvTiltak = new[]
            {
                new TiltakType
                {
                    bruk = new[]
                    {
                        new KodeType
                        {
                            kodeverdi = "Annet",
                            kodebeskrivelse = "Annet"
                        }
                    },
                    type = new[]
                    {
                        new KodeType
                        {
                            kodeverdi = "fasade",
                            kodebeskrivelse = "Endring av bygg - utvendig - Fasade"
                        }
                    },
                    foelgebrev = "foelgebrev"
                }
            };


            nabovarsel.rammebetingelser = new RammerType
            {
                arealdisponering = new ArealdisponeringType
                {
                    gjeldendePlan = new PlanType
                    {
                        plantype = new KodeType
                        {
                            kodeverdi = "RP",
                            kodebeskrivelse = "Reguleringsplan"
                        },
                        navn = "rammebetingelser navn"
                    }
                }
            };

            nabovarsel.tiltakshaver = new PartType
            {
                partstype = new KodeType
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                foedselsnummer = "10023000208",
                navn = "Tor Hammer",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Roald Dalhs gate 4",
                    poststed = "Skien",
                    postnr = "3701"
                },
                telefonnummer = "46576879",
                mobilnummer = "99009900",
                epost = "tor@arkitektum.no"
            };


            nabovarsel.naboeier =
                new NaboGjenboerType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Privatperson",
                        kodebeskrivelse = "Privatperson"
                    },
                    navn = "Vigdis Vater",
                    foedselsnummer = "10019000953",

                    adresse = new EnkelAdresseType
                    {
                        adresselinje1 = "Storgata 3",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },

                    gjelderNaboeiendom = new EiendomType
                    {
                        eiendomsidentifikasjon = new MatrikkelnummerType
                        {
                            gaardsnummer = "109",
                            bruksnummer = "1",
                            kommunenummer = "5001",
                            seksjonsnummer = "1"
                        },
                        adresse = new EiendommensAdresseType
                        {
                            adresselinje1 = "Storgata 3",
                            postnr = "7003",
                            poststed = "Trondheim"
                        }
                    }
                };


            nabovarsel.ansvarligSoeker = new PartType
            {
                partstype = new KodeType
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910065203",
                navn = "ARKITEKT FLINK",
                telefonnummer = "12345678",
                mobilnummer = "99009900",
                epost = "tor@arkitektum.no"
            };

            nabovarsel.dispensasjon = new DispensasjonType
            {
                dispensasjonstype = new[]
                {
                    new KodeType
                    {
                        kodeverdi = "PLAN",
                        kodebeskrivelse = "Arealplaner"
                    }
                }
            };

            nabovarsel.signatur = new SignaturType
            {
                signaturdato = new DateTime(2016, 09, 08),
                signertAv = "",
                signertPaaVegneAv = "",
                signeringssteg = ""
            };

            return nabovarsel;
        }
    }
}