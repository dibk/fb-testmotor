using no.kxml.skjema.dibk.samsvarserklaeringDirekteOpprettet;
using Org.BouncyCastle.Crypto.Engines;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class SamsvarserklaeringDirekteOpprettetExampleGenerator
    {
        public SamsvarserklaeringDirekteType NySamsvarserklaeringDirekteOpprettet()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var nySamsvarserklaeringDirekteOpprettet = new SamsvarserklaeringDirekteType()
            {
                fraSluttbrukersystem = "Fellestjenester bygg testmotor",
                hovedinnsendingsnummer = "44556677",
                prosjektnavn = "FTB Panorama",

                eiendomByggested = new[]
                {
                    new EiendomType()
                    {
                        eiendomsidentifikasjon = new MatrikkelnummerType
                        {
                            kommunenummer = rand.GetRandomTestKommuneNummer(),
                            gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                            bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                            festenummer = "0",
                            seksjonsnummer = "0"
                        },
                        adresse = new EiendommensAdresseType
                        {
                            adresselinje1 = "Bøgata 1",
                            postnr = "3800",
                            poststed = "Bø i Telemark",
                            landkode = "NO"
                        },
                        bygningsnummer = "80466985",
                        bolignummer = "H0102", 
                        kommunenavn = "Bø i Telemark"
                    }
                },

                ansvarligSoeker = new PartType()
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    organisasjonsnummer = "911455307",
                    navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                    kontaktperson = new KontaktpersonType()
                    {
                        navn = "Ingvild Testperson Halland",
                        mobilnummer = "99995555",
                        epost = "tor@arkitektum.no"
                    },
                    adresse = new EnkelAdresseType
                    {
                        adresselinje1 = "Bøgata 16",
                        postnr = "3802",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "tor@arkitektum.no"
                },

                kommunensSaksnummer = new SaksnummerType()
                {
                    saksaar = "2016",
                    sakssekvensnummer = "3456"
                }
            };

            nySamsvarserklaeringDirekteOpprettet.kommunensSaksnummer.saksaar = "2016";
            nySamsvarserklaeringDirekteOpprettet.kommunensSaksnummer.sakssekvensnummer = "231";

            nySamsvarserklaeringDirekteOpprettet.prosjektnr = "99";

            nySamsvarserklaeringDirekteOpprettet.foretak = new PartType()
            {
                partstype = new KodeType
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910065211",
                navn = "Nordmann Bygg og Anlegg AS",

                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "12345678",
                mobilnummer = "98765432",
                epost = "ola@byggmestern-ola.no",
            };

            nySamsvarserklaeringDirekteOpprettet.ansvarsrett = new AnsvarsomraadeType()
            {
                ansvarsrettErklaert = new System.DateTime(2016, 12, 05),
                ansvarsrettErklaertSpecified = true,
                funksjon = new KodeType()
                {
                    kodeverdi = "UTF",
                    kodebeskrivelse = "Ansvarlig utførelse"
                },
                beskrivelseAvAnsvarsomraadet = "Utføring av cement arbeid",
          
                prosjekterende = new ProsjekterendeType()
                {
                    okForFerdigattest = false,
                    okForFerdigattestSpecified = true,
                    okForRammetillatelse = true,
                    okForRammetillatelseSpecified = true,
                    okForIgangsetting = false,
                    okForIgangsettingSpecified = true,
                    okForMidlertidigBrukstillatelse = false,
                    okForMidlertidigBrukstillatelseSpecified = true
                },
                utfoerende = new UtfoerendeType()
                {
                    okForFerdigattest = false,
                    okForFerdigattestSpecified = true,
                    midlertidigBrukstillatelseGjenstaaende = new DelsoeknadMidlertidigBrukstillatelseType()
                        {
                            gjenstaaendeInnenfor = "Trapp konstruksjon øst",
                            gjenstaaendeUtenfor = "Forskaling av mur."
                        },
                    okMidlertidigBrukstillatelse = true,
                    okMidlertidigBrukstillatelseSpecified = true,
                    harTilstrekkeligSikkerhet = true,
                    harTilstrekkeligSikkerhetSpecified = true,
                    utfoertInnen = new System.DateTime(2016, 09, 30),
                    utfoertInnenSpecified = true,
                    typeArbeider = ""
                },
                ansvarsomraadetAvsluttet = true,
                ansvarsomraadetAvsluttetSpecified = true
            };

            nySamsvarserklaeringDirekteOpprettet.erklaeringUtfoerelse = true;
            nySamsvarserklaeringDirekteOpprettet.erklaeringUtfoerelseSpecified = true;
            nySamsvarserklaeringDirekteOpprettet.erklaeringProsjektering = false;
            nySamsvarserklaeringDirekteOpprettet.erklaeringProsjekteringSpecified = true;
           
            nySamsvarserklaeringDirekteOpprettet.signatur = new SignaturType
            {
                signaturdato = new System.DateTime(2016, 09, 08),
                signaturdatoSpecified = true,
                signertAv = "",
                signertPaaVegneAv = "",
                signeringssteg = ""
            };

            nySamsvarserklaeringDirekteOpprettet.erTEK10 = false;
            nySamsvarserklaeringDirekteOpprettet.erTEK10Specified = true;

            return nySamsvarserklaeringDirekteOpprettet;
        }
    }
}
