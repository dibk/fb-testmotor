﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Dispensasjon
    {
        public string Begrunnelse { get; set; }
        public string Beskrivelse { get; set; }
        public Kode Dispensasjonstype { get; set; }
        public Dispensasjon()
        {
            Begrunnelse = "Begrunnelse for dispensasjon";
            Beskrivelse = "Søknad om dispensasjon fra bestemmelsen NN...";
            Dispensasjonstype = new Kode()
            {
                Kodeverdi = "TEK",
                Kodebeskrivelse = "Byggteknisk forskrift med veiledning"
            };
        }
    }
}