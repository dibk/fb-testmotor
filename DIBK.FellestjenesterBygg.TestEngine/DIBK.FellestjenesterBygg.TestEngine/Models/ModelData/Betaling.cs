﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Betaling
    {
        public bool SkalFaktureres { get; set; }
        public bool SkalFaktureresSpecified { get; set; }
        public string Beskrivelse { get; set; }
        public string OrdreId { get; set; }
        public string Sum { get; set; }
        public string TransId { get; set; }
        public string GebyrKategori { get; set; }

        public Betaling()
        {
            SkalFaktureres = true;
            SkalFaktureresSpecified = true;
            Sum = "4221";
            GebyrKategori = "8";
            Beskrivelse = "En beskrivelse...";
        }
    }
}