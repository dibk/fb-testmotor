﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Saksnummer
    {
        public string Saksaar { get; set; }
        public string Sakssekvensnummer { get; set; }
    }
}