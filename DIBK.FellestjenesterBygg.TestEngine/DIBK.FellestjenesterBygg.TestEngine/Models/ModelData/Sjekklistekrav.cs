﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Sjekklistekrav
    {
        public System.Nullable<bool> Sjekklistepunktsvar { get; set; }
        public bool SjekklistepunktsvarSpecified { get; set; }

        public Kode Sjekklistepunkt { get; set; }
        public string Dokumentasjon { get; set; }
        public Sjekklistekrav()
        {

        }
        public Sjekklistekrav(System.Nullable<bool> sjekklistepunktsvar, bool sjekklistepunktsvarSpecified, Kode sjekklistepunkt, string dokumentasjon)
        {
            SjekklistepunktsvarSpecified = sjekklistepunktsvarSpecified;
            Sjekklistepunktsvar = sjekklistepunktsvar;
            Sjekklistepunkt = sjekklistepunkt;
            Dokumentasjon = dokumentasjon;
        }
        public Sjekklistekrav(System.Nullable<bool> sjekklistepunktsvar, Kode sjekklistepunkt, string dokumentasjon = "")
        {
            SjekklistepunktsvarSpecified = true;
            Sjekklistepunktsvar = sjekklistepunktsvar;
            Sjekklistepunkt = sjekklistepunkt;
            Dokumentasjon = dokumentasjon;
        }
    }
}