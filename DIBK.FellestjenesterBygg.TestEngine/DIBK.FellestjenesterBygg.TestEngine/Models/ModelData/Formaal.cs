﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Formaal
    {
        public Kode Anleggstype { get; set; }
        public Kode Bygningstype { get; set; }
        public Kode Naeringsgruppe { get; set; }
        public List<Kode> Tiltaksformaal { get; set; }
        public string BeskrivPlanlagtFormaal { get; set; }
    }
}