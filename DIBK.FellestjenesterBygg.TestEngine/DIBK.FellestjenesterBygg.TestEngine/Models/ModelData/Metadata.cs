﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Metadata
    {
        public string FraSluttbrukersystem { get; set; }
        public string Prosjektnavn { get; set; }
        public string FtbId { get; set; }
        public string SluttbrukersystemUrl { get; set; }
        public string Hovedinnsendingsnummer { get; set; }
        public bool KlartForSigneringFraSluttbrukersystem { get; set; }
        public bool KlartForSigneringFraSluttbrukersystemSpecified { get; set; }
        public bool ErNorskSvenskDansk { get; set; }
        public bool ErNorskSvenskDanskSpecified { get; set; }

        public bool UnntattOffentlighet { get; set; }
        public bool UnntattOffentlighetSpecified { get; set; }

        public Metadata()
        {
            FraSluttbrukersystem = "Fellestjenester bygg testmotor";
            Prosjektnavn = "Nytt prosjekt";
            FtbId = "1000000000001";
            SluttbrukersystemUrl = "https://fellesbygg.dibk.no";
            KlartForSigneringFraSluttbrukersystem = true;
            KlartForSigneringFraSluttbrukersystemSpecified = true;
            ErNorskSvenskDansk = true;
            ErNorskSvenskDanskSpecified = true;
            UnntattOffentlighet = false;
            UnntattOffentlighetSpecified = true;
        }
    }
}