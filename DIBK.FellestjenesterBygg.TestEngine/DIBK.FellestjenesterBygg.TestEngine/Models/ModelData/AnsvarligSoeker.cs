﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class AnsvarligSoeker
    {
        public Kode Partstype { get; set; }
        public string Navn { get; set; }
        public string Organisasjonsnummer { get; set; }
        public EnkelAdresse Adresse { get; set; }
        public string Mobilnummer { get; set; }
        public string Telefonnummer { get; set; }
        public string Epost { get; set; }
        public Kontaktperson Kontaktperson { get; set; }


        public AnsvarligSoeker()
        {
            Partstype = new Kode()
            {
                Kodeverdi = "Foretak",
                Kodebeskrivelse = "Foretak"
            };
            
            Organisasjonsnummer = "910598023";
            Navn = "HUSNES OG OPPDAL";
            
            Kontaktperson = new Kontaktperson()
            {
                Epost = "tor@arkitektum.no",
                Mobilnummer = "90900454",
                Navn = "Ingvild Testperson Halland",
                Telefonnummer = "36588888"
            };

            Adresse = new EnkelAdresse
            {
                Adresselinje1 = "Bøgata 16",
                Postnr = "3802",
                Poststed = "Bø i Telemark",
                Landkode = "NO"
            };

            Telefonnummer = "11223344";
            Mobilnummer = "99887766";
            Epost = "tor@arkitektum.no";
        }
    }
}