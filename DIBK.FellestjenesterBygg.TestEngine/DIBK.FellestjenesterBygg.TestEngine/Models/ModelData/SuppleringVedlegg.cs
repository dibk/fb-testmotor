﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class SuppleringVedlegg
    {
        public Kode Vedleggstype { get; set; }
        public string Versjonsnummer { get; set; }
        public DateTime Versjonsdato { get; set; }
        public string Filnavn { get; set; }


        public SuppleringVedlegg(string versjonsnummer, string filnavn)
        {
            Vedleggstype = new Kode()
            {
                Kodeverdi = "Situasjonsplan",
                Kodebeskrivelse = "Situasjonsplan"
            };

            Versjonsnummer = versjonsnummer;
            Versjonsdato = DateTime.Now;
            Filnavn = filnavn;
        }
    }
}