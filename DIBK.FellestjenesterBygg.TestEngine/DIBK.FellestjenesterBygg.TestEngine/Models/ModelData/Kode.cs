﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Kode
    {
        public string Kodeverdi { get; set; }
        public string Kodebeskrivelse { get; set; }
        public Kode()
        {

        }
        public Kode(string kodeverdi, string kodebeskrivelse)
        {
            Kodeverdi = kodeverdi;
            Kodebeskrivelse = kodebeskrivelse;
        }
    }
}