﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Mangelbesvarelse
    {
        public List<SuppleringVedlegg> SuppleringVedlegg { get; set; }
        public Kode Tema { get; set; }
        public Mangel Mangel { get; set; }
        public string Tittel { get; set; }
        public string BeskrivelseFraKommunen { get; set; }
        public bool ErMangelBesvart { get; set; }
        public bool ErMangelBesvartSpecified { get; set; }
        public bool ErMangelBesvaresSenere { get; set; }
        public bool ErMangelBesvaresSenereSpecified { get; set; }
        public string Kommentar { get; set; }
        public Kode[] Underskjema { get; set; }

        public Mangelbesvarelse()
        {
            SuppleringVedlegg = new List<SuppleringVedlegg> { new SuppleringVedlegg("2", "Et_annet_filnavn.pdf") };

            Tema = new Kode()
            {
                Kodeverdi = "byggetomt",
                Kodebeskrivelse = "Byggetomt og ubebygd areal"
            };

            Mangel = new Mangel();
            Tittel = "Mangelbesvarelse nr 1";
            BeskrivelseFraKommunen = "Beskrivelse fra kommunen for mangelbesvarelsen";
            ErMangelBesvart = true;
            ErMangelBesvartSpecified = true;
            ErMangelBesvaresSenere = false;
            ErMangelBesvaresSenereSpecified = true;
            Kommentar = "Kommentar til mangelbesvarelsen";
        }
    }
}