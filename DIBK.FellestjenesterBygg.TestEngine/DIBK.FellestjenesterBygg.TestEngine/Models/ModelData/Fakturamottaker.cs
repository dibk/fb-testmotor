﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Fakturamottaker
    {
        public string Navn { get; set; }
        public string Organisasjonsnummer { get; set; }
        public EnkelAdresse Adresse { get; set; }
        public string Prosjektnummer { get; set; }
        public string BestillerReferanse { get; set; }
        public string Fakturareferanser { get; set; }
        public bool FakturaPapir { get; set; }
        public bool FakturaPapirSpecified { get; set; }
        public bool EhfFaktura { get; set; }
        public bool EhfFakturaSpecified { get; set; }

        public Fakturamottaker()
        {
            Organisasjonsnummer = "910297937";
            Navn = "FANA OG HAFSLO REVISJON";

            Adresse = new EnkelAdresse()
            {
                Adresselinje1 = "Bøgata 16",
                Postnr = "3802",
                Poststed = "Bø i Telemark",
                Landkode = "NO"
            };
            Prosjektnummer = "12345";
            BestillerReferanse = "B.Ref1234";
            Fakturareferanser = "Ref999";
            FakturaPapir = false;
            FakturaPapirSpecified = true;
            EhfFaktura = true;
            EhfFakturaSpecified = true;
        }
    }
}