﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Kontaktperson
    {
        public string Navn { get; set; }
        public string Telefonnummer { get; set; }
        public string Mobilnummer { get; set; }
        public string Epost { get; set; }
    }
}