﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class EiendomByggested
    {
        public Matrikkel Eiendomsidentifikasjon { get; private set; }
        public EiendomsAdresse Adresse { get; set; }
        public string Bygningsnummer { get; set; }
        public string Bolignummer { get; set; }
        public string Kommunenavn { get; set; }
        ExampleDataRandomizer rand = new ExampleDataRandomizer();
        public EiendomByggested()
        {
            Eiendomsidentifikasjon = new Matrikkel()
            {
                Kommunenummer = "3817",
                Gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                Bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                Festenummer = "0",
                Seksjonsnummer = "0"
            };

            Adresse = new EiendomsAdresse()
            {
                Adresselinje1 = "Bøgata 1",
                Adresselinje2 = "co/HosKven",
                Postnr = "3800",
                Poststed = "Bø i Telemark",
                Landkode = "NO"
            };
            Bygningsnummer = "80466985";
            Bolignummer = "H0102";
            Kommunenavn = "Midt Telemark";
        }
    }
}