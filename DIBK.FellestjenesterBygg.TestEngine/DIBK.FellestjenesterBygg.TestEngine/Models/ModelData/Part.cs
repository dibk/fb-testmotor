﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Part
    {
        public Kode partstype { get; set; }
        public string Organisasjonsnummer { get; set; }
        public string Navn { get; set; }

    }
}