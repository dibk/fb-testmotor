﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Signatur
    {
        public DateTime Signaturdato { get; set; }
        public bool SignaturdatoSpecified { get; set; }

        public string SignertAv { get; set; }
        public string SignertPaaVegneAv { get; set; }

        public Signatur()
        {
            Signaturdato = new System.DateTime(2021, 10, 05);
            SignaturdatoSpecified = true;
            SignertAv = "Ole Brumm";
            SignertPaaVegneAv = "Tigergutt";
        }

    }
}