﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Tiltakshaver
    {
        public Kode Partstype { get; set; }
        public string Navn { get; set; }
        public string Foedselsnummer { get; set; }
        public string Organisasjonsnummer { get; set; }
        public EnkelAdresse Adresse { get; set; }
        public string Mobilnummer { get; set; }
        public string Telefonnummer { get; set; }
        public string Epost { get; set; }
        public Kontaktperson Kontaktperson { get; set; }

        public Tiltakshaver()
        {
            Partstype = new Kode
            {
                Kodeverdi = "Foretak",
                Kodebeskrivelse = "Foretak"
            };
            Organisasjonsnummer = "910748548";
            Navn = "BLOMSTERDALEN OG ØVRE SNERTINGDAL";

            Adresse = new EnkelAdresse()
            {
                Adresselinje1 = "Bøgata 16",
                Postnr = "3802",
                Poststed = "Bø i Telemark",
                Landkode = "NO"
            };

            Telefonnummer = "11223344";
            Epost = "tine@arkitektum.no";
            
            Kontaktperson = new Kontaktperson()
            {
                Navn = "Tine Høllre",
                Telefonnummer = "98839131",
                Epost = "tine@arkitektum.no"
            };

        }

    }
}