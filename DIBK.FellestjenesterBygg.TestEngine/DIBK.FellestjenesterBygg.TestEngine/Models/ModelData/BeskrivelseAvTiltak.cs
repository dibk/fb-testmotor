﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class BeskrivelseAvTiltak
    {
        public Formaal Bruk { get; set; }
        public string BRA { get; set; }
        public List<Kode> Type { get; set; }
        public BeskrivelseAvTiltak()
        {
            Bruk = new Formaal()
            {
                Anleggstype = new Kode()
                {
                    Kodeverdi = "industri",
                    Kodebeskrivelse = "Industri"
                },
                Bygningstype = new Kode()
                {
                    Kodeverdi = "249",
                    Kodebeskrivelse = "Annen landbruksbygning"
                },
                Naeringsgruppe = new Kode()
                {
                    Kodeverdi = "I",
                    Kodebeskrivelse = "Overnattings- og serveringsvirksomhet"
                },
                Tiltaksformaal = new List<Kode>() { new Kode(){ Kodeverdi = "Bolig", Kodebeskrivelse = "Bolig" }, new Kode() { Kodeverdi = "Annet", Kodebeskrivelse = "Annet" } },
                BeskrivPlanlagtFormaal = "En beskrivelse av planlagt formål....."
            };
            BRA = "300";
            Type = new List<Kode>
            {
                new Kode() { Kodeverdi = "nyttbyggover70m2", Kodebeskrivelse = "Nytt bygg - Over 70 m2 -Ikke boligformål" },
                new Kode() { Kodeverdi = "nyttbyggdriftsbygningunder1000m2", Kodebeskrivelse = "Nytt bygg - Driftsbygning i landbruk med samlet areal under 1000 m2" }
            };
        }
    }
}