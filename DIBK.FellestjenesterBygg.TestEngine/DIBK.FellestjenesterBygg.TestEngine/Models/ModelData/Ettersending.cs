﻿using System.Collections.Generic;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Ettersending
    {
        public List<SuppleringVedlegg> SuppleringVedlegg { get; set; }
        public Kode Tema { get; set; }
        public string Tittel { get; set; }
        public Kode[] Underskjema { get; set; }
        public string Kommentar { get; set; }

        public Ettersending()
        {
            SuppleringVedlegg = new List<SuppleringVedlegg> { new SuppleringVedlegg("1", "Et_filnavn.pdf")}; 

            Tema = new Kode()
            {
                Kodeverdi = "andreForhold",
                Kodebeskrivelse = "Andre forhold"
            };
            
            Tittel = "Ettersending nr 1";
            Kommentar = "Kommentar til ettersending";
        }
    }
}