﻿namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Mangel
    {
        public string MangelId { get; set; }
        public Kode Mangeltekst { get; set; }

        public Mangel()
        {
            Mangeltekst = new Kode()
            {
                Kodeverdi = "1.35",
                Kodebeskrivelse = "Avkjørsel er ikke inntegnet"
            };
            
            MangelId = "Id_paa_mangelen";
        }
    }
}