﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.TestEngine.Models.ModelData
{
    public class Arbeidsplasser
    {
        public bool Framtidige { get; set; }
        public bool FramtidigeSpecified { get; set; }
        public bool Faste { get; set; }
        public bool FasteSpecified { get; set; }
        public bool Midlertidige { get; set; }
        public bool MidlertidigeSpecified { get; set; }
        public bool Eksisterende { get; set; }
        public bool EksisterendeSpecified { get; set; }
        public bool UtleieBygg { get; set; }
        public bool UtleieByggSpecified { get; set; }
        public bool Veiledning { get; set; }
        public bool VeiledningSpecified { get; set; }
        public string AntallAnsatte { get; set; }
        public string AntallVirksomheter { get; set; }
        public string Beskrivelse { get; set; }

        public Arbeidsplasser()
        {
            Framtidige = false;
            FramtidigeSpecified = true;
            Faste = false;
            FasteSpecified = true;
            Midlertidige = true;
            MidlertidigeSpecified = true;
            Eksisterende = true;
            EksisterendeSpecified = true;
            UtleieBygg = true;
            UtleieByggSpecified = true;
            Veiledning = false;
            VeiledningSpecified = true;
            AntallAnsatte = "30";
            AntallVirksomheter = "2";
            Beskrivelse = "Beskrivelse av arbeidets art, arbeidsplassene, prosesser og aktiviteter";
        }
    }
}