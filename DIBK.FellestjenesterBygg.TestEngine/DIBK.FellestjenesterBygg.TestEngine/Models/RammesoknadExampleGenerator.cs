﻿using no.kxml.skjema.dibk.rammesoknad;

// Class for generating example data base on the
// rammesoknad09.xsd scheema

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class RammesoknadExampleGenerator
    {
        public RammetillatelseType NyRammetillatelse()
        {

            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            var rammetillatelse = new RammetillatelseType();

            rammetillatelse.fraSluttbrukersystem = "Fellestjenester bygg testmotor";


            // Tiltakshaver
            rammetillatelse.tiltakshaver =
               new PartType
               {
                   partstype = new KodeType
                   {
                       kodeverdi = "Privatperson",
                       kodebeskrivelse = "Privatperson"
                   },
                   foedselsnummer = "08022016123",
                   navn = "Hans Hansen",
                   adresse = new EnkelAdresseType()
                   {
                       adresselinje1 = "Storgata 5",
                       postnr = "7003",
                       poststed = "Trondheim"
                   },
                   telefonnummer = "11223344",
                   mobilnummer = "99887766",
                   epost = "hanshansen@bmail.no",
                   signaturdato = new System.DateTime(2016, 07, 28),
                   signaturdatoSpecified = true
               };


            // ansvarlig soeker
            rammetillatelse.ansvarligSoeker =
                    new PartType
                    {
                        partstype = new KodeType
                        {
                            kodeverdi = "Foretak",
                            kodebeskrivelse = "Foretak"
                        },
                        organisasjonsnummer = "914994780", // 914994780 Arkitektum
                        navn = "Siv. Ark. Fjell og Sønner",
                        kontaktperson = "Benjamin Fjell",
                        adresse = new EnkelAdresseType
                        {
                            adresselinje1 = "Lillegata 5",
                            postnr = "7003",
                            poststed = "Trondheim"
                        },
                        telefonnummer = "11223344",
                        mobilnummer = "99887766",
                        epost = "post@fjell.no",
                        signaturdato = new System.DateTime(2016, 07, 26),
                        signaturdatoSpecified = true
                    };




            // eiendomByggested
            rammetillatelse.eiendomByggested = new[] {
                    new EiendomType
                    {
                        eiendomsidentifikasjon = new MatrikkelnummerType
                        {
                            kommunenummer = rand.GetRandomTestKommuneNummer(),
                            gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                            bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                            festenummer = "0",
                            seksjonsnummer = "0"
                        },
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = "Nygate 33",
                            postnr = "3825",
                            poststed = "Lunde"
                        },
                        bygningsnummer = "123456789",
                        bolignummer = "H0101"
                    }
                };


            // rammebetingelser
            var rammebetingelserAdkomst = new VegType
            {
                nyeEndretAdkomst = true,
                erTillatelseGittRiksFylkesveg = true,
                erTillatelseGittKommunalVeg = false,
                erTillatelseGittPrivatVeg = false,
                vegtype = new[]
                {
                   new KodeType
                   {
                       kodeverdi = "PrivatVeg",
                       kodebeskrivelse = "PrivatVeg"
                   }
                }
            };

            var rammebetingelserArealdisponering = new ArealdisponeringType
            {
                tomtearealByggeomraade = 1000.5,
                gjeldendePlan = new PlanType
                {
                    utnyttingsgrad = 23.0,
                    plantype = new KodeType
                    {
                        kodeverdi = "RP",
                        kodebeskrivelse = "Reguleringsplan"
                    },
                    navn = "Naustgrendanabbevannet",
                    formaal = "Fritidsbegyggelse",
                    andreRelevanteKrav = "andre relevante krav her",
                    beregningsregelGradAvUtnytting = new KodeType
                    {
                        kodeverdi = "%BYA",
                        kodebeskrivelse = "Prosent bebygd areal"
                    }
                },
                tomtearealSomTrekkesFra = 30.2,
                tomtearealSomLeggesTil = 50.3,
                tomtearealBeregnet = 1020,
                beregnetMaksByggeareal = 300.2,
                arealBebyggelseEksisterende = 200.4,
                arealBebyggelseSomSkalRives = 10.6,
                arealBebyggelseNytt = 30.5,
                parkeringsarealTerreng = 18.0,
                arealSumByggesak = 240.5,
                beregnetGradAvUtnytting = 23.36
            };

            var rammebetingerlserGenerelleVilkaar = new GenerelleVilkaarType
            {
                oppfyllesVilkaarFor3Ukersfrist = true,
                beroererTidligere1850 = false,
                forhaandskonferanseAvholdt = true,
                paalagtUavhengigKontroll = true,
                beroererArbeidsplasser = true,
                utarbeideAvfallsplan = false,
                behovForTillatelse = true
            };


            var arealverdierRammebetingelserBygningsopplysinger = new BygningsarealType
            {
                arealBYA = new ArealfordelingType
                {
                    arealEksisterendebebyggelse = 200.3,
                    arealEksisterendebebyggelseSpecified = true,
                    arealNyBebyggelse = 30.2,
                    arealNyBebyggelseSpecified = true,
                    aapneArealer = 60.4,
                    aapneArealerSpecified = true,
                    sumAreal = 290.7,
                    sumArealSpecified = true
                },
                arealBoligBRA = new ArealfordelingType
                {
                    arealEksisterendebebyggelse = 300.3,
                    arealEksisterendebebyggelseSpecified = true,
                    arealNyBebyggelse = 30.2,
                    arealNyBebyggelseSpecified = true,
                    aapneArealer = 60.4,
                    aapneArealerSpecified = true,
                    sumAreal = 390.7,
                    sumArealSpecified = true
                },
                arealAnnetBRA = new ArealfordelingType
                {
                    arealEksisterendebebyggelse = 400.3,
                    arealEksisterendebebyggelseSpecified = true,
                    arealNyBebyggelse = 30.2,
                    arealNyBebyggelseSpecified = true,
                    aapneArealer = 60.4,
                    aapneArealerSpecified = true,
                    sumAreal = 490.7,
                    sumArealSpecified = true
                },
                arealIaltBRA = new ArealfordelingType
                {
                    arealEksisterendebebyggelse = 500.3,
                    arealEksisterendebebyggelseSpecified = true,
                    arealNyBebyggelse = 30.2,
                    arealNyBebyggelseSpecified = true,
                    aapneArealer = 60.4,
                    aapneArealerSpecified = true,
                    sumAreal = 590.7,
                    sumArealSpecified = true
                }
            };




            var rammebetingelserBygningsopplysinger = new BygningsopplysningerType
            {
                arealverdier = arealverdierRammebetingelserBygningsopplysinger,
                antallEtasjer = "2",
                antallBruksenheter = "8",
                antallBruksenheterEksisterende = "2",
                antallBruksenheterNye = "1",
                antallBruksenheterBolig = "4",
                antallBruksenheterBoligEksisterende = "3",
                antallBruksenheterBoligNye = "1",
                antallBruksenheterAnnet = "2",
                antallBruksenheterAnnetEksisterende = "1",
                antallBruksenheterAnnetNye = "1"
            };


            var rammebetingelserVannforsyning = new VannforsyningType
            {
                tilknytningstype = new[]
                {
                    new KodeType
                    {
                        kodeverdi = "Annen privat vannforsyning, innlagt vann",
                        kodebeskrivelse = "Annen privat vannforsyning, innlagt vann"
                    }
                },
                beskrivelse = "Felles privat vannforsyning i hele reguleringsområdet",
                krysserVannforsyningAnnensGrunn = true,
                tinglystErklaering = true
            };

            var rammebetingeslerAvloep = new AvloepType
            {
                tilknytningstype = new KodeType
                {
                    kodeverdi = "Offentlig avløpsanlegg",
                    kodebeskrivelse = "Offentlig avløpsanlegg"
                },
                installereVannklosett = true,
                utslippstillatelse = true,
                krysserAvloepAnnensGrunn = true,
                tinglystErklaering = true,
                overvannTerreng = false,
                overvannAvloepssystem = true
            };

            var rammebetingelserKravTilByggegrunn = new KravTilByggegrunnType
            {
                flomutsattOmraade = true,
                f1 = false,
                f2 = false,
                f3 = false,
                skredutsattOmraade = false,
                s1 = false,
                s2 = false,
                s3 = false,
                miljoeforhold = false
            };


            var rammebetingelserLoftinredninger = new LoefteinnretningerType
            {
                erLoefteinnretningIBygning = true,
                planleggesLoefteinnretningIBygning = true,
                planleggesHeis = true,
                planleggesHeisSpecified = true,
                planleggesTrappeheis = false,
                planleggesTrappeheisSpecified = true,
                planleggesRulletrapp = false,
                planleggesRulletrappSpecified = true,
                planleggesLoefteplattform = false,
                planleggesLoefteplattformSpecified = true
            };


            var rammebetingerlserPlassering = new PlasseringType
            {
                konfliktHoeyspentkraftlinje = false,
                konfliktVannOgAvloep = false,
                minsteAvstandNabogrense = 200.4,
                minsteAvstandTilAnnenBygning = 100.2,
                minsteAvstandTilMidtenAvVei = 40.2
            };

            rammetillatelse.rammebetingelser = new RammerType
            {
                adkomst = rammebetingelserAdkomst,
                arealdisponering = rammebetingelserArealdisponering,
                generelleVilkaar = rammebetingerlserGenerelleVilkaar,
                bygningsopplysninger = rammebetingelserBygningsopplysinger,
                vannforsyning = rammebetingelserVannforsyning,
                avloep = rammebetingeslerAvloep,
                kravTilByggegrunn = rammebetingelserKravTilByggegrunn,
                loefteinnretninger = rammebetingelserLoftinredninger,
                plassering = rammebetingerlserPlassering
            };


            // beskrivelseAvTiltak
            rammetillatelse.beskrivelseAvTiltak = new TiltakType[]
            {
                new TiltakType
                {
                    bruk = new FormaalType
                    {
                        anleggstype = new KodeType
                        {
                            kodeverdi = "andre",
                            kodebeskrivelse = "andre"
                        },
                        naeringsgruppe = new KodeType
                        {
                            kodeverdi = "Y",
                            kodebeskrivelse = "Næringsgruppe for annet som ikke er næring"
                        },
                        bygningstype = new[]
                        {
                           new KodeType {
                               kodeverdi = "161",
                               kodebeskrivelse = "Hytter, sommerhus og fritidsbygg"
                           }
                        },
                        tiltaksformaal = new[] {
                            new KodeType
                            {
                                kodeverdi = "Fritidsbolig",
                                kodebeskrivelse = "Fritidsbolig"
                            },
                            new KodeType
                            {
                                kodeverdi = "Annet",
                                kodebeskrivelse = "Annet"
                            },
                        },
                        beskrivPlanlagtFormaal = "Ny fritidsbolig med laftet anneks"
                    },
                    type = new []
                    {
                        new KodeType
                        {
                            kodeverdi = "nyttbyggboligformal",
                            kodebeskrivelse = "Nytt bygg - Boligformål"
                        }
                    },
                    foelgebrev = "Det søkes om bygging av en ny fritidsbolig og et laftet lite anneks. Annekset skal bygges først og benyttes som \"arbeidsbrakke\" mens hovedhytta bygges."
                }
            };


            rammetillatelse.soekesOmDispensasjon = false;
            rammetillatelse.soekesOmDispensasjonSpecified = true;



            // dispensasjon
            rammetillatelse.dispensasjon = new DispensasjonType
            {
                dispensasjonstype = new[]
                {
                    new KodeType
                    {
                        kodeverdi  = "TEK",
                        kodebeskrivelse = "Byggteknisk forskrift med veiledning"
                    }

                },
                begrunnelse = "Begrunnelse for dispensasjon"
            };


            // soekesOmDispensasjon
            rammetillatelse.soekesOmDispensasjon = true;


            // return data model with example data
            return rammetillatelse;

        }
    }
}