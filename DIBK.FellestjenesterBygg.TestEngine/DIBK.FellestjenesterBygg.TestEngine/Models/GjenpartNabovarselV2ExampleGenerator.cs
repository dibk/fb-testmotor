﻿using no.kxml.skjema.dibk.gjenpartnabovarselV2;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class GjenpartNabovarselV2ExampleGenerator
    {
        public NabovarselType NyttGjenpartNabovarselV2()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var gjenpartNabovarselV2 = new NabovarselType();

            var adresseTilHansen = new EnkelAdresseType()
            {
                adresselinje1 = "Storgata 5",
                postnr = "7003",
                poststed = "Trondheim"
            };

            gjenpartNabovarselV2.eiendomByggested = new[]
            {
                new EiendomType()
                {
                    adresse = new EiendommensAdresseType()
                    {
                        adresselinje1 = "Storgata 19",
                        landkode = "NO",
                        postnr = "3800",
                        poststed = "Bø i Telemark"
                    },
                    eiendomsidentifikasjon = new MatrikkelnummerType()
                    {
                        gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                        bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                        festenummer = "1",
                        seksjonsnummer = "1",
                        kommunenummer = rand.GetRandomTestKommuneNummer()
                    }, 
                    bygningsnummer = "0",
                    bolignummer = "H0101",
                    kommunenavn = "Bø i Telemark",
                    eier = "Hans Hansen"
                }
            };

            gjenpartNabovarselV2.beskrivelseAvTiltak = new[]
            {
                new TiltakType()
                {
                    bruk = new FormaalType
                    {
                        tiltaksformaal = new[]
                        {
                            new KodeType()
                            {
                                kodeverdi = "Annet",
                                kodebeskrivelse = "Annet"
                            },
                        },
                        beskrivPlanlagtFormaal = "Annet",
                    },
                    type = new[]
                    {
                        new KodeType()
                        {
                            kodeverdi = "fasade",
                            kodebeskrivelse = "Endring av bygg - utvendig - Fasade"
                        }
                    },
                    foelgebrev = "foelgebrev"
                }
            };


            gjenpartNabovarselV2.rammebetingelser = new RammerType()
            {
                arealdisponering = new ArealdisponeringType()
                {
                    gjeldendePlan = new[]
                    {
                        new PlanType()
                        {
                            plantype = new KodeType()
                            {
                                kodeverdi = "RP",
                                kodebeskrivelse = "Reguleringsplan"
                            },
                            navn = "Gjeldende reg plan for Blakkstadfeltet"
                        },
                        new PlanType()
                        {
                            plantype = new KodeType()
                            {
                                kodeverdi = "BP",
                                kodebeskrivelse = "Bebyggelsesplan"
                            },
                            navn = "Utbygningsvedtak, plan b45"
                        },
                        new PlanType()
                        {
                            plantype = new KodeType()
                            {
                                kodeverdi = "KP",
                                kodebeskrivelse = "Arealdel av kommuneplan"
                            },
                            navn = "Kommuneplan for utbygging av boligpakke nord hvorblant navn er utdypet lengre enn normalt for å se om man mulignes kunne klare å hoppe ned på en ny linje i tekst feltet"
                        }
                    }
                }
            };

            gjenpartNabovarselV2.tiltakshaver = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                navn = "Delilah Kvittingfoss",
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Roald Dalhs gate 4",
                    poststed = "Skien",
                    postnr = "3701"
                },
                telefonnummer = "46576879",
                mobilnummer = "99009900",
                epost = "dh@domene.no"
            };

            gjenpartNabovarselV2.naboeier = new[]
            {
                new NaboGjenboerType()
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = "Privatperson",
                        kodebeskrivelse = "Privatperson"
                    },

                    navn = "Ole Olsen IKKE SVAR",
                    foedselsnummer = "08117000290",

                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Storgata 3",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },

                    gjelderNaboeiendom = new EiendomType()
                    {
                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            gaardsnummer = "109",
                            bruksnummer = "1",
                            kommunenummer = "1601",
                            seksjonsnummer = "1"
                        },
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = "Storgata 3",
                            postnr = "7003",
                            poststed = "Trondheim"
                        }
                    },
                    nabovarselSendtVia = new KodeType()
                    {
                        kodeverdi = "Fellestjenester bygg",
                        kodebeskrivelse = "Fellestjenester bygg"
                    },
                    nabovarselSendt = new System.DateTime(2018, 03, 25),
                    nabovarselSendtSpecified = true,

                    //samtykkeMottatt = true,
                    //samtykkeMottattSpecified = true,
                    //samtykkeMottattDato = new System.DateTime(2018, 03, 26),
                    //samtykkeMottattDatoSpecified = true,

                    //merknadMottatt = false,
                    //merknadMottattSpecified = true,
                    //merknadMottattDato = new System.DateTime(2018, 03, 26),
                    //merknadMottattDatoSpecified = true

                },


                new NaboGjenboerType()
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = "Privatperson",
                        kodebeskrivelse = "Privatperson"
                    },
                    foedselsnummer = "08117000290",
                    navn = "Gordella VanKrupp SAMTYKKE",
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Plagiatore 23",
                        postnr = "3825",
                        poststed = "Lunde"
                    },
                    mobilnummer = "48955777",
                    epost = "tor@arkitektum.no",
                    gjelderNaboeiendom = new EiendomType()
                    {
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = "Moskusgaten 22",
                            postnr = "3825",
                            poststed = "Lunde"
                        },
                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                            bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                            festenummer = "1",
                            seksjonsnummer = "1",
                            kommunenummer = rand.GetRandomTestKommuneNummer()
                        },
                        bolignummer = "H0101",
                        bygningsnummer = "02",
                        kommunenavn = "Nome"
                    },
                    nabovarselSendtVia = new KodeType()
                    {
                        kodeverdi = "Fellestjenester bygg",
                        kodebeskrivelse = "Fellestjenester bygg"
                    },
                    nabovarselSendt = new System.DateTime(2018, 03, 25),
                    nabovarselSendtSpecified = true,

                    samtykkeMottatt = true,
                    samtykkeMottattSpecified = true,
                    samtykkeMottattDato = new System.DateTime(2018, 03, 26),
                    samtykkeMottattDatoSpecified = true,

                    //merknadMottatt = false,
                    //merknadMottattSpecified = true,
                    //merknadMottattDato = new System.DateTime(2018, 03, 26),
                    //merknadMottattDatoSpecified = true
                },



                new NaboGjenboerType()
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = "Privatperson",
                        kodebeskrivelse = "Privatperson"
                    },
                    foedselsnummer = "08117000296",
                    navn = "Ursulla Tentkle MERKNAD",
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Sjomuffins 23",
                        postnr = "3825",
                        poststed = "Lunde"
                    },
                    mobilnummer = "48955999",
                    epost = "tor@arkitektum.no",
                    gjelderNaboeiendom = new EiendomType()
                    {
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = "Moskusgaten 25",
                            postnr = "3825",
                            poststed = "Lunde"
                        },
                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                            bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                            festenummer = "1",
                            seksjonsnummer = "1",
                            kommunenummer = rand.GetRandomTestKommuneNummer()
                        },
                        bolignummer = "H0101",
                        bygningsnummer = "02",
                        kommunenavn = "Nome"
                    },
                    nabovarselSendtVia = new KodeType()
                    {
                        kodeverdi = "Fellestjenester bygg",
                        kodebeskrivelse = "Fellestjenester bygg"
                    },
                    nabovarselSendt = new System.DateTime(2018, 03, 25),
                    nabovarselSendtSpecified = true,

                    //samtykkeMottatt = true,
                    //samtykkeMottattSpecified = true,
                    //samtykkeMottattDato = new System.DateTime(2018, 03, 26),
                    //samtykkeMottattDatoSpecified = true,

                    merknadMottatt = true,
                    merknadMottattSpecified = true,
                    merknadMottattDato = new System.DateTime(2018, 03, 26),
                    merknadMottattDatoSpecified = true
                }
            };

            gjenpartNabovarselV2.kontaktPerson = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                navn = "Ole Olsen",
                foedselsnummer = "08117000290",
                telefonnummer = "111 22 333",
                epost = "tor@arkitektum",
                mobilnummer = "333 44 555",

                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = "Storgata 3",
                    postnr = "7003",
                    poststed = "Trondheim"
                },
            };

            // dispensasjon
            gjenpartNabovarselV2.dispensasjon = new[]
            {
                new DispensasjonType()
                {
                    dispensasjonstype = new KodeType()
                    {
                        kodeverdi = "PLAN",
                        kodebeskrivelse = "Arealplan"
                    },
                    beskrivelse = "Søknad om dispensasjon fra bestemmelsen NN i plan",
                    begrunnelse = "Begrunnelse for dispensasjon"
                }
            };

            gjenpartNabovarselV2.ansvarligSoeker = new PartType()
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "911455307",
                navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "11223344",
                mobilnummer = "99887766",
                epost = "tor@arkitektum.no"
            };

            return gjenpartNabovarselV2;
        }
    }
}
