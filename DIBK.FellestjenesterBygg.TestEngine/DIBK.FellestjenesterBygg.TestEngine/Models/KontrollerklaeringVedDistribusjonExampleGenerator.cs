using no.kxml.skjema.dibk.kontrollerklaeringVedDistribusjon;
using Org.BouncyCastle.Crypto.Engines;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class KontrollerklaeringVedDistribusjonExampleGenerator
    {
        public KontrollerklaeringType NyKontrollerklaeringVedDistribusjon()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();
            var kontrollerklaeringVedDistribusjon = new KontrollerklaeringType()
            {
                fraSluttbrukersystem = "Fellestjenester bygg testmotor",
                hovedinnsendingsnummer = "44556677",
                prosjektnavn = "FTB Panorama",

                eiendomByggested = new[]
                {
                    new EiendomType()
                    {
                        eiendomsidentifikasjon = new MatrikkelnummerType
                        {
                            kommunenummer = rand.GetRandomTestKommuneNummer(),
                            gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                            bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                            festenummer = "0",
                            seksjonsnummer = "0"
                        },
                        adresse = new EiendommensAdresseType
                        {
                            adresselinje1 = "Bøgata 1",
                            postnr = "3800",
                            poststed = "Bø i Telemark",
                            landkode = "NO"
                        },
                        bygningsnummer = "80466985",
                        bolignummer = "H0102", 
                        kommunenavn = "Bø i Telemark"
                    }
                },

                ansvarligSoeker = new PartType()
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    organisasjonsnummer = "911455307",
                    navn = "SÆBØVÅGEN OG LONGYEARBYEN",
                    kontaktperson = new KontaktpersonType()
                    {
                        navn = "Ingvild Testperson Halland",
                        mobilnummer = "99995555",
                        epost = "tor@arkitektum.no"
                    },
                    adresse = new EnkelAdresseType
                    {
                        adresselinje1 = "Bøgata 16",
                        postnr = "3802",
                        poststed = "Bø i Telemark",
                        landkode = "NO"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "tor@arkitektum.no"
                },

                kommunensSaksnummer = new SaksnummerType()
                {
                    saksaar = "2016",
                    sakssekvensnummer = "3456"
                }
            };

            kontrollerklaeringVedDistribusjon.kommunensSaksnummer.saksaar = "2016";
            kontrollerklaeringVedDistribusjon.kommunensSaksnummer.sakssekvensnummer = "231";

            kontrollerklaeringVedDistribusjon.prosjektnr = "99";

            kontrollerklaeringVedDistribusjon.foretak = new PartType()
            {
                partstype = new KodeType
                {
                    kodeverdi = "Foretak",
                    kodebeskrivelse = "Foretak"
                },
                organisasjonsnummer = "910065211",
                navn = "Nordmann Bygg og Anlegg AS",

                adresse = new EnkelAdresseType
                {
                    adresselinje1 = "Bøgata 16",
                    postnr = "3802",
                    poststed = "Bø i Telemark",
                    landkode = "NO"
                },
                telefonnummer = "12345678",
                mobilnummer = "98765432",
                epost = "ola@byggmestern-ola.no",
            };

            kontrollerklaeringVedDistribusjon.ansvarsrett = new AnsvarsomraadeType()
            {
                ansvarsrettErklaert = new System.DateTime(2016, 12, 05),
                ansvarsrettErklaertSpecified = true,
                funksjon = new KodeType()
                {
                    kodeverdi = "UTF",
                    kodebeskrivelse = "Ansvarlig utførelse"
                },
                beskrivelseAvAnsvarsomraadet = "Utføring av cement arbeid",
                kontrollerende = new KontrollerendeType()
                {
                    observerteAvvik = false,
                    observerteAvvikSpecified = true,
                    aapneAvvik = false,
                    aapneAvvikSpecified = true,
                    ingenAvvik = true,
                    ingenAvvikSpecified = true
                },
             
                ansvarsomraadetAvsluttet = true,
                ansvarsomraadetAvsluttetSpecified = true
            };
           
            kontrollerklaeringVedDistribusjon.signatur = new SignaturType
            {
                signaturdato = new System.DateTime(2016, 09, 08),
                signaturdatoSpecified = true,
                signertAv = "",
                signertPaaVegneAv = "",
                signeringssteg = ""
            };

            kontrollerklaeringVedDistribusjon.erTEK10 = false;
            kontrollerklaeringVedDistribusjon.erTEK10Specified = true;

            kontrollerklaeringVedDistribusjon.erklaeringKontroll = true;
            kontrollerklaeringVedDistribusjon.erklaeringKontrollSpecified = true;

            return kontrollerklaeringVedDistribusjon;
        }
    }
}
