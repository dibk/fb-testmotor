﻿using no.kxml.skjema.dibk.nabovarsel;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class SendNabovarselTjeneste
    {

        public string HentEksempelXml()
        {
            NabovarselType nabovarsel = new NabovarselExampleGenerator().NyttNabovarsel();

            var serializer = new System.Xml.Serialization.XmlSerializer(nabovarsel.GetType());
            var stringWriter = new Utf8StringWriter();
            serializer.Serialize(stringWriter, nabovarsel);

            return stringWriter.ToString();
        }
    }
}