﻿using no.kxml.skjema.dibk.igangsettingstillatelseV2;

namespace DIBK.FellestjenesterBygg.TestEngine.Models
{
    public class IgangsettingstillatelseV2ExampleGenerator
    {
        public IgangsettingstillatelseType NyIgangsettingstillatelseType()
        {
            ExampleDataRandomizer rand = new ExampleDataRandomizer();

            var igangsettingstillatelseV2 = new IgangsettingstillatelseType();
            igangsettingstillatelseV2.fraSluttbrukersystem = "Fellestjenester bygg testmotor";
            igangsettingstillatelseV2.kommunensSaksnummer = new SaksnummerType
            {
                saksaar = "2015",
                sakssekvensnummer = "124340"
            };

            igangsettingstillatelseV2.prosjektnavn = "Trondheim Panorama";


            var eiendomByggestedEiendomsidentifikasjon = new MatrikkelnummerType
            {
                kommunenummer = rand.GetRandomTestKommuneNummer(),
                gaardsnummer = rand.GetRandomGaardsOrBruksNummer(),
                bruksnummer = rand.GetRandomGaardsOrBruksNummer(),
                festenummer = "0",
                seksjonsnummer = "0"
            };

            var eiendomByggestedAdresse = new EiendommensAdresseType
            {
                adresselinje1 = "Storgata 3",
                postnr = "7003",
                poststed = "Trondheim"
            };

            var eiendomIdx0 = new EiendomType
            {
                eiendomsidentifikasjon = eiendomByggestedEiendomsidentifikasjon,
                adresse = eiendomByggestedAdresse,
                bygningsnummer = "123456789",
                bolignummer = "H0101",
                kommunenavn = "Trondheim"
            };

            igangsettingstillatelseV2.eiendomByggested = new[]
            {
                eiendomIdx0
            };

            igangsettingstillatelseV2.beroererArbeidsplasser = true;

            igangsettingstillatelseV2.gjelderHeleTiltaket = false;

            var igangsettingstillatelseDelsoeknaderIdx0 = new DelsoeknadIgangsettingType
            {
                kommunensSaksnummer = new SaksnummerType
                {
                    saksaar = "2015",
                    sakssekvensnummer = "124340"
                },
                delAvTiltaket = "Foajé klar for bruk",
                tillatelsedato = new System.DateTime(2016, 02, 11),
                tillatelsedatoSpecified = true,
                kommentar = "IG v2 (fri tekst felt, liste element 0)"
            };
            var igangsettingstillatelseDelsoeknaderIdx1 = new DelsoeknadIgangsettingType
            {
                kommunensSaksnummer = new SaksnummerType
                {
                    saksaar = "2015",
                    sakssekvensnummer = "124340"
                },
                delAvTiltaket = "1. etasje ventilasjonssystem installert",
                tillatelsedato = new System.DateTime(2016, 10, 11),
                tillatelsedatoSpecified = true,
                kommentar = "IG v3 (fri tekst felt, liste element 1)"
            };

            igangsettingstillatelseV2.delsoeknader = new []
            {
                igangsettingstillatelseDelsoeknaderIdx0,
                igangsettingstillatelseDelsoeknaderIdx1,
            };

            igangsettingstillatelseV2.delAvTiltaket = "Denne søknaden gjelder overbygg, påbygg og bod";

            igangsettingstillatelseV2.vilkaar = new[]
            {
                new VilkaarType
                {
                    vilkaarID = "1242",
                    beskrivelse = "Tiltaksplan for forurenset grunn, se eget brev",
                    utsettes = false,
                    utsettesSpecified = true,
                    besvart = true,
                    besvartSpecified = true, 
                    kommentar = ""
                } , 

                new VilkaarType
                {
                    vilkaarID = "123",
                    beskrivelse = "Tinglyst erklæring om midlertidig dispensasjon fra plikte til å opparbeide offentlig vei",
                    utsettes = true,
                    utsettesSpecified = true,
                    besvart = false,
                    besvartSpecified = true,
                    kommentar = "Se eget vedlegg"
                }, 
            };

            igangsettingstillatelseV2.ansvarligSoeker =
                new PartType
                {
                    partstype = new KodeType
                    {
                        kodeverdi = "Foretak",
                        kodebeskrivelse = "Foretak"
                    },
                    organisasjonsnummer = "974702665", // 974702665 DIBK Oslo
                    navn = "Byggmester Bob",
                    kontaktperson = "Hans Hansen",
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = "Lillegata 5",
                        postnr = "7003",
                        poststed = "Trondheim"
                    },
                    telefonnummer = "11223344",
                    mobilnummer = "99887766",
                    epost = "hansen@domene.no"
                };
            
            return igangsettingstillatelseV2;
        } 
    }
}