new Vue({
    el: '#sendinnskjema',
    data: {
        forms: null,
        chosenForm: null,
        messageId: null,
        jsonResponse: null,
        postData: null,
        reportees: null,
        chosenOrganizationNumber: 'my',
        sendAttachments: true,
        sendFormCollection: true,
        editMessageId: null,
        validationResponse: null,
        samples: null,
        selectedSample: null,
        xmlResponse: null,
        standardXml: null,
        userManager: apiUtils.getUserManager(),
        config: apiUtils.getConfig()
    },
    created: async function () {
        const response = await axios.get('/Api/SkjemaData');
        const forms = response.data;
        const formsTemp = [{ "Name": "Velg et skjema", "AvailableService": true }];
        this.forms = formsTemp.concat(forms);

        const searchParams = new URLSearchParams(window.location.search);
        const preSelectedDataFormatID = searchParams.get('DataFormatID');
        const preSelectedServiceEditionCode = searchParams.get('ServiceEditionCode');

        if (preSelectedDataFormatID && preSelectedServiceEditionCode) {
            this.setChosenForm({ DataFormatID: preSelectedDataFormatID, ServiceEditionCode: parseInt(preSelectedServiceEditionCode) });
        }

        this.userManager.startSilentRenew();

        await this.fetchReportees();
    },
    methods: {
        fetchReportees: async function () {
            const url = `${this.config.altinnApiUrl}/api/reportees?showConsentReportees=false`;

            try {
                const response = await apiUtils.fetchData(url);
                this.reportees = response.data._embedded.reportees;
            } catch (error) {
                console.log(error);
                this.reportees = [];
            }
        },

        setChosenForm: function (formInfo) {
            const form = this.forms.find(form => {
                return form.DataFormatID === formInfo.DataFormatID && form.ServiceEditionCode === formInfo.ServiceEditionCode;
            });

            this.selectedSample = null;
            this.validationResponse = null;
            this.chosenForm = form;
            this.standardXml = form.Data;
            this.startSamplesVariant();
            this.getSamplesForForm();
        },

        startSamplesVariant: function () {
            this.samples = [{
                DataFormatId: this.chosenForm.DataFormatID,
                DataFormatVersion: this.chosenForm.DataFormatVersion,
                Description: "Standard xml testdata.",
                Variant: "StandardXml"
            }];
        },

        getSamplesForForm: function () {
            $.ajax({
                type: "GET",
                url: this.config.arbeidsflytApiUrl + "/api/skjemaeksempel/" + this.chosenForm.DataFormatID + "/" + this.chosenForm.DataFormatVersion,
                headers: {
                    "Accept": "application/json"
                },
                success: function (data, status, jqHxr) {
                    this.jsonResponse = "status=" + status + JSON.stringify(data, null, 2);
                    this.samples = this.samples.concat(data);
                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    this.jsonResponse = "status=" + textStatus + jqHxr.responseText + jqHxr.status + jqHxr.statusText + errorThrown;
                }.bind(this)
            });
        },

        updateSelectedSampleVariantId(sampleIndex) {
            this.validationResponse = null;

            if (sampleIndex === '0') {
                this.chosenForm.Data = this.standardXml;
            } else {
                this.selectedSample = this.samples[sampleIndex];
                this.getXmlSampleData();
            }
        },

        getXmlSampleData: function () {
            console.log("sample variantId : " + this.selectedSample.Variant);

            $.ajax({
                type: "GET",
                url: this.config.arbeidsflytApiUrl + "/api/skjemaeksempelvariant/" + this.chosenForm.DataFormatID + "/" + this.chosenForm.DataFormatVersion + "/" + this.selectedSample.Variant, // "http://localhost:54927//api/validate/form",
                headers: {
                    "Accept": "application/json"
                },
                success: function (data, status, jqHxr) {
                    this.jsonResponse = "status=" + status + JSON.stringify(data, null, 2);
                    console.log("url fellestebygg:" + this.config.arbeidsflytApiUrl + "/api/skjemaeksempelvariant/" + this.chosenForm.DataFormatID + "/" + this.chosenForm.DataFormatVersion);
                    this.chosenForm.Data = data.FormData;
                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    this.jsonResponse = "status=" + textStatus + jqHxr.responseText + jqHxr.status + jqHxr.statusText + errorThrown;
                }.bind(this)
            });
        },

        showMetadata: async function (event) {
            event.preventDefault();

            this.validationResponse = '';
            this.jsonResponse = 'Henter metadata...';

            const url = `${this.config.altinnApiUrl}/api/metadata/formtask/${this.chosenForm.ServiceCode}/${this.chosenForm.ServiceEditionCode}`;

            try {
                const response = await apiUtils.fetchData(url);
                this.jsonResponse = `status=${response.status}${JSON.stringify(response.data, null, 2)}`;
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        validateForm: function (event) {
            this.validationResponse = '';
            event.preventDefault();
            
            let validationRequestPayload;
            let contentType;
            
            if (this.chosenForm.ValidationUrl.Type === 'New') {
                validationRequestPayload = JSON.stringify({ formData: this.chosenForm.Data });
                contentType = 'application/json; charset=UTF-8';
            } else {
                validationRequestPayload = {
                    DataFormatId: this.chosenForm.DataFormatID,
                    DataFormatVersion: this.chosenForm.DataFormatVersion,
                    FormData: this.chosenForm.Data,
                    AttachmentTypesAndForms: this.chosenForm.Name
                };

                contentType = "application/x-www-form-urlencoded; charset=UTF-8";
            }

            $.ajax({
                type: "POST",
                url: this.chosenForm.ValidationUrl.Url,
                contentType: contentType,
                headers: {
                    "Accept": "application/json"
                },
                data: validationRequestPayload,
                success: function (data, status, jqHxr) {
                    this.jsonResponse = "status=" + status + JSON.stringify(data, null, 2);
                    this.validationResponse = data;
                    console.log("status=" + status);
                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    this.jsonResponse = "status=" + textStatus + jqHxr.responseText + jqHxr.status + jqHxr.statusText + errorThrown;
                }.bind(this)
            });

        },

        submitAndArchiveForm: async function (event, type) {
            event.preventDefault();

            this.validationResponse = '';
            this.jsonResponse = 'Sender inn skjema...';

            this.postData = {
                Type: 'FormTask',
                ServiceCode: this.chosenForm.ServiceCode,
                ServiceEdition: this.chosenForm.ServiceEditionCode,
                _embedded: {
                    forms: [{
                        Type: 'MainForm',
                        DataFormatId: this.chosenForm.DataFormatID,
                        DataFormatVersion: this.chosenForm.DataFormatVersion,
                        FormData: this.chosenForm.Data
                    }]
                }
            };

            if (this.chosenForm.Vedlegg !== '' && this.sendAttachments) {
                this.postData._embedded.attachments = this.chosenForm.Vedlegg;
            }

            if (!!this.chosenForm.SkjemaSet && this.sendFormCollection) {
                var subForms = this.chosenForm.SkjemaSet;

                for (let i = 0; i < subForms.length; i++) {
                    const subForm = subForms[i];

                    const namelessSubForm = {
                        Type: 'SubForm',
                        DataFormatID: subForm.DataFormatId,
                        DataFormatVersion: subForm.DataFormatVersion,
                        FormData: subForm.XmlData
                    };

                    this.postData._embedded.forms.push(namelessSubForm);
                }
            }

            const url = `${this.config.altinnApiUrl}/api/${this.chosenOrganizationNumber}/messages`;

            try {
                const response = await apiUtils.fetchData(url, {
                    method: type.toString().toUpperCase(),
                    data: this.postData,
                    headers: {
                        'Content-Type': 'application/hal+json'
                    }
                });

                this.jsonResponse = `status=${response.status}\r\n\r\n${response.data}`;
                this.messageId = response.request.getResponseHeader('Location');
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        saveForm: async function (event, type) {
            event.preventDefault();

            this.validationResponse = '';

            this.postData = {
                Type: 'FormTask',
                ServiceCode: this.chosenForm.ServiceCode,
                ServiceEdition: this.chosenForm.ServiceEditionCode,
                _embedded: {
                    forms: [
                        {
                            Type: 'MainForm',
                            DataFormatId: this.chosenForm.DataFormatID,
                            DataFormatVersion: this.chosenForm.DataFormatVersion,
                            FormData: this.chosenForm.Data
                        }
                    ]
                }
            };
            if (this.chosenForm.Vedlegg !== '' && this.sendAttachments) {
                this.postData._embedded.attachments = this.chosenForm.Vedlegg;
            }

            if (!!this.chosenForm.SkjemaSet && this.sendFormCollection) {
                const subForms = this.chosenForm.SkjemaSet;

                for (let i = 0; i < subForms.length; i++) {
                    const subForm = subForms[i];

                    const namelessSubForm = {
                        Type: 'SubForm',
                        DataFormatID: subForm.DataFormatId,
                        DataFormatVersion: subForm.DataFormatVersion,
                        FormData: subForm.XmlData
                    };

                    this.postData._embedded.forms.push(namelessSubForm);
                }
            }

            const url = `${this.config.altinnApiUrl}/api/${this.chosenOrganizationNumber}/messages?complete=false`;

            try {
                const response = await apiUtils.fetchData(url, {
                    method: type.toString().toUpperCase(),
                    data: this.postData,
                    headers: {
                        'Content-Type': 'application/hal+json'
                    }
                });

                this.jsonResponse = `status=${response.status}\r\n\r\n${response.data}`;
                this.messageId = response.request.getResponseHeader('Location');

                await this.showMessage()
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        showMessage: async function (event) {
            if (event) {
                event.preventDefault();
            }

            try {
                const response = await apiUtils.fetchData(this.messageId);

                this.editMessageId = response.data.MessageId;
                this.jsonResponse = `status=${response.status}${JSON.stringify(response.data, null, 2)}`;
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        }
    }
});

