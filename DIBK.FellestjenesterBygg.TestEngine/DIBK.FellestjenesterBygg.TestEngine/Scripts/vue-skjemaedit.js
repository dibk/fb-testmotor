new Vue({
    el: '#skjemaedit',
    data: {
        messageId: idMessage,
        reporteeId: reporteeId,
        messageMetaData: null,
        serviceMetaData: null,
        messageData: null,
        xmlFormData: null,
        formDataFormatId: '1',
        formDataFormatVersion: '1',
        jsonResponse: null,
        postData: null,
        progressbar: 0,
        selectedAddForm: null,
        subforms: null,
        attachments: null,
        messageAttachements: null,
        form: null,
        showTiltaksHaverSignatur: false,
        showAddFormModal: false,
        showAddAttachmentModal: false,
        tiltakshaverSignatur: {
            "Prosjektnavn": null,
            "FraSluttbrukerSystem": null,
            "ServiceEdition": null,
            "ServiceCode": null,
            "Tiltakshaver": null,
            "AnsvarligSoeker": null,
            "AnsvarligSoekerOrganisasjonsnummer": null,
            "Byggested": null,
            "Guid": null,
            "Tiltaktypes": null,
            "TiltakshaverOrgnr": null,
            "TiltakshaverPartstypeKode": null
        },
        emailHaveError: false,
        emailForTiltakshaverSignaturIsSending: false,
        userManager: apiUtils.getUserManager(),
        config: apiUtils.getConfig()
    },

    created: async function () {
        this.userManager.startSilentRenew();

        await this.getMessageData(true);
    },

    computed: {
        canStartTiltakshaverSignatur: function () {
            var hasByggested = Array.isArray(this.tiltakshaverSignatur.Byggested) && this.tiltakshaverSignatur.Byggested.length;
            return hasByggested && this.tiltakshaverSignatur.AnsvarligSoeker && this.tiltakshaverSignatur.Tiltakshaver;
        },

        emailForTiltakshaverSignaturIsSent: function () {
            return this.tiltakshaverSignatur.Guid && !this.emailHaveError;
        },

        notSelectedSubforms: function () {
            const selectedSubforms = this.messageMetaData._links.form;

            return selectedSubforms && selectedSubforms.length
                ? this.subforms.filter(availableSubform => {
                    const availableSubformName = availableSubform.FormName;
                    return !selectedSubforms.find(selectedSubform => {
                        return selectedSubform.name === availableSubformName;
                    });
                })
                : this.subforms;
        },

        exampleEmailSubject: function () {
            const prosjektnavn = this.tiltakshaverSignatur && this.tiltakshaverSignatur.Prosjektnavn ? this.tiltakshaverSignatur.Prosjektnavn : null;
            const byggested = this.tiltakshaverSignatur && this.tiltakshaverSignatur.Byggested && this.tiltakshaverSignatur.Byggested.length ? this.tiltakshaverSignatur.Byggested[0] : null;
            const adresse = byggested && byggested.Adresselinje1 ? byggested.Adresselinje1 : null;
            const poststed = byggested && byggested.Poststed ? byggested.Poststed : null;

            if (prosjektnavn) {
                return `Samtykke til byggeplaner i ${prosjektnavn}${poststed ? ', ' + poststed : ''}`;
            } else if (adresse) {
                return `Samtykke til byggeplaner i ${adresse}${poststed ? ', ' + poststed : ''}`;
            } else if (poststed) {
                return `Samtykke til byggeplaner i ${poststed}`;
            } else {
                return `Samtykke til byggeplaner`;
            }
        },

        exampleEmailByggestedString: function () {
            const byggested = this.tiltakshaverSignatur && this.tiltakshaverSignatur.Byggested && this.tiltakshaverSignatur.Byggested.length ? this.tiltakshaverSignatur.Byggested[0] : null;
            if (byggested) {
                const addressStringPart = byggested.Adresselinje1 ? `${byggested.Adresselinje1}, ` : '';
                const zipCodeStringPart = byggested.Postnr ? `${byggested.Postnr} ` : '';
                const placeStringPart = byggested.Poststed || '';

                return addressStringPart + zipCodeStringPart + placeStringPart;
            } else {
                return null;
            }
        },

        getInTouchString: function () {
            const epost = this.tiltakshaverSignatur && this.tiltakshaverSignatur.AnsvarligSokerEpost && this.tiltakshaverSignatur.AnsvarligSokerEpost.length ? this.tiltakshaverSignatur.AnsvarligSokerEpost : null;
            const telefon = this.tiltakshaverSignatur && this.tiltakshaverSignatur.AnsvarligSokerTelefon && this.tiltakshaverSignatur.AnsvarligSokerTelefon.length ? this.tiltakshaverSignatur.AnsvarligSokerTelefon : null;

            if (epost && telefon) {
                return `Har du noen spørsmål, kan du gjerne ta kontakt med oss på e-post ${epost} eller telefon ${telefon}.`;
            } else if (epost) {
                return `Har du noen spørsmål, kan du gjerne ta kontakt med oss på e-post ${epost}.`;
            } else if (telefon) {
                return `Har du noen spørsmål, kan du gjerne ta kontakt med oss på telefon ${telefon}.`;
            } else {
                return '';
            }
        }
    },
    methods: {
        populateTiltakshaverSignaturWithApiData: function (apiData) {
            if (apiData && Object.keys(apiData).length) {
                Object.keys(apiData).forEach(apiDataKey => {
                    this.tiltakshaverSignatur[apiDataKey] = apiData[apiDataKey];
                });
            }
        },

        convertFormDataXmlToJson: function (xml, DataFormatId, DataFormatVersion) {
            $.ajax({
                type: "POST",
                url: this.config.arbeidsflytApiUrl + "/api/internal/convertXmlMainFormToTiltakshaverSamtykke",
                headers: {
                    "Content-Type": "application/json"
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa("apiTest" + ":" + "apiTest"));
                },
                crossDomain: true,
                data: JSON.stringify({
                    "DataFormatId": DataFormatId,
                    "DataFormatVersion": DataFormatVersion,
                    "XmlData": xml
                }),
                success: function (data, status, jqHxr) {
                    this.populateTiltakshaverSignaturWithApiData(data);

                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    this.jsonResponse = "status=" + textStatus + jqHxr.responseText + " " + jqHxr.status + " " + jqHxr.statusText + " " + errorThrown;
                }.bind(this)
            });
        },

        getFormDataXml: async function (url) {
            try {
                const formDataResponse = await apiUtils.fetchData(url);
                const href = formDataResponse.data._links?.formdata?.href;

                if (!href) {
                    return;
                }

                const subforms = this.serviceMetaData && this.serviceMetaData.FormsMetaData
                    ? this.serviceMetaData.FormsMetaData
                    : null;

                if (!this.shouldShowTiltakshaverSignaturSection(subforms)) {
                    return;
                }

                try {
                    const response = await apiUtils.fetchData(href, {
                        headers: {
                            'Accept': 'application/xml'
                        }
                    });

                    const serializedXml = response.data;
                    const mainForm = subforms ? this.getMainFormFromSubFormsArray(subforms) : null;
                    const dataFormatID = mainForm.DataFormatID;
                    const dataFormatVersion = mainForm.DataFormatVersion;

                    this.convertFormDataXmlToJson(serializedXml, dataFormatID, dataFormatVersion);
                } catch (error) {
                    this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
                }
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        getMessageData: async function (withServiceMetadata) {
            const url = `${this.config.altinnApiUrl}/api/${this.reporteeId}/Messages/${this.messageId}?language=1044`;

            try {
                const response = await apiUtils.fetchData(url);
                const data = response.data;

                this.jsonResponse = `status=${response.status}${JSON.stringify(data, null, 2)}`;
                this.messageData = JSON.stringify(data);
                this.messageMetaData = data;

                await this.getAttachmentsInfo();

                if (withServiceMetadata) {
                    await this.setServiceMetadata();
                }

                if (data._links?.form) {
                    const href = data._links.form.at(0)?.href;

                    if (href) {
                        await this.getFormDataXml(href)
                    }
                }
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        uploadFile: async function () {

            const filename = encodeURIComponent(fileToUpload.files[0].name);

            const url = `${this.config.altinnApiUrl}/api/${this.reporteeId}/messages/${this.messageId}/attachments/streamedattachment?filename=${filename}&attachmenttype=${attachmenttype.value}`;

            try {
                const response = await apiUtils.fetchData(url, {
                    method: 'POST',
                    data: fileToUpload.files[0],
                    onUploadProgress: progressEvent => {
                        this.progressbar = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                    },
                    headers: {
                        'Content-Type': 'application/octet-stream'
                    }
                });

                this.jsonResponse = `status=${response.status}`;
                await this.getMessageData(false);
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            } finally {
                this.showAddAttachmentModal = false;
            }
        },

        deleteAttachment: async function (url) {
            try {
                const response = await apiUtils.fetchData(url, {
                    method: 'DELETE'
                });

                this.jsonResponse = `status=${response.status} ${response.statusText}`;
                await this.getMessageData(false);
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        addForm: async function () {
            const url = `${this.config.altinnApiUrl}/api/${this.reporteeId}/messages/${this.messageId}/forms`;

            try {
                const response = await apiUtils.fetchData(url, {
                    method: 'POST',
                    data: {
                        Type: 'SubForm',
                        DataFormatId: this.selectedAddForm.DataFormatID,
                        DataFormatVersion: this.selectedAddForm.DataFormatVersion
                    }
                });

                this.jsonResponse = `status=${response.status} ${response.statusText}`;

                await this.getMessageData(false);
                await this.addSampleXmlToForm();                
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        addSampleXmlToForm: async function () {
            const dataFormatId = this.selectedAddForm.DataFormatID;
            const dataFormatVersion = this.selectedAddForm.DataFormatVersion;
            const formLinks = this.messageMetaData._links.form;
            const formUrl = formLinks[formLinks.length - 1].href;

            const sampleXmlData = await this.getSampleXmlData(dataFormatId, dataFormatVersion);

            if (sampleXmlData === null) {
                return;
            }

            await apiUtils.fetchData(formUrl, {
                method: 'PUT',
                data: {
                    Type: 'SubForm',
                    DataFormatId: dataFormatId,
                    DataFormatVersion: dataFormatVersion,
                    FormData: sampleXmlData.Data
                }
            });
        },

        getSampleXmlData: async function (dataFormatId, dataFormatVersion) {
            const url = `/api/skjemadata/sample/${dataFormatId}/${dataFormatVersion}`;

            try {
                const response = await axios.get(url);
                return response.data;
            } catch (error) {
                console.error(error);
                return null;
            }
        },

        handleAddFormClick: async function () {
            this.showAddFormModal = false;
            await this.addForm();
            this.selectedAddForm = null;            
        },

        deleteForm: async function (url) {
            try {
                const response = await apiUtils.fetchData(url, {
                    method: 'DELETE'
                });

                this.jsonResponse = `status=${response.status} ${response.statusText}`;
                await this.getMessageData(false);
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        getMainFormFromSubFormsArray(subforms) {
            return subforms?.find(form => form.FormType === 'MainForm');
        },

        shouldShowTiltakshaverSignaturSection: function (subforms) {
            const mainForm = this.getMainFormFromSubFormsArray(subforms);

            if (mainForm) {
                const isRammetillatelse = mainForm.DataFormatID === '5693' && mainForm.DataFormatVersion === 42425;
                const isEttTrinnV3 = mainForm.DataFormatID === '6742' && mainForm.DataFormatVersion === 45753;
                const isRammetillatelseV3 = mainForm.DataFormatID === '6741' && mainForm.DataFormatVersion === 45752;

                return isRammetillatelse || isEttTrinnV3 || isRammetillatelseV3;
            } else {
                return false;
            }
        },

        saveMessage: async function (event, isSubmit) {
            event.preventDefault();

            this.postData = {
                Type: 'FormTask',
                ServiceCode: this.messageMetaData && this.messageMetaData.ServiceCode ? this.messageMetaData.ServiceCode : null,
                ServiceEdition: this.messageMetaData && this.messageMetaData.ServiceEdition ? this.messageMetaData.ServiceEdition : null
            };

            const url = `${this.config.altinnApiUrl}/api/${this.reporteeId}/messages/${this.messageId}?language=1044&complete=${isSubmit}&sign=${isSubmit}`;

            try {
                const response = await apiUtils.fetchData(url, {
                    method: 'PUT',
                    data: this.postData
                });

                if (isSubmit) {
                    this.messageId = response.request.getResponseHeader('Location');
                } else {
                    await this.getMessageData(false);
                }

                this.jsonResponse = `status=${response.status}${response.data}`;
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
                console.log(this.jsonResponse);
            }
        },

        validateForm: async function () {
            $.ajax({
                type: "POST",
                url: this.config.arbeidsflytApiUrl + "/api/validatev2/form",
                headers: {
                    "Accept": "application/json"
                },
                data: {
                    "DataFormatId": this.formData.DataFormatId,
                    "DataFormatVersion": this.formData.DataFormatVersion,
                    "FormName": "string",
                    "FormData": this.formXmlData,
                    "SubForms": [
                        {
                            "FormName": "string",
                            "FormData": "string",
                            "DataFormatId": 0,
                            "DataFormatVersion": 0
                        }
                    ],
                    "Attachments": [
                        {
                            "Name": "string",
                            "Filename": "string",
                            "FileSize": 0
                        }
                    ]
                },
                success: function (data, status, jqHxr) {
                    this.jsonResponse = "status=" + status + JSON.stringify(data, null, 2);


                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    this.jsonResponse = "status=" + textStatus + jqHxr.responseText + " " + jqHxr.status + " " + jqHxr.statusText + " " + errorThrown;
                }.bind(this)
            });
        },

        setServiceMetadata: async function () {
            const serviceCode = this.messageMetaData && this.messageMetaData.ServiceCode ? this.messageMetaData.ServiceCode : '';
            const serviceEdition = this.messageMetaData && this.messageMetaData.ServiceEdition ? this.messageMetaData.ServiceEdition : '';
            const url = `${this.config.altinnApiUrl}/api/metadata/formtask/${serviceCode}/${serviceEdition}`;

            try {
                const response = await apiUtils.fetchData(url);

                this.serviceMetaData = response.data;
                this.tiltakshaverSignatur.ServiceCode = response.data.ServiceCode ? response.data.ServiceCode : null;
                this.tiltakshaverSignatur.ServiceEdition = response.data.ServiceEditionCode ? response.data.ServiceEditionCode : null;
                this.tiltakshaverSignatur.FraSluttbrukerSystem = 'TestMotor';

                //TODO add first element and disable Link Button for first element
                if (this.serviceMetaData.FormsMetaData !== null) {
                    this.subforms = this.serviceMetaData.FormsMetaData.sort((a, b) => helpers.orderBy(a, b, 'FormName'));
                }

                //TODO add first element to attachemnts and disable Link Button for first element
                if (this.serviceMetaData.AttachmentRules !== null) {
                    this.serviceMetaData.AttachmentRules = this.serviceMetaData.AttachmentRules.sort((a, b) => helpers.orderBy(a, b, 'AttachmentTypeNameLanguage'));

                    const attachmentRules = [{
                        "AttachmentTypeNameLanguage": "Velg et vedlegg"
                    }];

                    this.attachments = attachmentRules.concat(this.serviceMetaData.AttachmentRules);
                }
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },
        getAttachmentsInfo: async function () {
            const url = `${this.config.altinnApiUrl}/api/${this.reporteeId}/messages/${this.messageId}/attachments`;

            try {
                const response = await apiUtils.fetchData(url);
                this.messageAttachements = response.data._embedded.attachments;
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        handleshowTiltaksHaverSignaturClick: function () {
            this.showTiltaksHaverSignatur = !this.showTiltaksHaverSignatur;
        },

        startTiltakshaverSignatur: function () {
            this.emailForTiltakshaverSignaturIsSending = true;

            $.ajax({
                type: "POST",
                url: this.config.arbeidsflytApiUrl + "/api/signatur/samtykketiltakshaver/ny",
                headers: {
                    "Accept": "application/json"
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa("apiTest" + ":" + "apiTest"));
                },
                data: this.tiltakshaverSignatur,
                success: function (data, status, jqHxr) {
                    this.emailForTiltakshaverSignaturIsSending = false;
                    this.tiltakshaverSignatur.Guid = data;

                    if (this.tiltakshaverSignatur.Guid != null) {
                        //Upload attachements
                        for (i = 0; i < this.messageAttachements.length; i++) {
                            this.downloadAttachmentFromUrl(this.messageAttachements[i]);
                        }

                        //Upload Søknad
                        var vedlegg = {
                            "FileName": this.serviceMetaData.ServiceName + ".pdf",
                            "Name": this.serviceMetaData.ServiceName + ".pdf",
                            "AttachmentType": "Soknad",
                            "AttachmentTypeLocalized": this.messageMetaData.Subject,
                            "_links": {
                                "self": {
                                    "href": this.messageMetaData._links.print.href
                                }
                            }
                        };
                        this.downloadAttachmentFromUrl(vedlegg);
                        this.emailHaveError = false;
                    }

                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    this.jsonResponse =
                        "status=" +
                        textStatus +
                        jqHxr.responseText +
                        " " +
                        jqHxr.status +
                        " " +
                        jqHxr.statusText +
                        " " +
                        errorThrown;
                    console.log("Error sending Tiltakshaver Signatur");
                    this.emailHaveError = true;
                    this.emailForTiltakshaverSignaturIsSending = false;
                }.bind(this)
            });
        },

        downloadAttachmentFromUrl: async function (vedlegg) {
            const url = vedlegg._links.self.href;

            try {
                const response = await apiUtils.fetchData(url, { responseType: 'blob' });
                await this.uploadAttachment(vedlegg, response.data);
            } catch {
                console.error(`Could not download attachment from ${url}`);
            }
        },

        uploadAttachment: async function (vedlegg, blob) {
            const encodedVedleggFilnavn = encodeURIComponent(vedlegg.FileName);
            const url = `${this.config.arbeidsflytApiUrl}/api/signatur/samtykketiltakshaver/streamvedlegg/${this.tiltakshaverSignatur.Guid}/${vedlegg.AttachmentType}/${encodedVedleggFilnavn}`;

            const formData = new FormData();
            formData.append("file-0", blob);

            try {
                await axios.post(url, formData, {
                    headers: {
                        'Authorization': `Basic ${window.btoa('apiTest:apiTest')}`,
                        'Content-Type': 'multipart/form-data'
                    }
                });
            } catch {
                this.emailHaveError = true;
                console.error(`Could not upload attachment to ${url}`);
            }
        }
    }
});

