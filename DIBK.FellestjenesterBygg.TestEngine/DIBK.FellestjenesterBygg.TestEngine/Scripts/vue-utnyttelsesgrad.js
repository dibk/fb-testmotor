﻿new Vue({
    el: '#utnyttelsesgrad',
    data: {
        apiUrl: 'https://dibk-utnyttelsesgrad-test.azurewebsites.net/api/Utnyttelsesgrad/',
        beregninger: [
            { type: 'BYA', regplan: 'Reguleringsplaner 2007->', tekniskInfo: 'Teknisk forskrift nr. 96 (2007), Byggteknisk forskrift nr. 489 (2010); NS3940:2007, NS3940:2012', visningsKode: 1, apiAction: 'BYA' },
            { type: 'ProsentBYA', regplan: 'Reguleringsplaner 1997->', tekniskInfo: 'Teknisk forskrift nr. 33 (1997), Teknisk forskrift nr. 96 (2007), Byggteknisk forskrift nr. 489 (2010); NS3940 2. utg, NS3940:2007, NS3940:2012', visningsKode: 0, apiAction: 'ProsentBYA' },
            { type: 'BRA', regplan: 'Reguleringsplaner 2007->', tekniskInfo: 'Teknisk forskrift nr. 96 (2007), Byggteknisk forskrift nr. 489 (2010); NS3940:2007, NS3940:2012', visningsKode: 1, apiAction: 'BRA' },
            { type: 'ProsentBRA', regplan: 'Reguleringsplaner 2007->', tekniskInfo: 'Teknisk forskrift nr. 96 (2007), Byggteknisk forskrift nr. 489 (2010); NS3940:2007, NS3940:2012', visningsKode: 0, apiAction: 'ProsentBRA' },
            { type: 'Tillatt Bruksareal T-BRA', regplan: 'Reguleringsplaner 1997–2007', tekniskInfo: 'Teknisk forskrift nr. 33 (1997); NS3940 2. utg', visningsKode: 1, apiAction: 'TillattBruksarealTBRA' },
            { type: 'ProsentTU', regplan: 'Reguleringsplaner 1997–2007', tekniskInfo: 'Teknisk forskrift nr. 33 (1997); NS3940 2. utg', visningsKode: 0, apiAction: 'ProsentTU' },
            { type: 'Tillatt Bebygd Areal', regplan: 'Reguleringsplaner 1987–1997', tekniskInfo: 'Byggeforskrift 1987; NS3940 2. utg', visningsKode: 0, apiAction: 'TillattBebygdAreal' },
            { type: 'Tillatt Bruksareal', regplan: 'Reguleringsplaner 1987–1997', tekniskInfo: 'Byggeforskrift 1987; NS3940 2. utg', visningsKode: 1, apiAction: 'TillattBruksareal' },
            { type: 'Tillatt Tomte Utnyttelse', regplan: 'Reguleringsplaner 1987–1997', tekniskInfo: 'Byggeforskrift 1987; NS3940 2. utg', visningsKode: 0, apiAction: 'TillattTomteUtnyttelse' },
            { type: 'Golvareal/Grunnareal', regplan: 'Reguleringsplaner 1969–1987', tekniskInfo: 'Byggeforskrift 1969, 1985; NS 848, 3940 1. utg, 3940 2. utg', visningsKode: 2, apiAction: 'GolvarealDeltPaaGrunnareal' }
        ],
        valgtBeregning: '',
        resultat: '',
        httpStatus: '',
        httpErrorData: {responseText: '', status: '', statusText: ''},
        inputData: [
            {
                visningsKode: 0,
                data: { tomtearealByggeomraade: '', arealBebyggelseEksisterende: '', arealBebyggelseSomSkalRives: '', arealBebyggelseNytt: '', antallParkeringsplasserTerreng: '', kravKvmPrParkeringsplass: '' }
            },
            {
                visningsKode: 1,
                data: { arealBebyggelseEksisterende: '', arealBebyggelseSomSkalRives: '', arealBebyggelseNytt: '', antallParkeringsplasserTerreng: '', kravKvmPrParkeringsplass: '' }
            },
            {
                visningsKode: 2,
                data: { tomtearealByggeomraade: '', arealBebyggelseEksisterende: '', arealBebyggelseSomSkalRives: '', arealBebyggelseNytt: '' }
            }
        ]
    },
    methods: {
        postJson: function () {
            var jsonData = $(this.inputData[this.valgtBeregning.visningsKode].data);
            $.ajax({
                type: "POST",
                url: this.apiUrl + this.valgtBeregning.apiAction,
                // The key needs to match your method's input parameter (case-sensitive).
                data: JSON.stringify(jsonData[0]),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    this.httpStatus = textStatus;
                    this.resultat = data;
                    console.log(textStatus);
                }.bind(this),
                error: function (jqXHR, textStatus, errorThrown) {
                    this.httpStatus = textStatus;
                    this.httpErrorData.responseText = jqXHR.responseText;
                    this.httpErrorData.status = jqXHR.status;
                    this.httpErrorData.statusText = jqXHR.statusText;
                    console.log("Error issuing API call to utnyttelsesgrad"); console.log(textStatus); console.log(jqXHR);
                }
            });
        }
    }
})


