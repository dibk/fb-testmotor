﻿new Vue({
    el: '#formedit',
    data: {
        formurl: formurl,
        messageId: messageId,
        reporteeId: reporteeId,
        formData: null,
        formXmlData: null,
        messageAttachments: null,
        messageForms: null,
        jsonResponse: null,
        samples: null,
        selectedSample: null,
        xmlData: null,
        userManager: apiUtils.getUserManager(),
        config: apiUtils.getConfig()
    },
    computed: {
        formattedXmlData: function () {
            return helpers.formatXml(this.formXmlData);
        }
    },
    created: async function () {
        this.userManager.startSilentRenew();

        try {
            const response = await apiUtils.fetchData(this.formurl, {
                headers: {
                    'Access-Control-Allow-Origin': location.origin,
                }
            });

            this.jsonResponse = `status=${response.status}${JSON.stringify(response.data, null, 2)}`;
            this.formData = response.data;

            await Promise.all([
                this.getFormXMLdata(this.formurl),
                this.getMessageAttachments(),
                this.getMessageForms()
            ]);

            this.startSamplesVariant();
        } catch (error) {
            this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
        }
    },
    methods: {
        startSamplesVariant: function () {
            this.samples = [{
                "Variant": "XmlFraAltinn",
                "Description": "XML fra Altinn"
            },
            {
                "Variant": "Eksempeldata",
                "Description": "XML fra testmotor"
            }];
        },

        getSamplesForForm: function () {
            $.ajax({
                type: "GET",
                url: this.config.arbeidsflytApiUrl + "/api/skjemaeksempel/" + this.chosenForm.DataFormatID + "/" + this.chosenForm.DataFormatVersion,
                headers: {
                    "Accept": "application/json"
                },
                success: function (data, status, jqHxr) {
                    this.jsonResponse = "status=" + status + JSON.stringify(data, null, 2);
                    this.samples.push(data);
                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    this.jsonResponse = "status=" + textStatus + jqHxr.responseText + jqHxr.status + jqHxr.statusText + errorThrown;
                }.bind(this)
            });
        },

        setXmlSampleData: async function(sampleIndex) {
            this.jsonResponse = null;
            this.selectedSample = this.samples[sampleIndex];

            if (sampleIndex === '0') {
                this.formXmlData = helpers.formatXml(this.xmlData);
            } else if (sampleIndex === '1') {
                this.formXmlData = helpers.formatXml(await this.getTestEngineXmlSampleData());
            } else {
                this.getXmlSampleData();
            }
        },

        getTestEngineXmlSampleData: async function () {
            const url = `/api/skjemadata/sample/${this.formData.DataFormatId}/${this.formData.DataFormatVersion}`;

            try {
                const response = await axios.get(url);
                return response.data?.Data || null;
            } catch (error) {
                console.error(error);
            }
        },

        getXmlSampleData: function () {
            $.ajax({
                type: "GET",
                url: this.config.arbeidsflytApiUrl + "/api/skjemaeksempelvariant/" + this.chosenForm.DataFormatID + "/" + this.chosenForm.DataFormatVersion + "/" + this.selectedSample.Variant, // "http://localhost:54927//api/validate/form",
                headers: {
                    "Accept": "application/json"
                },
                success: function (data, status, jqHxr) {
                    this.jsonResponse = "status=" + status + JSON.stringify(data, null, 2);                    
                    this.chosenForm.Data = data.FormData;
                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    this.jsonResponse = "status=" + textStatus + jqHxr.responseText + jqHxr.status + jqHxr.statusText + errorThrown;
                }.bind(this)
            });
        },

        getFormXMLdata: async function (url) {
            try {
                const response = await apiUtils.fetchData(url + '/formdata', {
                    headers: {
                        'Accept': 'application/xml',
                        'Access-Control-Allow-Origin': location.origin
                    }
                });

                this.jsonResponse = `status=${response.status}\r\n\r\n${helpers.formatXml(response.data)}`;
                this.formXmlData = helpers.formatXml(response.data);
                this.xmlData = response.data;
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        getMessageAttachments: async function () {
            const url = `${this.config.altinnApiUrl}/api/${this.reporteeId}/messages/${this.messageId}/attachments`;

            try {
                const response = await apiUtils.fetchData(url);
                this.messageAttachments = response.data;
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        getMessageForms: async function () {
            const url = `${this.config.altinnApiUrl}/api/${this.reporteeId}/messages/${this.messageId}/forms`;

            try {
                const response = await apiUtils.fetchData(url);
                this.messageForms = response.data;
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        saveForm: async function () {
            try {
                const response = await apiUtils.fetchData(this.formurl, {
                    method: 'PUT',
                    data: {
                        Type: 'SubForm',
                        DataFormatId: this.formData.DataFormatId,
                        DataFormatVersion: this.formData.DataFormatVersion,
                        FormData: this.formXmlData
                    }
                });

                this.jsonResponse = `status=${response.status}${JSON.stringify(response.data, null, 2)}`;
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        validateForm: async function () {
            const url = `/api/skjemadata/validationUrl/${this.formData.DataFormatId}`;
            const response = await axios.get(url);
            const validationUrl = response.data;

            this.validationResponse = '';
            let validationRequestPayload;
            let contentType;

            if (validationUrl.Type === 'New') {
                validationRequestPayload = JSON.stringify({ formData: this.formXmlData });
                contentType = 'application/json; charset=UTF-8';
            } else {
                validationRequestPayload = {
                    DataFormatId: this.formData.DataFormatId,
                    DataFormatVersion: this.formData.DataFormatVersion,
                    FormData: this.formXmlData,
                    AttachmentTypesAndForms: this.getAttachmentTypes()
                };

                contentType = "application/x-www-form-urlencoded; charset=UTF-8";
            }

            $.ajax({
                type: "POST",
                url: validationUrl.Url,
                contentType: contentType,
                headers: {
                    "Accept": "application/json"
                },
                data: validationRequestPayload,
                success: function (data, status, jqHxr) {
                    this.jsonResponse = "status=" + status + JSON.stringify(data, null, 2);
                    this.validationResponse = data;
                    console.log("status=" + status);
                }.bind(this),
                error: function (jqHxr, textStatus, errorThrown) {
                    this.jsonResponse = "status=" + textStatus + jqHxr.responseText + jqHxr.status + jqHxr.statusText + errorThrown;
                }.bind(this)
            });
        },

        getAttachmentTypes: function () {
            var attachmentTypes = [];

            for (var i = 0, len = this.messageForms._embedded.forms.length; i < len; i++) {
                attachmentTypes.push(this.messageForms._embedded.forms[i].Name);
            }
            for (var i = 0, len = this.messageAttachments._embedded.attachments.length; i < len; i++) {
                attachmentTypes.push(this.messageAttachments._embedded.attachments[i].AttachmentType);
            }

            return attachmentTypes;
        }
    }
});

