﻿new Vue({
    el: '#main-menu',
    data: {
        isLoggedIn: false,
        userManager: apiUtils.getUserManager(),
        config: apiUtils.getConfig()
    },
    created: async function () {
        await this.loginCheck();

        await apiUtils.continueLoginProcess();
    },
    methods: {
        logout: async function (event) {
            event.preventDefault();
            await this.userManager.signoutRedirect();
        },

        loginCheck: async function () {
            this.isLoggedIn = await apiUtils.isLoggedIn();

            setTimeout(async () => await this.loginCheck(), 5000);
        },

        openMessageBox: async function (event) {
            event.preventDefault();
            const redirectPath = `${this.config.fellesbyggApiUrl}/meldingsboks`;

            await apiUtils.login(redirectPath);
        }
    }
});