new Vue({
    el: '#list-skjemaer',
    data: {
        forms: null,
        userManager: apiUtils.getUserManager(),
        isLoggedIn: false
    },
    created: async function () {
        await this.loginCheck();

        try {
            const response = await axios.get('/api/SkjemaData?data=false&attachments=false&formCollection=false');
            this.forms = response.data;
        } catch (error) {
            console.error(error);
        }
    },
    methods: {
        login: async function (event, redirectPath) {
            event.preventDefault();

            await apiUtils.login(redirectPath);
        },

        loginCheck: async function () {
            this.isLoggedIn = await apiUtils.isLoggedIn();

            setTimeout(async () => await this.loginCheck(), 5000);
        }
    }
})