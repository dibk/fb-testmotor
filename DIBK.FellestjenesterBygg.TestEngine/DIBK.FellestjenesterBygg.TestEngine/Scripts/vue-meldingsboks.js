﻿new Vue({
    el: '#meldingsboks',
    data: {
        forms: null,
        postData: '',
        httpStatus: 'tbd',
        jsonResponse: {
            responseText: '',
            status: '',
            statusText: ''
        },
        delegateAllowed: [{
            ServiceCode: 4419,
            status: 'Utfylling'
        }],
        editAllowed: 'Utfylling',
        temp: '',
        selectedMessage: '',
        idSelectOptions: [{
            ToEntity: 'Privat Person',
            IdType: 'Fødselsnummer',
            FieldText: 'skriv inn fødselsnummer',
            NameType: 'Mottaker etternavn'
        },
        {
            ToEntity: 'Bedrift',
            IdType: 'Organisasjonsnummer',
            FieldText: 'skriv inn orgnummer',
            NameType: 'Bedrift navn'
        }],
        idSelectedPrivatPerson: 0,
        idSelectedCompany: 1,
        chosenIdType: '',
        chosenIdNumber: '',
        chosenName: '',
        chosenEmail: '',
        reportees: '',
        chosenReportee: 'my',
        userManager: apiUtils.getUserManager(),
        config: apiUtils.getConfig()
    },
    computed: {
        canShowMessages: function () {
            return !!this.forms?._embedded.messages;
        }
    },
    created: async function () {
        this.userManager.startSilentRenew();

        await Promise.resolve([
            this.showMessages(),
            this.fetchReportees()
        ]);
    },
    methods: {
        showMessages: async function () {
            const url = `${this.config.altinnApiUrl}/api/${this.chosenReportee}/messages`;

            try {
                const response = await apiUtils.fetchData(url);
                this.forms = response.data;
            } catch (error) {
                console.error(error.request.status, error.request.statusText, error.request.responseText);
            }
        },

        fetchReportees: async function () {
            const url = `${this.config.altinnApiUrl}/api/reportees?showConsentReportees=false`;

            try {
                const response = await apiUtils.fetchData(url);
                this.reportees = response.data._embedded.reportees;
            } catch (error) {
                console.error(error.request.status, error.request.statusText, error.request.responseText);
            }
        },

        isDelegatable: function (message) {
            return this.delegateAllowed.some(delegate => delegate.ServiceCode === message.ServiceCode && delegate.status === message.Status) ? 1 : 0;
        },

        isEditable: function (message) {
            return message.Status === this.editAllowed ? 1 : 0;
        },

        setSelectedMessage: function (message) {
            this.selectedMessage = message;
            this.httpStatus = 'tbd';
            this.chosenIdType = '';
            this.chosenIdNumber = '';
            this.chosenName = '';
            this.chosenEmail = '';
        },

        delegateRight: async function (idSelectionObj, messageObj, idnum, name, email) {
            if (idSelectionObj.IdType == 'Fødselsnummer') {
                this.postData = {
                    "Type": email,
                    "SocialSecurityNumber": idnum,
                    "LastName": name,
                    "_embedded": {
                        "Rights": [
                            {
                                "MessageID": messageObj.MessageId,
                                "ServiceCode": messageObj.ServiceCode,
                                "ServiceEditionCode": messageObj.ServiceEdition,
                                "Action": "Read"
                            },
                            {
                                "MessageID": messageObj.MessageId,
                                "ServiceCode": messageObj.ServiceCode,
                                "ServiceEditionCode": messageObj.ServiceEdition,
                                "Action": "Write"
                            }
                            ,
                            {
                                "MessageID": messageObj.MessageId,
                                "ServiceCode": messageObj.ServiceCode,
                                "ServiceEditionCode": messageObj.ServiceEdition,
                                "Action": "Sign"
                            }
                        ]
                    }
                };
            }

            if (idSelectionObj.IdType == 'Organisasjonsnummer') {
                this.postData = {
                    "Type": email,
                    "OrganizationNumber": idnum,
                    "Name": name,
                    "_embedded": {
                        "Rights": [
                            {
                                "MessageID": messageObj.MessageId,
                                "ServiceCode": messageObj.ServiceCode,
                                "ServiceEditionCode": messageObj.ServiceEdition,
                                "Action": "Read"
                            },
                            {
                                "MessageID": messageObj.MessageId,
                                "ServiceCode": messageObj.ServiceCode,
                                "ServiceEditionCode": messageObj.ServiceEdition,
                                "Action": "Write"
                            }
                            ,
                            {
                                "MessageID": messageObj.MessageId,
                                "ServiceCode": messageObj.ServiceCode,
                                "ServiceEditionCode": messageObj.ServiceEdition,
                                "Action": "Sign"
                            }
                        ]
                    }
                };
            }

            const url = `${this.config.altinnApiUrl}/api/my/authorization/delegations`;

            try {
                const response = await apiUtils.fetchData(url, {
                    method: 'POST',
                    data: this.postData
                });

                this.httpStatus = response.statusText;
                this.jsonResponse.responseText = response.request.responseText;
                this.jsonResponse.status = response.status;
                this.jsonResponse.statusText = response.statusText;
            } catch (error) {
                this.httpStatus = error.request.statusText;
                this.jsonResponse.responseText = error.request.responseText;
                this.jsonResponse.status = error.request.status;
                this.jsonResponse.statusText = error.request.statusText;
            }
        }
    }
})
