const helpers = (() => {
    function formatXml(xml) {
        if (xml === null) {
            return null;
        }

        const pattern = /(>)(<)(\/*)/g;
        xml = xml.replace(pattern, '$1\r\n$2$3');
        const nodes = xml.split('\r\n');

        let formatted = '';
        let pad = 0;

        nodes.forEach(node => {
            let indent = 0;

            if (node.match(/.+<\/\w[^>]*>$/)) {
                indent = 0;
            } else if (node.match(/^<\/\w/)) {
                if (pad != 0) {
                    pad -= 1;
                }
            } else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
                indent = 1;
            } else {
                indent = 0;
            }

            let padding = '';

            for (let i = 0; i < pad; i++) {
                padding += '  ';
            }

            formatted += padding + node + '\r\n';
            pad += indent;
        });

        return formatted;
    }

    function orderBy(a, b, propName) {
        const propA = a[propName].toUpperCase();
        const propB = b[propName].toUpperCase();

        if (propA < propB) {
            return -1;
        }
        if (propA > propB) {
            return 1;
        }

        return 0;
    }

    return {
        formatXml,
        orderBy
    };
})();