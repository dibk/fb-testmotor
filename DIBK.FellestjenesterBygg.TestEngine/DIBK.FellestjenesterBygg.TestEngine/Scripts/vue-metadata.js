﻿new Vue({
    el: '#metadata',
    data: {
        forms: null,
        chosenForm: null,
        messageId: null,
        jsonResponse: null,
        jsonResponseProd: null,
        postData: null,
        serviceCode: serviceCode,
        serviceEdition: serviceEdition,
        userManager: apiUtils.getUserManager(),
        config: apiUtils.getConfig()
    },

    created: async function () {
        await Promise.all([
            this.fetchFormTask(),
            this.fetchFormTaskProd()
        ]);
    },
    methods: {
        setChosenForm: function (form) {
            this.chosenForm = form;
        },

        fetchFormTask: async function () {
            const url = `${this.config.altinnApiUrl}/api/metadata/formtask/${this.serviceCode}/${this.serviceEdition}`;

            try {
                const response = await apiUtils.fetchData(url, {
                    withCredentials: false,
                    headers: {
                        'Accept': 'application/hal+json'
                    }
                });

                this.jsonResponse = `status=${response.status}${JSON.stringify(response.data, null, 2)}`;
            } catch (error) {
                this.jsonResponse = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        },

        fetchFormTaskProd: async function () {
            const url = `https://www.altinn.no/api/metadata/formtask/${this.serviceCode}/${this.serviceEdition}`;

            try {
                const response = await axios.get(url, {
                    headers: {
                        'Accept': 'application/hal+json'
                    }
                });

                this.jsonResponseProd = `status=${response.status}${JSON.stringify(response.data, null, 2)}`;
            } catch (error) {
                this.jsonResponseProd = `status=${error.request.status}\r\n${error.request.statusText}\r\n${error.request.responseText}`;
            }
        }
    }
})