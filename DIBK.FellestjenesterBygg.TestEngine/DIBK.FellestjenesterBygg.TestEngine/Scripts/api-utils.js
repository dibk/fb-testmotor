const apiUtils = (() => {
    let _config = null;
    let _userManager = null;

    function _createConfig() {
        if (_config !== null) {
            return;
        }        

        const configElement = document.querySelector('[data-api-config]');
        _config = JSON.parse(configElement.dataset.apiConfig);
    }

    function _createUserManager() {
        if (_userManager !== null) {
            return;
        }

        const settings = {
            authority: _config.oidc.authority,
            client_id: _config.oidc.clientId,
            redirect_uri: _config.oidc.redirectUri,
            post_logout_redirect_uri: _config.oidc.postLogoutRedirectUri,
            scope: _config.oidc.scope,
            acr_values: _config.oidc.acrValues,
            client_secret: _config.oidc.clientSecret,
            client_authentication: 'client_secret_basic',
            response_type: 'code',
            ui_locales: 'nb',
            automaticSilentRenew: true,
            userStore: new oidc.WebStorageStateStore({
                store: localStorage
            }),
            revokeAccessTokenOnSignout: true
        };

        _userManager = new oidc.UserManager(settings);
    }

    function getConfig() {
        return _config;
    }

    function getUserManager() {
        return _userManager;
    }

    function hasAuthParams() {
        const searchParams = new URLSearchParams(location.search);

        return (searchParams.has('code') || searchParams.has('error')) && searchParams.has('state');
    }

    async function isLoggedIn() {
        const user = await _userManager.getUser();
        
        return user !== null && !user.expired;
    }

    async function loginCheck(redirectPath = location.pathname + location.search) {
        if (!await isLoggedIn()) {
            sessionStorage.autoRedirectPath = redirectPath;
            _userManager.signinRedirect();
        }
    }

    async function login(redirectPath) {
        if (await isLoggedIn()) {
            window.location.href = redirectPath;
        } else {
            sessionStorage.autoRedirectPath = redirectPath;
            _userManager.signinRedirect();
        }
    }

    async function continueLoginProcess() {
        if (hasAuthParams()) {
            await _userManager.signinCallback();
        }

        const autoRedirectPath = sessionStorage.autoRedirectPath || window.location.path;
        sessionStorage.removeItem('autoRedirectPath');

        if (window.location.path !== autoRedirectPath) {
            window.location.href = autoRedirectPath;
        }
    }

    async function fetchData(url, options = {}, withAuth = true) {
        const { headers, ...restOptions } = options;

        const defaultHeaders = {
            'Accept': 'application/hal+json',
        };

        if (withAuth) {
            await loginCheck();
            const user = await _userManager.getUser();

            defaultHeaders['Authorization'] = `Bearer ${user.access_token}`;
            defaultHeaders['ApiKey'] = getConfig().altinnApiKey;
        }

        const _headers = { ...defaultHeaders, ...headers || {} };

        const defaultOptions = {
            method: 'GET',
            headers: _headers,
            withCredentials: true
        };

        const _options = { ...defaultOptions, ...restOptions };

        return axios(url, _options);
    }

    _createConfig();
    _createUserManager();

    return {
        getConfig,
        getUserManager,
        hasAuthParams,
        isLoggedIn,
        loginCheck,
        login,
        continueLoginProcess,
        fetchData
    };
})();
