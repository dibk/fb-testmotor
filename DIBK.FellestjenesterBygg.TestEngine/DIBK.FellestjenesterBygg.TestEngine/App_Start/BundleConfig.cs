﻿using System.Web.Optimization;

namespace DIBK.FellestjenesterBygg.TestEngine
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/bower_components/dibk-fb-webcommon/assets/js/scripts").Include(
                "~/Content/bower_components/dibk-fb-webcommon/assets/js/main.min.js",
                "~/Content/bower_components/vue/dist/vue.js",                
                "~/Content/bower_components/respond/dest/respond.min.js",
                "~/Scripts/axios.min.js",
                "~/Scripts/modernizr-*"
            ));

            bundles.Add(new StyleBundle("~/Content/bower_components/dibk-fb-webcommon/assets/css/styles").Include(
                "~/Content/bower_components/dibk-fb-webcommon/assets/css/main.min.css",
                "~/Content/custom.css",
                "~/Content/dibk-design.css"
            ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
