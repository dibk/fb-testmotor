﻿using DIBK.FellestjenesterBygg.TestEngine.Models.Config;
using System.Web.Mvc;

namespace DIBK.FellestjenesterBygg.TestEngine.Controllers
{
    public class MetadataController : Controller
    {
        // GET: Metadata
        public ActionResult Index(int ServiceCode = 0, int ServiceEdition = 0)
        {
            ViewData["ApiConfig"] = ApiConfig.Create();
            ViewBag.ServiceCode = ServiceCode;
            ViewBag.ServiceEdition = ServiceEdition;

            return View();
        }
    }
}