﻿using DIBK.FellestjenesterBygg.TestEngine.Models.Config;
using System.Web.Mvc;

namespace DIBK.FellestjenesterBygg.TestEngine.Controllers
{
    public class SkjemaEditController : Controller
    {
        public ActionResult Index(string messageId, string reporteeId)
        {
            ViewData["ApiConfig"] = ApiConfig.Create();
            ViewBag.messageId = messageId;
            ViewBag.reporteeId = reporteeId;

            return View();
        }

        public ActionResult Form(string messageId, string reporteeId, string formurl)
        {
            ViewData["ApiConfig"] = ApiConfig.Create();
            ViewBag.messageId = messageId;
            ViewBag.reporteeId = reporteeId;
            ViewBag.formurl = formurl;

            return View();
        }
    }
}