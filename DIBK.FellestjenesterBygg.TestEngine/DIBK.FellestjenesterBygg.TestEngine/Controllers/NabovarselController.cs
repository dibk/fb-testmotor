﻿using System.Web.Mvc;
using DIBK.FellestjenesterBygg.TestEngine.Models;

namespace DIBK.FellestjenesterBygg.TestEngine.Controllers
{
    public class NabovarselController : Controller
    {
        public ActionResult Index()
        {
            string eksempelXml = new SendNabovarselTjeneste().HentEksempelXml();

            return View("Index", model: eksempelXml);
        }
    }
}