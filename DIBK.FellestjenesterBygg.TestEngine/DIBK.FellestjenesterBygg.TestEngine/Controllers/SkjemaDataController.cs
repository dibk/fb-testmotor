﻿using DIBK.FellestjenesterBygg.TestEngine.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace DIBK.FellestjenesterBygg.TestEngine.Controllers
{
    public class SkjemaDataController : ApiController
    {
        [Route("api/skjemadata")]
        [HttpGet]
        public List<Skjema> Get(bool data = true, bool attachments = false, bool formCollection = false)
        {
            var forms = new FormService().GetForms(data, attachments, formCollection);
            return forms;
        }

        [Route("api/skjemadata/validationUrl/{dataFormatId}")]
        [HttpGet]
        public ValidationApiUrl Get(string dataFormatId)
        {
            var valUrl = new FormService().GetValidationURL(dataFormatId);
            return valUrl;
        }

        [Route("api/skjemadata/{servicecode}/{serviceedition}")]
        [HttpGet]
        public Skjema GetForm(int servicecode, int serviceedition)
        {
            return new FormService().GetForm(servicecode, serviceedition);
        }

        [Route("api/skjemadata/vedlegg/{servicecode}/{serviceedition}")]
        [HttpGet]
        public List<Vedlegg> GetVedlegg(int servicecode, int serviceedition)
        {
            return new FormService().GetVedlegg(servicecode, serviceedition);
        }

        [Route("api/skjemadata/sample/{dataformatid}/{dataformatversion}")]
        [HttpGet]
        public Skjema GetFormSampleData(string dataformatid, string dataformatversion)
        {
            return new FormService().GetFormSampleData(dataformatid, dataformatversion);
        }
    }
}
