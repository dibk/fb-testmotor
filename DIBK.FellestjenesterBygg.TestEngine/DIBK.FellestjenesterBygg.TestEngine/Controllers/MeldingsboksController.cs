﻿using DIBK.FellestjenesterBygg.TestEngine.Models.Config;
using System.Web.Mvc;

namespace DIBK.FellestjenesterBygg.TestEngine.Controllers
{
    public class MeldingsboksController : Controller
    {
        public ActionResult Index()
        {
            ViewData["ApiConfig"] = ApiConfig.Create();

            return View();
        }
    }
}